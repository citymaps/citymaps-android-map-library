Follow the instructions at http://developer.citymaps.com/quick-start-guide-android to download the repo

- Create a new Android gradle project.

In the settings.gradle file, add
include ':mapenginelibrary'
project(':mapenginelibrary').projectDir = new File('<PathToEngineSDK>')

In your module's build.gradle, add the following to your dependencies list
compile project(':mapenginelibrary')

In your module's AndroidManifest, add the following permissions
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    
In your module's AndroidManifest, add the following to the application
    <meta-data android:name="com.citymaps.citymapsengine.API_KEY"
        android:value="<Your API key here>" />
        
Sync gradle.