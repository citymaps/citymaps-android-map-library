attribute vec2 a_position;
attribute vec2 a_perp;
attribute vec2 a_texCoord;

varying vec2 v_texCoord;

uniform mat4 u_mvp;
uniform float u_width;

void main()
{
    vec2 v2Pos = a_position + (a_perp * u_width);
    gl_Position = u_mvp * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
    v_texCoord = a_texCoord;
}