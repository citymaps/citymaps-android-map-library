varying lowp vec2 v_texCoord; 

uniform sampler2D u_texture;
uniform highp float u_alpha;

void main()
{
    lowp vec4 tex = texture2D(u_texture, v_texCoord);
    tex *= u_alpha;
    gl_FragColor = tex;
}
