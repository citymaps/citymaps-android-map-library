attribute vec2 VertexPosition;
attribute vec2 VertexTexCoord;

varying vec2 TexCoord;

uniform lowp float ZPosition;
uniform highp mat4 MVP;

void main()
{
    //gl_PointSize = 55.0;
    TexCoord = VertexTexCoord;
    
    gl_Position = MVP * vec4(VertexPosition.x, VertexPosition.y, 0.0, 1.0);
}
