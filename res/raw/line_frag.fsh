varying highp vec4 Color; 
varying highp vec2 TexCoord;

uniform lowp int drawMode;
uniform sampler2D Texture;
uniform sampler2D LineTexture;

void main()
{
    highp vec4 finalColor = Color;
    if(drawMode == 1) {
        highp vec4 texColor = texture2D(Texture, gl_PointCoord);
        if(texColor.r < 0.1) {
            discard;
        }
    } else if(false) {
        highp vec4 texColor = texture2D(LineTexture, TexCoord);
        highp float alpha = 1.0 - texColor.r;
        finalColor = vec4(Color.x, Color.y, Color.z, alpha);
    }
    
    gl_FragColor = finalColor;
    //gl_FragColor = finalColor;
    //gl_FragColor = texColor;
   // gl_FragColor = vec4(TexCoord.x, TexCoord.y, 0.0, 1.0);
}
