attribute vec2 a_position;
attribute vec2 a_texCoord;
attribute float a_layerId;

varying vec4 v_color;
varying vec2 v_texCoord;

uniform mat4 u_mvp;
uniform vec4 u_colors[75];

void main()
{
    int layerIdInt = int(a_layerId);
    v_color = u_colors[layerIdInt];
    
    v_texCoord = a_texCoord;
    
    gl_Position = u_mvp * vec4(a_position.x, a_position.y, 0.0, 1.0);
}