package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.DiskDataSource}.
 */
public class DiskDataSourceOptions extends DataSourceOptions
{
    private String mFilepath = null;

    /**
     * Constructs a new DiskDataSourceOptions.
     */
    public DiskDataSourceOptions()
    {}

    /**
     * Sets the filepath to retrieve tiles from.
     * <p>
     *   See {@link com.citymaps.citymapsengine.DiskDataSource} for more information about the format of this filepath.
     * </p>
     * @param filepath filepath to retrieve from. An exception will be thrown if this is left null.
     * @return this.
     */
    public DiskDataSourceOptions filepath(String filepath)
    {
        mFilepath = filepath;
        return this;
    }

    /**
     * Returns the filepath used to retrieve tiles.
     */
    public String getFilepath()
    {
        return mFilepath;
    }

}
