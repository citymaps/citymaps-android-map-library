package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapPosition;

/**
 * Options to create a {@link com.citymaps.citymapsengine.MapView}.
 */
public class MapViewOptions
{
    MapPosition mPosition = new MapPosition(new LonLat(-73.9863, 40.7566), 5, 0, 0);

    private float mMinZoom = 3.01f;
    private float mMaxZoom = 20;

    /**
     * Constructs a new MapViewOptions object.
     */
    public MapViewOptions()
    {}

    /**
     * Sets the starting position of the MapView.
     * @param position Starting position of the MapView
     * @return this
     */
    public MapViewOptions position(MapPosition position)
    {
        mPosition = position;
        return this;
    }

    /**
     * Sets the min zoom of the MapView.
     * @param minZoom Minimum zoom (most zoomed out) of the MapView. This must be a value [0..30].
     * @return this.
     * @throws java.lang.IllegalArgumentException If minZoom is outside its accepted bounds or is greater than or equal to max zoom.
     */
    public MapViewOptions minZoom(float minZoom)
    {
        if (minZoom >= mMaxZoom || minZoom < 0 || minZoom > 30)
            throw new IllegalArgumentException("MinZoom must be within [0..30]");
        mMinZoom = minZoom;
        return this;
    }

    /**
     * Sets the max zoom of the MapView
     * @param maxZoom Maximum zoom (most zoomed in) of the MapView. This must be a value [0..30]
     * @return this.
     * @throws java.lang.IllegalArgumentException If maxZoom is outside its accepted bounds or is less than or equal to min zoom.
     */
    public MapViewOptions maxZoom(float maxZoom)
    {
        if (maxZoom < 0 || maxZoom > 30)
            throw new IllegalArgumentException("MaxZoom must be within [0..30]");
        mMaxZoom = maxZoom;
        return this;
    }

    /**
     * Returns the requested MapPosition.
     */
    public MapPosition getPosition()
    {
        return mPosition;
    }

    /**
     * Returns the requested min zoom.
     */
    public float getMinZoom()
    {
        return mMinZoom;
    }

    /**
     * Returns the requested max zoom.
     */
    public float getMaxZoom()
    {
        return mMaxZoom;
    }
}
