package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.Size;

/**
 * Options to create a {@link com.citymaps.citymapsengine.EllipseFeature}.
 */
public class EllipseFeatureOptions extends CanvasFeatureOptions {
    private LonLat mCenter = new LonLat(0,0);
    private Size mRadii = new Size(10, 5);

    /**
     * Constructs a new EllipseFeatureOptions.
     */
    public EllipseFeatureOptions()
    {}

    /**
     * Sets the center of the ellipse feature.
     * @return this.
     */
    public EllipseFeatureOptions center(LonLat lonLat)
    {
        mCenter = new LonLat(lonLat);
        return this;
    }

    /**
     * Sets the radii of the ellipse feature.
     * @return this.
     */
    public EllipseFeatureOptions radii(Size radii)
    {
        mRadii = new Size(radii.width, radii.height);
        return this;
    }

    /**
     * Returns the center of the ellipse feature.
     */
    public LonLat getCenter()
    {
        return new LonLat(mCenter);
    }

    /**
     * Returns the radii of the ellipse feature.
     */
    public Size getRadii()
    {
        return new Size(mRadii.width, mRadii.height);
    }
}
