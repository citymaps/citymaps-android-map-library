package com.citymaps.citymapsengine.options;

import android.content.Context;
import com.citymaps.citymapsengine.VectorTileSource;

/**
 * Options to create a {@link com.citymaps.citymapsengine.VectorLayer}.
 */
public class VectorLayerOptions extends TileLayerOptions
{
    public VectorLayerOptions(String apiKey)
    {
        super();
        this.tileSource(new VectorTileSource(apiKey));
        this.setLODZooms();
    }

    public VectorLayerOptions(String apiKey, String styleFile)
    {
        super();
        this.tileSource(new VectorTileSource(apiKey, styleFile));

        this.setLODZooms();
    }

    public VectorLayerOptions(Context context, int styleResourceId)
    {
        super();
        String resource = context.getResources().getResourceEntryName(styleResourceId);
        this.tileSource(new VectorTileSource(resource));

        this.setLODZooms();
    }

    private void setLODZooms()
    {
        for(int i = 3; i <= 13; i += 2)
        {
            this.addLODZoom(i);
        }
    }
}
