package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.RegionTileSource;

/**
 * Options to create a {@link com.citymaps.citymapsengine.RegionLayer}.
 */
public class RegionLayerOptions extends TileLayerOptions
{
    private String mImageHostname = "http://r.citymaps.com";

    /**
     * Constructs a new RegionLayerOptions.
     */
    public RegionLayerOptions(String apiKey)
    {
        super();
        this.maxZoom(9);
        this.tileSource(new RegionTileSource(apiKey));
        this.setLODZooms();
    }

    private void setLODZooms()
    {
        for(int i = 3; i <= 9; i += 2)
        {
            this.addLODZoom(i);
        }
    }

    /**
     * Sets the hostname to retreive region images from.
     * @param hostname Hostname to use. Must not be null.
     * @return this.
     * @throws IllegalArgumentException If hostname is null.
     */
    public RegionLayerOptions hostname(String hostname)
    {
        if (hostname == null)
            throw new IllegalArgumentException("BusinessLayerOptions Hostname cannot be null.");
        mImageHostname = hostname;
        return this;
    }

    /**
     * Returns the hostname used to retrieve region images.
     */
    public String getHostname()
    {
        return mImageHostname;
    }
}