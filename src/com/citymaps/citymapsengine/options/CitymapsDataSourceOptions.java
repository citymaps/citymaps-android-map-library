package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.CitymapsDataSource}
 */
public class CitymapsDataSourceOptions extends WebDataSourceOptions
{
    public int mCacheVersion = 30;
    public boolean mSignedURLs = false;
    public String mSignedURLPattern;
    public String mAPIKey;

    /**
     * Constructs a new CitymapsDataSourceOptions.
     * If no API key is specified, the layer will look for it in the android manifest meta-data key "com.citymaps.citymapsengine.API_KEY"
     */
    public CitymapsDataSourceOptions()
    {
    }

    /**
     * Constructs a new CitymapsDataSourceOptions with an API Key.
     *
     * @param apiKey Your application's Citymaps API key
     */
    public CitymapsDataSourceOptions(String apiKey)
    {
        mAPIKey = apiKey;
    }

    /**
     * Sets the cache version for this data source.
     * @param version Version to use in this data source.
     * @return this.
     */
    public CitymapsDataSourceOptions cacheVersion(int version)
    {
        mCacheVersion = version;
        return this;
    }

    /**
     * Sets whether this layer uses signed URLs
     * @param signedURls whether to use signed URLs
     * @return this.
     */
    public CitymapsDataSourceOptions signedURLs(boolean signedURls)
    {
        mSignedURLs = signedURls;
        return this;
    }

    /**
     * Sets the signed URL pattern
     * @param signedURLPattern The signed URL pattern
     * @return this.
     */
    public CitymapsDataSourceOptions signedURLPattern(String signedURLPattern)
    {
        mSignedURLPattern = signedURLPattern;
        return this;
    }

    /**
     * Sets the API Key
     * @param apiKey Your application's Citymaps API key
     * @return this.
     */
    public CitymapsDataSourceOptions apiKey(String apiKey)
    {
        mAPIKey = apiKey;
        return this;
    }



    /**
     * Returns the cache version to be used by the data source.
     */
    public int getCacheVersion()
    {
        return mCacheVersion;
    }

    /**
     * Returns whether this source uses signed URLs
     */

    public boolean isSignedURLs() { return mSignedURLs; }

    /**
     * Returns the signed URL pattern
     */
    public String getSignedURLPattern() { return mSignedURLPattern; }

    /**
     * Returns your application's API Key
     */
    public String getAPIKey() { return mAPIKey; }
}
