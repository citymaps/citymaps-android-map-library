package com.citymaps.citymapsengine.options;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Options to create a {@link com.citymaps.citymapsengine.CanvasFeature}
 */
public class CanvasFeatureOptions
{
    private int mFillColor = Color.RED;
    private int mStrokeColor = Color.BLACK;
    private float mStrokeWidth = 0;
    private float mZIndex = 0;
    private boolean mVisible = true;
    private Bitmap mImage = null;

    /**
     * Constructs a new CanvasFeatureOptions.
     */
    public CanvasFeatureOptions()
    {}

    /**
     * Sets the fill color for the canvas feature.
     * @param color Fill {@link Color}
     * @return this.
     */
    public CanvasFeatureOptions fillColor(int color)
    {
        mFillColor = color;
        return this;
    }

    /**
     * Sets the stroke color for the canvas feature.
     * @param color Stroke {@link Color}
     * @return this.
     */
    public CanvasFeatureOptions strokeColor(int color)
    {
        mStrokeColor = color;
        return this;
    }

    /**
     * Sets the stroke width for the canvas feature (in pixels).
     * @param width Stroke width in pixels.
     * @return this.
     */
    public CanvasFeatureOptions strokeWidth(float width)
    {
        mStrokeWidth = width;
        return this;
    }

    /**
     * Sets the z index for the canvas feature.
     * @param zIndex Z index used for ordering.
     * @return this.
     */
    public CanvasFeatureOptions zIndex(float zIndex)
    {
        mZIndex = zIndex;
        return this;
    }

    /**
     * Sets the visibility for the canvas feature.
     * @param visible Whether the feature is visible.
     * @return this.
     */
    public CanvasFeatureOptions visible(boolean visible)
    {
        mVisible = visible;
        return this;
    }

    /**
     * Sets the image for the canvas feature.
     * @param image Bitmap to the image to be displayed on the feature.
     * @return this.
     */
    public CanvasFeatureOptions image(Bitmap image)
    {
        mImage = image;
        return this;
    }

    /**
     * Returns the fill color for this canvas feature.
     */
    public int getFillColor()
    {
        return mFillColor;
    }

    /**
     * Returns the stroke color for this canvas feature.
     */
    public int getStrokeColor()
    {
        return mStrokeColor;
    }

    /**
     * Retuns the stroke width for this canvas feature.
     */
    public float getStrokeWidth()
    {
        return mStrokeWidth;
    }

    /**
     * Returns the z index for this canvas feature.
     */
    public float getZIndex()
    {
        return mZIndex;
    }

    /**
     * Returns whether the canvas feature should be visible.
     */
    public boolean isVisible()
    {
        return mVisible;
    }

    /**
     * Returns a Bitmap to the image to be displayed on the canvas feature.
     */
    public Bitmap getImage()
    {
        return mImage;
    }
}
