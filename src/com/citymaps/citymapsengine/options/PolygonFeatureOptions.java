package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.PolygonFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * Options to create a {@link com.citymaps.citymapsengine.options.PolygonFeatureOptions}.
 */
public class PolygonFeatureOptions extends CanvasFeatureOptions
{
    private ArrayList<LonLat> mPoints = new ArrayList<LonLat>();
    private List<PolygonFeature> mHoles = new ArrayList<PolygonFeature>();

    /**
     * Constructs a new PolygonFeatureOptions.
     */
    public PolygonFeatureOptions()
    {
    }

    /**
     * Adds a point to the polygon feature.
     * @return this.
     */
    public PolygonFeatureOptions add(LonLat point)
    {
        mPoints.add(point);
        return this;
    }

    /**
     * Adds a series of points to the polygon feature.
     * @return this.
     */
    public PolygonFeatureOptions add(LonLat... points)
    {
        for(LonLat p : points)
        {
            mPoints.add(p);
        }

        return this;
    }

    /**
     * Adds a set of points to the polygon feature.
     * @return this.
     */
    public PolygonFeatureOptions addAll(Iterable<LonLat> points)
    {
        for(LonLat p : points)
        {
            mPoints.add(p);
        }
        return this;
    }

    /**
     * Adds a set of points as a hole to the polygon feature.
     * @return this.
     */
    public PolygonFeatureOptions addHole(Iterable<LonLat> points)
    {
        ArrayList<LonLat> hole = new ArrayList<LonLat>();
        for(LonLat p : points)
        {
            hole.add(p);
        }

        this.addHole(new PolygonFeature(new PolygonFeatureOptions().addAll(hole)));

        return this;
    }

    /**
     * Adds a hole to the polygon feature.
     * @return this.
     */
    public PolygonFeatureOptions addHole(PolygonFeature hole)
    {
        mHoles.add(hole);
        return this;
    }

    /**
     * Returns a list of holes in this polygon feature.
     */
    public List<PolygonFeature> getHoles()
    {
        return mHoles;
    }

    /**
     * Returns the list of points that make up this polygon feature.
     */
    public List<LonLat> getPoints ()
    {
        return mPoints;
    }
}
