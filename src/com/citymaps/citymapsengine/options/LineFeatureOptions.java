package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;

import java.util.ArrayList;
import java.util.List;

/**
 * Options to create a {@link com.citymaps.citymapsengine.LineFeature}.
 */
public class LineFeatureOptions extends CanvasFeatureOptions
{
    private List<LonLat> mPoints = new ArrayList<LonLat>();
    private float mWidth = 10;
    private boolean mDecimationEnabled = true;

    /**
     * Constructs a new LineFeatureOptions.
     */
    public LineFeatureOptions()
    {
    }

    /**
     * Adds a point to the end of the line feature.
     * @return this.
     */
    public LineFeatureOptions add(LonLat point)
    {
        mPoints.add(point);
        return this;
    }

    /**
     * Adds a series of points to the end of the line feature.
     * @return this.
     */
    public LineFeatureOptions add(LonLat... points)
    {
        for(LonLat p : points)
        {
            mPoints.add(p);
        }

        return this;
    }

    /**
     * Adds a set of points to the end of the line feature.
     * @return this.
     */
    public LineFeatureOptions addAll(Iterable<LonLat> points)
    {
        for(LonLat p : points)
        {
            mPoints.add(p);
        }

        return this;
    }

    /**
     * Sets the fill width of the line feature, in pixels.
     * @return this.
     */
    public LineFeatureOptions fillWidth(float width)
    {
        mWidth = width;
        return this;
    }

    /**
     * Whether or not to simplify the line based on zoom level.
     * @return this.
     */
    public LineFeatureOptions decimate(boolean enabled)
    {
        mDecimationEnabled = enabled;
        return this;
    }

    /**
     * Returns the set of points that make up this line feature.
     */
    public List<LonLat> getPoints()
    {
        return mPoints;
    }

    /**
     * Returns the fill width of this line feature.
     */
    public float getFillWidth()
    {
        return mWidth;
    }

    /**
     * Returns whether this line will be simplified based on zoom level.
     */
    public boolean isDecimationEnabled()
    {
        return mDecimationEnabled;
    }
}
