package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.Size;

/**
 * Options to create a {@link com.citymaps.citymapsengine.SquareFeature}.
 */
public class SquareFeatureOptions extends CanvasFeatureOptions
{
    LonLat mPosition = new LonLat(0,0);
    float mSize = 50;

    /**
     * Constructs a new SquareFeatureOptions.
     */
    public SquareFeatureOptions()
    {
        super();
    }

    /**
     * Sets the position of this square feature.
     * @return this.
     */
    public SquareFeatureOptions position(LonLat lonLat)
    {
        mPosition = new LonLat(lonLat);
        return this;
    }

    /**
     * Sets the size of this square feature.
     * @return this.
     */
    public SquareFeatureOptions size(float size)
    {
        mSize = size;
        return this;
    }

    /**
     * Returns the position of this square feature.
     */
    public LonLat getPosition()
    {
        return new LonLat(mPosition);
    }

    /**
     * Returns the size of this square feature.
     */
    public float getSize()
    {
        return mSize;
    }
}
