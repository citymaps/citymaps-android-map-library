package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.Layer}.
 */
public class LayerOptions
{
    private int mMinZoom = 0;
    private int mMaxZoom = 30;
    private boolean mVisible = true;

    /**
     * Constructs a new LayerOptions with default values.
     */
    public LayerOptions()
    {}

    /**
     * Sets the min zoom of the Layer.
     * @param minZoom Minimum zoom (most zoomed out) to display this layer. This must be a value [0..30].
     * @return this.
     * @throws java.lang.IllegalArgumentException If minZoom is outside its accepted bounds or is greater than or equal to max zoom.
     */
    public LayerOptions minZoom(int minZoom)
    {
        if (minZoom >= mMaxZoom || minZoom < 0 || minZoom > 30)
            throw new IllegalArgumentException("MinZoom must be within [0..30]");
        mMinZoom = minZoom;
        return this;
    }

    /**
     * Sets the max zoom of the Layer.
     * @param maxZoom Maximum zoom (most zoomed in) to display this layer. This must be a value [0..30].
     * @return this.
     * @throws java.lang.IllegalArgumentException If minZoom is outside its accepted bounds or is less than or equal to min zoom.
     */
    public LayerOptions maxZoom(int maxZoom)
    {
        mMaxZoom = maxZoom;
        return this;
    }

    /**
     * Sets the default visibility of the Layer. This can be changed after the layer has been created.
     * @param visible Whether the layer is visible by default.
     * @return this.
     */
    public LayerOptions visible(boolean visible)
    {
        mVisible = visible;
        return this;
    }


    /**
     * Returns the layer's min zoom.
     */
    public int getMinZoom()
    {
        return mMinZoom;
    }

    /**
     * Returns the layer's max zoom.
     */
    public int getMaxZoom()
    {
        return mMaxZoom;
    }

    /**
     * Returns the default visibility of this layer.
     */
    public boolean isVisible()
    {
        return mVisible;
    }
}
