package com.citymaps.citymapsengine.options;

import android.content.Context;

/**
 * Options to create a {@link com.citymaps.citymapsengine.CitymapsMapView}.
 */
public class CitymapsMapViewOptions extends MapViewOptions
{
    private String mAPIKey = "";

    private String mStyleFile = "map_config.xml";
    private boolean mBaseLayerVisible = true;
    private boolean mBusinessLayerVisible = true;
    private boolean mRegionLayerVisible = true;

    /**
     * Constructs a new CitymapsMapViewOptions.
     * If no API key is specified, the layer will look for it in the android manifest meta-data key "com.citymaps.citymapsengine.API_KEY"
     */
    public CitymapsMapViewOptions()
    {
        super();
    }

    /**
     * Constructs a new CitymapsDataSourceOptions with an API Key.
     *
     * @param apiKey Your application's Citymaps API key
     */
    public CitymapsMapViewOptions(String apiKey)
    {
        mAPIKey = apiKey;
    }

    /**
     * Sets the resource name to the vector layer configuration file.
     * @return this.
     */
    public CitymapsMapViewOptions styleFile(String styleFile)
    {
        mStyleFile = styleFile;
        return this;
    }

    /**
     * Sets the resource to be used as the vector layer configuration file.
     * @return this.
     */
    public CitymapsMapViewOptions styleFile(Context context, int resourceId)
    {
        mStyleFile = context.getResources().getResourceEntryName(resourceId);
        return this;
    }

    /**
     * Sets the visibility of the base layer of the map.
     * @return this.
     */
    public CitymapsMapViewOptions baseLayerVisible(boolean visible)
    {
        mBaseLayerVisible = visible;
        return this;
    }

    /**
     * Sets the visibility of the business layer of the map.
     * @return this.
     */
    public CitymapsMapViewOptions businessLayerVisible(boolean visible)
    {
        mBusinessLayerVisible = visible;
        return this;
    }

    /**
     * Sets the visibility of the region layer of the map.
     * @return this.
     */
    public CitymapsMapViewOptions regionLayerVisible(boolean visible)
    {
        mRegionLayerVisible = visible;
        return this;
    }

    /**
     * Sets the API Key
     * @param apiKey Your application's Citymaps API key
     * @return this.
     */
    public CitymapsMapViewOptions apiKey(String apiKey)
    {
        mAPIKey = apiKey;
        return this;
    }

    /**
     * Returns the name of the resource to be used as the vector layer configuration file.
     */
    public String getStyleFile()
    {
        return mStyleFile;
    }

    /**
     * Returns whether the base layer should be visible.
     */
    public boolean getBaseLayerVisible()
    {
        return mBaseLayerVisible;
    }

    /**
     * Returns whether the business layer should be visible.
     */
    public boolean getBusinessLayerVisible()
    {
        return mBusinessLayerVisible;
    }

    /**
     * Returns whether the region layer should be visible.
     */
    public boolean getRegionLayerVisible()
    {
        return mRegionLayerVisible;
    }

    /**
     * Returns your application's API Key
     */
    public String getAPIKey() { return mAPIKey; }
}
