package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.WebDataSource}.
 */
public class WebDataSourceOptions extends DataSourceOptions
{
    private String mURL = null;

    /**
     * Constructs a new DiskDataSourceOptions.
     */
    public WebDataSourceOptions()
    {}

    public WebDataSourceOptions(String url)
    {
        mURL = url;
    }

    /**
     * Sets the url to retrieve tiles from.
     * <p>
     *   See {@link com.citymaps.citymapsengine.WebDataSource} for more information about the format of this url.
     * </p>
     * @param url filepath to retrieve from. An exception will be thrown if this is left null.
     * @return this.
     */
    public WebDataSourceOptions url(String url)
    {
        mURL = url;
        return this;
    }

    /**
     * Returns the url used to retrieve tiles.
     */
    public String getUrl()
    {
        return mURL;
    }

}
