package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;

/**
 * Options to create a {@link com.citymaps.citymapsengine.CircleFeature}.
 */
public class CircleFeatureOptions extends CanvasFeatureOptions
{
    private LonLat mCenter = new LonLat(0,0);
    private float mRadius = 5;

    /**
     * Constructs a new CircleFeatureOptions.
     */
    public CircleFeatureOptions()
    {}

    /**
     * Sets the center for the circle feature.
     * @param lonLat Center of the circle feature.
     * @return this.
     */
    public CircleFeatureOptions center(LonLat lonLat)
    {
        mCenter = new LonLat(lonLat);
        return this;
    }

    /**
     * Sets the radius for the circle feature.
     * @param radius Radius of the circle feature.
     * @return this.
     */
    public CircleFeatureOptions radius(float radius)
    {
        mRadius = radius;
        return this;
    }

    /**
     * Returns the center of the circle feature.
     */
    public LonLat getCenter()
    {
        return new LonLat(mCenter);
    }

    /**
     * Returns the radius of the circle feature.
     */
    public float getRadius()
    {
        return mRadius;
    }
}
