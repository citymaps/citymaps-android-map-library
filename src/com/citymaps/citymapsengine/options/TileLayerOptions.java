package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.TileSource;

import java.util.HashSet;

/**.
 * Options to create a {@link com.citymaps.citymapsengine.TileLayer}
 */
public class TileLayerOptions extends LayerOptions
{
    private TileSource mTileSource = null;
    private int mTileSize = 256;
    private int mBuffer = 0;
    private HashSet<Integer> mLODZooms = new HashSet<Integer>();
    /**
     * Constructs a new TileLayerOptions with default values.
     */
    public TileLayerOptions()
    {}

    public TileLayerOptions(TileSource source)
    {
        mTileSource = source;
    }

    /**
     * Sets the TileSource for the TileLayer. An exception will be thrown if TileSource is left null when constructing the TileLayer.
     * @param source TileSource to be used for the TileLayer.
     * @return this.
     */
    public TileLayerOptions tileSource(TileSource source)
    {
        mTileSource = source;
        return this;
    }

    /**
     * Sets the tile size for the TileLayer (in mercator).
     * @param size Size of each tile in this TileLayer.
     * @return this.
     */
    public TileLayerOptions tileSize(int size)
    {
        mTileSize = size;
        return this;
    }

    /**
     * Sets the amount of tiles to retrieve outside the map bounds. This can be used to reduce the amount of loading tiles that appear, at the cost of memory and performance.
     * @param buffer Amount of tiles to retreive outside the map bounds.
     * @return this.
     */
    public TileLayerOptions buffer(int buffer)
    {
        mBuffer = buffer;
        return this;
    }

    /**
     * Adds a level of detail zoom. If any LOD zooms are set, the layer will only request tiles from these zooms. The layer will still display between its min and max zoom.
     * @param zoom LOD zoom to add.
     * @return this.
     */
    public TileLayerOptions addLODZoom(int zoom)
    {
        mLODZooms.add(zoom);
        return this;
    }

    /**
     * Returns the TileSource for this TileLayer.
     */
    public TileSource getTileSource()
    {
        return mTileSource;
    }

    /**
     * Returns the tile size for this TileLayer.
     */
    public int getTileSize()
    {
        return mTileSize;
    }

    /**
     * Returns the buffer for this TileLayer.
     */
    public int getBuffer()
    {
        return mBuffer;
    }

    /**
     * @return The set of level of detail zooms.
     */
    public HashSet<Integer> getLODZooms() { return mLODZooms; }
}
