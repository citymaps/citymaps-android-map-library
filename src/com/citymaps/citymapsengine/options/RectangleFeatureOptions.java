package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.Size;

/**
 * Options to create a {@link com.citymaps.citymapsengine.options.RectangleFeatureOptions}.
 */
public class RectangleFeatureOptions extends CanvasFeatureOptions
{
    LonLat mPosition = new LonLat(0,0);
    Size mSize = new Size(50, 50);

    /**
     * Constructs a new RectangleFeatureOptions.
     */
    public RectangleFeatureOptions()
    {
        super();
    }

    /**
     * Sets the position of this rectangle feature.
     * @return this.
     */
    public RectangleFeatureOptions position(LonLat lonLat)
    {
        mPosition = new LonLat(lonLat);
        return this;
    }

    /**
     * Sets the size of this rectangle feature.
     * @return this.
     */
    public RectangleFeatureOptions size(Size size)
    {
        mSize = new Size(size);
        return this;
    }

    /**
     * Returns the position of this rectangle feature.
     */
    public LonLat getPosition()
    {
        return new LonLat(mPosition);
    }

    /**
     * Returns the size of this rectangle feature.
     */
    public Size getSize()
    {
        return new Size(mSize);
    }
}