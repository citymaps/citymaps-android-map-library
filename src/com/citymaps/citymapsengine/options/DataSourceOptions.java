package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.DataSource}.
 */
public class DataSourceOptions
{
    private int mMaxDataCache = 5;

    /**
     * Create a new DataSourceOptions.
     */
    public DataSourceOptions()
    {}

    /**
     * Sets the max number of data retrievals to cache.
     * @param cache Number of data retrievals to cache.
     * @return this.
     */
    public DataSourceOptions maxDataCache(int cache)
    {
        mMaxDataCache = cache;
        return this;
    }

    /**
     * Returns the max number of data retrievals to cache.
     */
    public int getMaxDataCache()
    {
        return mMaxDataCache;
    }
}
