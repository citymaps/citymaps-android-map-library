package com.citymaps.citymapsengine.options;

import android.graphics.PointF;
import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.LonLatBounds;

/**
 * Options to create a {@link com.citymaps.citymapsengine.ImageFeature}.
 */
public class ImageFeatureOptions extends CanvasFeatureOptions
{
    public static final float NO_DIMENSION = -1.0f;

    private PointF mAnchor = new PointF(0.5f, 0.5f);
    private float mRotation = 0.0f;
    private LonLatBounds mBounds = null;
    private float mHeight = NO_DIMENSION;
    private float mAlpha = 1;
    private float mWidth = NO_DIMENSION;
    private LonLat mLocation = null;

    /**
     * Constructs a new ImageFeatureOptions.
     */
    public ImageFeatureOptions()
    {
    }

    /**
     * Sets the anchorpoint of the image feature.
     * @param u Normalized x position along the image to anchor to. (0 - left; 1 - right).
     * @param v Normalized y position along the image to anchor to. (0 - top; 1 - bottom).
     * @return this.
     */
    public ImageFeatureOptions anchor(float u, float v)
    {
        mAnchor.x = u;
        mAnchor.y = v;
        return this;
    }

    /**
     * Sets the anchorpoint of the image feature.
     * @param anchor Normalized position along the image to anchor to.
     * @return this.
     */
    public ImageFeatureOptions anchor(PointF anchor)
    {
        mAnchor.x = anchor.x;
        mAnchor.y = anchor.y;
        return this;
    }

    /**
     * Sets the rotation of the image feature.
     * @param rotation Rotation in degrees and clockwise.
     * @return this.
     */
    public ImageFeatureOptions rotation(float rotation)
    {
        mRotation = rotation;
        return this;
    }

    /**
     * Sets the position and size of the image feature. This does not keep the aspect ratio of the image in mind.
     * @param location Anchored location of the image feature.
     * @param width Width of the image feature (in mercator).
     * @param height Height of the image feature (in mercator).
     * @return this.
     */
    public ImageFeatureOptions position(LonLat location, float width, float height)
    {
        if (mBounds != null)
        {
            throw new IllegalStateException("ImageFeatureOptions was already set by positionFromBounds");
        }

        if (width < 0 || height < 0)
        {
            throw new IllegalArgumentException("ImageFeatureOptions: Width and height cannot be negative");
        }

        mLocation = new LonLat(location.latitude, location.longitude);
        mWidth = width;
        mHeight = height;
        mBounds = null;

        return this;
    }

    /**
     * Sets the position and size of the image feature. This keeps the aspect ratio of the image in mind.
     * @param location Anchored location of the image feature.
     * @param width Width of the image feature (in mercator).
     * @return this.
     */
    public ImageFeatureOptions position(LonLat location, float width)
    {
        if (mBounds != null)
        {
            throw new IllegalStateException("ImageFeatureOptions position was already set by positionFromBounds.");
        }

        if (width < 0)
        {
            throw new IllegalArgumentException("ImageFeatureOptions: Width cannot be negative.");
        }

        mLocation = new LonLat(location);
        mWidth = width;
        mHeight = NO_DIMENSION;

        return this;
    }

    /**
     * Sets the position. This will display the image based on image size, rather than mercator.
     * @param location Anchored location of the image feature.
     * @return this.
     */
    public ImageFeatureOptions position(LonLat location)
    {
        if (mBounds != null)
        {
            throw new IllegalStateException("ImageFeatureOptions position was already set by positionFromBounds.");
        }

        mLocation = new LonLat(location);
        mWidth = NO_DIMENSION;
        mHeight = NO_DIMENSION;

        return this;
    }

    /**
     * Sets the bounds to embed the image to. The image is scaled to tightly fit these bounds.
     * @return this.
     */
    public ImageFeatureOptions positionFromBounds(LonLatBounds bounds)
    {
        if (mLocation != null)
        {
            throw new IllegalStateException("ImageFeatureOptions position was already set by another method.");
        }

        mBounds = new LonLatBounds(bounds.min, bounds.max);
        mHeight = NO_DIMENSION;
        mWidth = NO_DIMENSION;
        return this;
    }

    /**
     * Sets the alpha of the image feature.
     * @return this.
     */
    public ImageFeatureOptions alpha(float alpha)
    {
        mAlpha = alpha;
        return this;
    }

    /**
     * Returns the u (x) coordinate of the anchor point.
     */
    public float getAnchorU()
    {
        return mAnchor.x;
    }

    /**
     * Returns the v (y) coordinate of the anchor point.
     */
    public float getAnchorV()
    {
        return mAnchor.y;
    }

    /**
     * Returns the anchor point of the image feature.
     */
    public PointF getAnchor()
    {
        return new PointF(mAnchor.x, mAnchor.y);
    }

    /**
     * Returns the rotation of the image feature.
     */
    public float getRotation()
    {
        return mRotation;
    }

    /**
     * Returns the bounds the image feature will be fitted to, or null if the image feature is not using bounds.
     */
    public LonLatBounds getBounds()
    {
        return mBounds;
    }

    /**
     * Returns the height of the image feature in mercator, or -1 if the height is unspecified.
     */
    public  float getHeight()
    {
        return mHeight;
    }

    /**
     * Returns the location of the image feature, or null if the image feature is using bounds.
     */
    public LonLat getLocation()
    {
        return mLocation;
    }

    /**
     * Returns the alpha of the image feature.
     */
    public float getAlpha()
    {
        return mAlpha;
    }

    /**
     * Returns the width of the image feature, or -1 if the width is unspecified.
     */
    public float getWidth()
    {
        return mWidth;
    }
}
