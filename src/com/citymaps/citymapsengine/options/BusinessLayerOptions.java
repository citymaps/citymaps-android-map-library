package com.citymaps.citymapsengine.options;

import com.citymaps.citymapsengine.BusinessTileSource;

/**
 * Options to create a {@link com.citymaps.citymapsengine.BusinessLayer}.
 */
public class BusinessLayerOptions extends TileLayerOptions
{
    private String mImageHostname = "http://r.citymaps.com";
    private String mLogoURL = "/riak/business_logos/{id}_150x150";
    private String mCategoryURL = "/riak/category_icons5_app/{id}";

    /**
     * Constructs a new BusinessLayerOptions.
     */
    public BusinessLayerOptions(String apiKey)
    {
        super();
        this.tileSource(new BusinessTileSource(apiKey)).minZoom(10);

        this.setLODZooms();
    }

    private void setLODZooms()
    {
        for(int i = 11; i <= 17; i += 2)
        {
            this.addLODZoom(i);
        }
    }

    /**
     * Sets the hostname to retreive business images from.
     * @param hostname Hostname to use. Must not be null.
     * @return this.
     * @throws java.lang.IllegalArgumentException If hostname is null.
     */
    public BusinessLayerOptions hostname(String hostname)
    {
        if (hostname == null)
            throw new IllegalArgumentException("BusinessLayerOptions Hostname cannot be null.");
        mImageHostname = hostname;
        return this;
    }

    /**
     * Sets the url to retreive logo from.
     * @param url Hostname to use. Must not be null.
     * @return this.
     * @throws java.lang.IllegalArgumentException If url is null.
     */
    public BusinessLayerOptions logoUrl(String url)
    {
        if (url == null)
            throw new IllegalArgumentException("BusinessLayerOptions LogoURL cannot be null.");
        mLogoURL = url;
        return this;
    }

    /**
     * Sets the url to retreive category icons from.
     * @param url Hostname to use. Must not be null.
     * @return this.
     * @throws java.lang.IllegalArgumentException If url is null.
     */
    public BusinessLayerOptions categoryURL(String url)
    {
        if (url == null)
            throw new IllegalArgumentException("BusinessLayerOptions CategoryURL cannot be null.");
        mCategoryURL = url;
        return this;
    }

    /**
     * Returns the hostname used to retrieve business images.
     */
    public String getHostname()
    {
        return mImageHostname;
    }

    /**
     * Returns the url used to retrieve business logos.
     */
    public String getLogoURL()
    {
        return mLogoURL;
    }

    /**
     * Returns the hostname used to retrieve category icons.
     */
    public String getCategoryURL()
    {
        return mCategoryURL;
    }
}