package com.citymaps.citymapsengine.options;

/**
 * Options to create a {@link com.citymaps.citymapsengine.VectorTileSource}
 */
public class VectorTileSourceOptions extends TileSourceOptions
{
    String mConfigFile = "map_config.xml";

    /**
     * Constructs a new VectorTileSourceOptions.
     */
    public VectorTileSourceOptions()
    {}

    /**
     * Sets the file to be used as the map config file.
     * @param configFile File to be used as the map config file.
     * @return this.
     */
    public VectorTileSourceOptions config(String configFile)
    {
        mConfigFile = configFile;
        return this;
    }

    /**
     * Returns the file to be used as the map config file.
     */
    public String getConfig()
    {
        return mConfigFile;
    }

}
