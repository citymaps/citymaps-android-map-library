package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapPosition;
import com.citymaps.citymapsengine.MapView;

/**
 * Created by adam on 5/8/14.
 */
public class MapTouchEvent
{
    public static final int TOUCH_UP = 0;
    public static final int TOUCH_DOWN = 1;
    public static final int TOUCH_MOVE = 2;
    public static final int TAP = 3;
    public static final int DOUBLE_TAP = 4;
    public static final int LONG_PRESS = 5;

    private MapView mMap;
    private LonLat mTouchPoint;
    private int mType;

    public MapTouchEvent(MapView map)
    {
        mMap = map;
    }

    public final MapView getMap() {
        return mMap;
    }

    public void setMap(MapView map) {
        this.mMap = map;
    }

    public final LonLat getTouchPoint() {
        return mTouchPoint;
    }

    public void setTouchPoint(LonLat point) {
        this.mTouchPoint = point;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}
