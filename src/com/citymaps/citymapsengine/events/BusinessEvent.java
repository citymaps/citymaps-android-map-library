package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.BusinessData;

/**
 * Created by adam on 5/8/14.
 */

public class BusinessEvent
{
    public final static int ADDED = 0;
    public final static int REMOVED = 1;

    private BusinessData mBusiness;
    private int mType;

    public BusinessEvent(BusinessData business)
    {
        mBusiness = business;
    }
    public BusinessEvent(BusinessData business, int type) {mBusiness = business; mType = type;}

    public final BusinessData getBusiness() {
        return mBusiness;
    }

    public void setBusiness(BusinessData business) {
        this.mBusiness = business;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}

