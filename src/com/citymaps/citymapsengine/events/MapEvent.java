package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.CanvasFeature;
import com.citymaps.citymapsengine.MapPosition;
import com.citymaps.citymapsengine.MapView;

/**
 * Created by adam on 5/8/14.
 */
public class MapEvent
{
    public static final int MOVE_START = 0;
    public static final int MOVE_END = 1;
    public static final int ZOOM_START = 2;
    public static final int ZOOM_END = 3;
    public static final int ROTATE_START = 4;
    public static final int ROTATE_CHANGE = 5;
    public static final int ROTATE_END = 6;
    public static final int TILT_START = 7;
    public static final int TILT_CHANGE = 8;
    public static final int TILT_END = 9;

    private MapView mMap;
    private MapPosition mPosition;
    private int mType;

    public MapEvent(MapView map)
    {
        mMap = map;
    }

    public final MapView getMap() {
        return mMap;
    }

    public void setMap(MapView map) {
        this.mMap = map;
    }

    public final MapPosition getPosition() {
        return mPosition;
    }

    public void setPosition(MapPosition position) {
        this.mPosition = position;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}
