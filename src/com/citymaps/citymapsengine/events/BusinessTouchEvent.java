package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.BusinessData;

/**
 * Created by adam on 5/8/14.
 */
public class BusinessTouchEvent
{
    public final static int TAP = 0;
    public final static int DOUBLE_TAP = 1;
    public final static int LONG_PRESS = 2;

    private BusinessData mBusiness;
    private int mType;

    public BusinessTouchEvent(BusinessData business)
    {
        mBusiness = business;
    }
    public BusinessTouchEvent(BusinessData business, int type) {mBusiness = business; mType = type;}

    public final BusinessData getBusiness() {
        return mBusiness;
    }

    public void setBusiness(BusinessData business) {
        this.mBusiness = business;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}
