package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.CanvasFeature;
import com.citymaps.citymapsengine.Marker;

/**
 * Created by adam on 5/8/14.
 */
public class MarkerEvent
{
    public static final int TOUCH_UP = 0;
    public static final int TOUCH_DOWN = 1;
    public static final int TAP = 2;
    public static final int DOUBLE_TAP = 3;
    public static final int LONG_PRESS = 4;
    public static final int DRAG_START = 5;
    public static final int DRAG_MOVE = 6;
    public static final int DRAG_END = 7;

    private Marker mMarker;
    private int mType;

    public MarkerEvent(Marker marker)
    {
        mMarker = marker;
    }

    public final Marker getMarker() {
        return mMarker;
    }

    public void setMarker(Marker marker) {
        this.mMarker = marker;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}
