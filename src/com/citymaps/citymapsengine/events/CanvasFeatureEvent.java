package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.CanvasFeature;

/**
 * Created by adam on 5/8/14.
 */
public class CanvasFeatureEvent
{
    public static final int TAP = 1;
    public static final int DOUBLED_TAP = 2;
    public static final int LONG_PRESS = 3;

    private CanvasFeature mFeature;
    private int mType;

    public CanvasFeatureEvent(CanvasFeature feature)
    {
        mFeature = feature;
    }

    public final CanvasFeature getFeature() {
        return mFeature;
    }

    public void setFeature(CanvasFeature feature) {
        this.mFeature = feature;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }
}
