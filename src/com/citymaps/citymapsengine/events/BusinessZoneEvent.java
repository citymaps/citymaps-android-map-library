package com.citymaps.citymapsengine.events;

import com.citymaps.citymapsengine.BusinessData;

/**
 * Created by adam on 5/8/14.
 */
public class BusinessZoneEvent
{
    public final static int ENTER = 0;
    public final static int EXIT = 1;

    private BusinessData mBusiness;
    private int mType;
    private int mIndex;

    public BusinessZoneEvent(BusinessData business)
    {
        mBusiness = business;
    }

    public BusinessZoneEvent(BusinessData business, int type, int index) {mBusiness = business; mType = type; mIndex = index;}

    public final BusinessData getBusiness() {
        return mBusiness;
    }

    public void setBusiness(BusinessData business) {
        this.mBusiness = business;
    }

    public final int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public final int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }
}
