package com.citymaps.citymapsengine;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.List;

class CitymapsOrientationListener implements SensorEventListener
{
    public interface OrientationChangeListener
    {
        public void onOrientationChanged(double orientation);
    }

    private OrientationChangeListener mListener = null;
    private SensorManager mSensorManager = null;
    private Sensor mAccelerometer = null;
    private Sensor mMagnetometer = null;

    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mAccelerometerChanged = false;
    private boolean mMagnetometerChanged = false;
    private boolean mAccelerometerSet = false;
    private boolean mMagnetometerSet = false;

    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];

    private float mLastOrientation;
    public static float kMinOrientationChange = 3;

    public CitymapsOrientationListener(Context context)
    {
        mSensorManager = (SensorManager) context
                .getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    public void onProviderEnabled(String provider) {

    }

    public void onProviderDisabled(String provider) {

    }

    public void activate(OrientationChangeListener listener)
    {
        if (listener == null)
            throw new IllegalArgumentException("OrientationChangeListener cannot be null on activation.");

        this.deactivate();

        final CitymapsOrientationListener instance = this;

        mListener = listener;
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                mAccelerometerChanged = false;
                mMagnetometerChanged = false;
                mAccelerometerSet = false;
                mMagnetometerSet = false;

                // The third parameter can be a constant from
                // SensorManager.SENSOR_DELAY...
                // or a custom time, in microseconds.
                mSensorManager.registerListener(instance, mAccelerometer, 50000);
                mSensorManager.registerListener(instance, mMagnetometer, 50000);
            }
        };

        new Handler(Looper.getMainLooper()).post(runnable);

    }

    public void deactivate()
    {
        if (mListener == null)
            return;

        mListener = null;
        final CitymapsOrientationListener instance = this;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mSensorManager.unregisterListener(instance);
            }
        });
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event)
    {
        if (event.sensor == mAccelerometer) {
                System.arraycopy(event.values, 0, mLastAccelerometer, 0,
                        event.values.length);
                mAccelerometerChanged = true;
                mAccelerometerSet = true;
        } else if (event.sensor == mMagnetometer) {
                System.arraycopy(event.values, 0, mLastMagnetometer, 0,
                        event.values.length);
                mMagnetometerChanged = true;
                mMagnetometerSet = true;
        }
        if (mAccelerometerSet && mMagnetometerSet && (mAccelerometerChanged || mMagnetometerChanged)) {
            mAccelerometerChanged = false;
            mMagnetometerChanged = false;

            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer,
                    mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);

            float degrees = (float) Math.toDegrees(mOrientation[0]);
            if (degrees < 0)
                degrees = 360 + degrees;

            float dif = Math.abs(degrees - mLastOrientation);
            if (dif > kMinOrientationChange && mListener != null)
            {
                mLastOrientation = degrees;
                mListener.onOrientationChanged(mLastOrientation);
            }
        }
    }
}
