package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.events.BusinessEvent;
import com.citymaps.citymapsengine.events.BusinessTouchEvent;
import com.citymaps.citymapsengine.events.BusinessZoneEvent;

/**
 * An object used by {@link CitymapsMapView} that can receive touch events on displayed businesses.
 * A BusinessOnTouchListener must be set to a {@link BusinessLayer}.
 * @see BusinessLayer
 */
public interface BusinessOnTouchListener
{
    /**
     * Occurs when a business event is triggered.
     * @param event Information about the business event.
     */
    public void onBusinessEvent(final BusinessEvent event);

    /**
     * Occurs when a displayed business is touched
     * @param event Information about the touch event
     *             @see BusinessData
     */
    public void onTouchEvent(final BusinessTouchEvent event);

    /**
     * Occurs when a business enters or exist one of the specified zones.
     * @param event Information about the business zone event.
     */
    public void onZoneEvent(final BusinessZoneEvent event);
}
