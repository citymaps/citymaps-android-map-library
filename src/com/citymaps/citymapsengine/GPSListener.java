package com.citymaps.citymapsengine;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

class GPSListener implements LocationListener, SensorEventListener {
    private long mAppPointer;
    private SensorManager mSensorManager;
    private LocationManager mLocationManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;

    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mAccelerometerChanged = false;
    private boolean mMagnetometerChanged = false;
    private boolean mAccelerometerSet = false;
    private boolean mMagnetometerSet = false;

    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];

    private float mLastOrientation;
    public static float kMinOrientationChange = 3;

    private native void PositionChanged(long appPointer,
                                        double latitude, double longitude, double accuracy);

    private native void AltitudeChanged(long appPointer, double latitude);

    private native void DirectionChanged(long appPointer,
                                         double direction);

    public GPSListener() {
        Context context = WindowAndroid.getApplicationContext();
        mAppPointer = 0;
        // Acquire a reference to the system Location Manager
        mLocationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        mSensorManager = (SensorManager) context
                .getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public synchronized void onLocationChanged(Location location) {
        // Called when a new location is found by the network location
        // provider.
        if (mAppPointer != 0) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            double accuracy = location.getAccuracy();
            PositionChanged(mAppPointer, latitude, longitude, accuracy);

            if (location.hasAltitude()) {
                double altitude = location.getAltitude();
                AltitudeChanged(mAppPointer, altitude);
            }
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {

    }

    public void onProviderDisabled(String provider) {

    }

    protected synchronized void setApplication(long appPointer) {
        mAppPointer = appPointer;

        if (mAppPointer == 0)
            return;

        Location bestLocation = getLastKnownLocation();

        if (bestLocation != null) {

            double bearing = bestLocation.getBearing();
            double altitude = bestLocation.getAltitude();
            double accuracy = bestLocation.getAccuracy();
            double lon = bestLocation.getLongitude();
            double lat = bestLocation.getLatitude();

            PositionChanged(mAppPointer, lat, lon, accuracy);
            DirectionChanged(mAppPointer, bearing);
            AltitudeChanged(mAppPointer, altitude);
        }
    }

    public synchronized void startUpdatingLocation(final LocationParams param) {
        final GPSListener instance = this;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                switch (param.getAccuracy()) {
                    case LocationParams.LOCATION_ACCURACY_FINE:
                        mLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                (long) (param.getMinTime() * 1000),
                                param.getMinDistance(), instance);
                        break;
                    case LocationParams.LOCATION_ACCURACY_COURSE:
                        mLocationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                (long) (param.getMinTime() * 1000),
                                param.getMinDistance(), instance);

                }

                Location bestLocation = getLastKnownLocation();

                if (bestLocation != null) {

                    double bearing = bestLocation.getBearing();
                    double altitude = bestLocation.getAltitude();
                    double accuracy = bestLocation.getAccuracy();
                    double lon = bestLocation.getLongitude();
                    double lat = bestLocation.getLatitude();

                    if (mAppPointer != 0) {
                        PositionChanged(mAppPointer, lat, lon, accuracy);
                        DirectionChanged(mAppPointer, bearing);
                        AltitudeChanged(mAppPointer, altitude);
                    }
                }
            }
        };

        new Handler(Looper.getMainLooper()).post(runnable);

    }

    public void startUpdatingOrientation() {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                mAccelerometerChanged = false;
                mMagnetometerChanged = false;
                mAccelerometerSet = false;
                mMagnetometerSet = false;

                // The third parameter can be a constant from
                // SensorManager.SENSOR_DELAY...
                // or a custom time, in microseconds.
                mSensorManager.registerListener(GPSListener.this, mAccelerometer, 50000);
                mSensorManager.registerListener(GPSListener.this, mMagnetometer, 50000);
            }
        };

        new Handler(Looper.getMainLooper()).post(runnable);

    }

    public void stopUpdatingLocation() {
        final GPSListener instance = this;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mLocationManager.removeUpdates(instance);
            }
        });
    }

    public void stopUpdatingOrientation() {
        final GPSListener instance = this;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mSensorManager.unregisterListener(instance);
            }
        });
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public synchronized void onSensorChanged(SensorEvent event)
    {
        if (event.sensor == mAccelerometer)
        {
                System.arraycopy(event.values, 0, mLastAccelerometer, 0,
                        event.values.length);
                mAccelerometerChanged = true;
                mAccelerometerSet = true;
        } else if (event.sensor == mMagnetometer)
        {
                System.arraycopy(event.values, 0, mLastMagnetometer, 0,
                        event.values.length);
                mMagnetometerChanged = true;
                mMagnetometerSet = true;
        }
        if (mAccelerometerSet && mMagnetometerSet && (mAccelerometerChanged || mMagnetometerChanged)) {
            mAccelerometerChanged = false;
            mMagnetometerChanged = false;

            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer,
                    mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);

            float degrees = (float) Math.toDegrees(mOrientation[0]);
            if (degrees < 0)
                degrees = 360 + degrees;

            degrees = 360 - degrees;
            float dif = Math.abs(degrees - mLastOrientation);
            if (dif > kMinOrientationChange && mAppPointer != 0) {
                mLastOrientation = degrees;
                DirectionChanged(mAppPointer, degrees);
            }
        }
    }

    public Location getLastKnownLocation() {
        Location bestLocation = null;

        List<String> providers = mLocationManager.getAllProviders();
        for (String provider : providers) {
            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location != null) {
                if (bestLocation == null) {
                    bestLocation = location;
                } else {
                    if (location.getAccuracy() > bestLocation.getAccuracy()) {
                        bestLocation = location;
                    }
                }
            }
        }

        return bestLocation;
    }
}
