package com.citymaps.citymapsengine;

import java.util.ArrayList;

/**
 * Created by eddiekimmel on 9/10/14.
 */
public class Route
{
    private LonLat mStart;
    private LonLat mEnd;
    private LonLat[] mPoints;
    private RouteInstruction[] mInstructions;
    private float mDistance;
    private int mDuration;

    private long mPtr;
    public Route()
    {}

    private native void nativeDelete(long ptr);

    public final LonLat getStart()
    {
        return mStart;
    }

    public final LonLat getEnd()
    {
        return mEnd;
    }

    public final LonLat[] getPoints()
    {
        return mPoints;
    }

    public final RouteInstruction[] getInstructions()
    {
        return mInstructions;
    }

    public float getDistance()
    {
        return mDistance;
    }

    public int getDuration()
    {
        return mDuration;
    }

    public void finalize()
    {
        nativeDelete(mPtr);
    }
}