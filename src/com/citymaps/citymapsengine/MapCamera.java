package com.citymaps.citymapsengine;

import android.graphics.PointF;

public class MapCamera
{
    //MapCamera*
    private long mNativeCameraPointer = 0;

    // Projection*
    private long mNativeProjectionPtr = 0;

    private native Object nativeProject(long nativeCameraPtr, long nativeProjectionPtr, double lon, double lat);
    private native Object nativeUnproject(long nativeCameraPtr, long nativeProjectionPtr, double x, double y);
    private native Object nativeGetBounds(long nativeCameraPtr, long nativeProjectionPtr);
    private native void nativeRelease(long nativeCameraPointer, long nativeProjectionPtr);

    protected MapCamera(long nativeCameraPtr, long nativeProjectionPtr)
    {
        mNativeCameraPointer = nativeCameraPtr;
        mNativeProjectionPtr = nativeProjectionPtr;
    }

    public PointF project(LonLat lonLat)
    {
        return (PointF)nativeProject(mNativeCameraPointer, mNativeProjectionPtr, lonLat.longitude, lonLat.latitude);
    }

    public LonLat unproject(PointF screenPosition)
    {
        return (LonLat)nativeUnproject(mNativeCameraPointer, mNativeProjectionPtr, screenPosition.x, screenPosition.y);
    }

    public LonLatBounds getBounds()
    {
        return (LonLatBounds)nativeGetBounds(mNativeCameraPointer, mNativeProjectionPtr);
    }

    public void finalize()
    {
        nativeRelease(mNativeCameraPointer, mNativeProjectionPtr);
    }
}
