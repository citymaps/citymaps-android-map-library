package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.LayerOptions;
import com.citymaps.citymapsengine.options.TileLayerOptions;

import java.util.HashMap;

/** This class allows the creation of an image-based tile layer.  Image based tile layers are the most common type of map layer found.
 * <p>
 * In order to create an image tile layer, you must provide a {@link ImageTileSource}, with valid URLs.  {@link ImageTileSource} requires a mercator-based tile server, similar to OpenStreetMap.org.
 * <p>
 * See {@link TileLayer} for a list of available options.
 */
public class ImageTileLayer extends TileLayer {
    private native long nativeCreateLayer(TileLayerDescription desc);

    public ImageTileLayer(TileLayerOptions options) {
        super(options);
    }

    @Override
    protected long createLayer(LayerOptions options) {
        TileLayerDescription desc = new TileLayerDescription();
        this.applyOptions(options, desc);

        return this.nativeCreateLayer(desc);
    }
}
