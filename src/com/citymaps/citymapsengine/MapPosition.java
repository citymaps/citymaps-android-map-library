package com.citymaps.citymapsengine;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Contains all properties that make up the position of a MapView.
 */
public class MapPosition implements Parcelable {
    public LonLat center;
    public float zoom;
    public double orientation;
    public double tilt;

    /**
     * Constructs a new {@link MapPosition}
     * @param center Point at which the map will be focused. Depending on tilt, this may not be the exact center of the map.
     * @param zoom Zoom of the map. Must be within the range [0..30].
     * @param orientation Orientation / Rotation of the map. Must be within the range [0..360].
     * @param tilt Tilt / Skew of the map. Must be within the range [0..60].
     * @throws java.lang.IllegalArgumentException If any property is outside its range.
     */
    public MapPosition(LonLat center, float zoom, double orientation, double tilt)
    {
        if (zoom < 0)
            throw new IllegalArgumentException("Zoom must be within the range [0..30]");

        if (orientation < 0 || orientation > 360)
            throw new IllegalArgumentException("Orientation must be within the range [0..360]");

        this.center = new LonLat(center);
        this.zoom = zoom;
        this.orientation = orientation;
        this.tilt = tilt;
    }

    /**
     * Constructs a copy of an existing {@link MapPosition}
     * @param other The {@link MapPosition} to copy
     * @throws java.lang.IllegalArgumentException If any property is outside its range.
     */
    public MapPosition(MapPosition other)
    {
        this.center = new LonLat(other.center);
        this.zoom = other.zoom;
        this.orientation = other.orientation;
        this.tilt = other.tilt;
    }

    private MapPosition(Parcel in)
    {
        this.center = in.readParcelable(LonLat.class.getClassLoader());
        this.zoom = in.readFloat();
        this.orientation = in.readDouble();
        this.tilt = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(center, 0);
        dest.writeFloat(zoom);
        dest.writeDouble(orientation);
        dest.writeDouble(tilt);
    }

    public static final Creator<MapPosition> CREATOR = new Creator<MapPosition>() {
        @Override
        public MapPosition createFromParcel(Parcel source) {
            return new MapPosition(source);
        }

        @Override
        public MapPosition[] newArray(int size) {
            return new MapPosition[size];
        }
    };
}
