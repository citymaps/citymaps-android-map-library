package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.LineFeatureOptions;

import java.util.ArrayList;
import java.util.List;

/** This class represents a user defined line to be placed on a {@link CanvasLayer}. */
public class LineFeature extends CanvasFeature
{
    private List<LonLat> mPoints = new ArrayList<LonLat>();
    private float mFillWidth = 10;
    private boolean mDecimationEnabled = true;

    private native void nativeAddPoint(long ptr, double x, double y);
    private native void nativeAddPoints(long ptr, double[] points, int count);
    private native void nativeSetPoints(long ptr, double[] points, int count);
    private native void nativeSetFillWidth(long ptr, float width);
    private native void nativeSetDecimationEnabled(long ptr, boolean enabled);

    public LineFeature()
    {
        this(new LineFeatureOptions());
    }

    public LineFeature(LineFeatureOptions options)
    {
        super(options);
        this.setFillWidth(options.getFillWidth());
        this.setPoints(options.getPoints());
        this.setDecimationEnabled(options.isDecimationEnabled());
    }

    protected long createFeature()
    {
        long ptr = this.nativeCreateShape(CanvasFeatureType.CanvasFeatureLine.getValue());
        return ptr;
    }

    /** This method adds a point to the line.
     * @param point The point to add to the line.
     */
    public void addPoint(LonLat point)
    {
        mPoints.add(point);
        this.nativeAddPoint(mNativePtr, point.longitude, point.latitude);
    }

    /** This method adds points to the line.
     * @param points Points to add to the line.
     */
    public void addPoints(List<LonLat> points)
    {
        double[] doubleArray = new double[points.size() * 2];

        for(int i = 0; i < points.size(); ++i)
        {
            LonLat point = points.get(i);
            mPoints.add(point);
            int index = i * 2;
            doubleArray[index + 0] = point.longitude;
            doubleArray[index + 1] = point.latitude;
        }

        this.nativeAddPoints(mNativePtr, doubleArray, points.size());
    }

    /** This method sets the points for the line.
     * @param points The new points for the line
     */
    public void setPoints(List<LonLat> points)
    {
        double[] doubleArray = new double[points.size() * 2];
        mPoints.clear();

        for(int i = 0; i < points.size(); ++i)
        {
            LonLat point = points.get(i);
            mPoints.add(point);
            int index = i * 2;
            doubleArray[index + 0] = point.longitude;
            doubleArray[index + 1] = point.latitude;
        }

        this.nativeSetPoints(mNativePtr, doubleArray, points.size());
    }

    /** Returns a copy of the list of points in this line.
     * @return Copy of the list of points of this line.
     */
    public List<LonLat> getPoints()
    {
        List<LonLat> copy = new ArrayList<LonLat>();
        for(LonLat p : mPoints)
        {
            copy.add(new LonLat(p.longitude, p.latitude));
        }

        return copy;
    }

    /** Sets the fill width of the line (in pixels). */
    public void setFillWidth(float width)
    {
        mFillWidth = width;
        this.nativeSetFillWidth(mNativePtr, width);
    }

    /** Gets the fill width of the line (in pixels). */
    public float getFillWidth()
    {
        return mFillWidth;
    }

    /** Returns the number of points in the line.
     * @return The number of points in the line.
     */
    public int getNumberOfPoints()
    {
        return mPoints.size();
    }

    /** Returns the point at the given index.
     * @param index The index of the point you want to retrieve.
     * @return The point requested.
     */
    public LonLat getPointAtIndex(int index)
    {
        if (index < 0 || index >= mPoints.size())
            return null;

        return new LonLat(mPoints.get(index));
    }

    public void setDecimationEnabled(boolean enabled)
    {
        nativeSetDecimationEnabled(mNativePtr, enabled);
    }

    public boolean isDecimationEnabled()
    {
        return mDecimationEnabled;
    }
}
