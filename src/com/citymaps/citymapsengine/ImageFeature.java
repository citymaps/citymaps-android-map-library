package com.citymaps.citymapsengine;

import android.graphics.Bitmap;
import android.graphics.PointF;
import com.citymaps.citymapsengine.options.ImageFeatureOptions;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 11/21/13
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class ImageFeature extends CanvasFeature
{
    private LonLat mPosition = new LonLat(0,0);
    private PointF mAnchorPoint = new PointF(0,0);
    private float mRotation = 0;
    private float mAlpha = 1.0f;
    private float mWidth = 0;
    private float mHeight = 0;
    private Bitmap mImage;

    private LonLatBounds mBounds = new LonLatBounds(0,0,0,0);
    private State mState = State.Position;

    private native void nativeSetAnchorPoint(long ptr, double x, double y);
    private native void nativeSetRotation(long ptr, float rotation);
    private native void nativeSetAlpha(long ptr, float alpha);
    private native void nativeSetPosition(long ptr, double lon, double lat);
    private native void nativeSetPositionAndWidth(long ptr, double lon, double lat, double width);
    private native void nativeSetPositionAndSize(long ptr, double lon, double lat, double width, double height);
    private native void nativeSetBounds(long ptr, double minLon, double minLat, double maxLon, double maxLat);

    public ImageFeature()
    {
        this(new ImageFeatureOptions());
    }
    /**
     * Constructs a new ImageFeature.
     */
    public ImageFeature(ImageFeatureOptions options)
    {
        super(options);

        this.setAlpha(options.getAlpha());
        this.setAnchorPoint(options.getAnchor());
        this.setRotation(options.getRotation());

        Bitmap bitmap = options.getImage();
        if (bitmap != null)
        {
            this.setImageFromBitmap(bitmap);
        }

        LonLat position = options.getLocation();
        if (position != null)
        {
            float width = options.getWidth();
            float height = options.getHeight();
            if (width < 0)
            {
                this.setPosition(position);
            }
            else if (height < 0)
            {
                this.setPosition(position, width);
            }
            else
            {
                this.setPosition(position, width, height);
            }
        }
        else
        {
            LonLatBounds bounds = options.getBounds();
            if (bounds != null)
            {
                this.setBounds(bounds);
            }
        }
    }

    protected long createFeature() {
        long ptr = this.nativeCreateShape(CanvasFeature.CanvasFeatureType.CanvasFeatureImage.getValue());
        return ptr;
    }

    /**
     * Sets the anchor point for this feature. The origin is the top left corner of the image.
     * @param point Position on the image to anchor the ImageFeature to.
     */
    public void setAnchorPoint(PointF point)
    {
        mAnchorPoint.set(point);
        nativeSetAnchorPoint(mNativePtr, mAnchorPoint.x, mAnchorPoint.y);
    }

    /**
     * Sets the rotation for this feature.
     * @param rotation Rotation for this feature (degrees).
     */
    public void setRotation(float rotation)
    {
        mRotation = rotation;
        nativeSetRotation(mNativePtr, mRotation);
    }

    /**
     * Sets the alpha for this feature.
     * @param alpha Alpha for this feature [0..1].
     */
    public void setAlpha(float alpha)
    {
        mAlpha = alpha;
        nativeSetAlpha(mNativePtr, mAlpha);
    }

    /**
     * Sets the image to be displayed from a {@link android.graphics.Bitmap}
     * @param bitmap The bitmap to display
     *               @see android.graphics.Bitmap
     */
    @Override
    public void setImageFromBitmap(Bitmap bitmap)
    {
        mImage = Bitmap.createBitmap(bitmap);
        super.setImageFromBitmap(bitmap);


        switch(mState)
        {
            case Position:
                mWidth = mImage.getWidth();
                mHeight = mImage.getHeight();
                break;
            case PositionRatio:
                mWidth = mImage.getWidth();
                float ratio = (float)mImage.getHeight() / (float)mImage.getWidth();
                mHeight = mWidth * ratio;
                break;
            case PositionSize:
                break;
            case Bounds:
                break;
        }
    }

    /**
     * Sets the position for this ImageFeature.
     * This will cause the feature to be drawn at the image's size, and ignore any preset size.
     * @param position New position of this image feature.
     */
    public void setPosition(LonLat position)
    {
        mPosition.set(position);
        mState = State.Position;

        if (mImage != null)
        {
            mWidth = mImage.getWidth();
            mHeight = mImage.getHeight();
        }
        nativeSetPosition(mNativePtr, mPosition.longitude, mPosition.latitude);
    }

    /**
     * Sets the position and width for this ImageFeature.
     * The height of this feature will be set according to the image's size ratio.
     * @param position New position of this image feature.
     * @param width Width of this image feature (in meters)
     */
    public void setPosition(LonLat position, float width)
    {
        mPosition.set(position);
        mWidth = width;
        mState = State.PositionRatio;
        if (mImage != null)
        {
            float ratio = (float)mImage.getHeight() / (float)mImage.getWidth();
            mHeight = mWidth * ratio;
        }

        nativeSetPositionAndWidth(mNativePtr, mPosition.longitude, mPosition.latitude, mWidth);
    }

    /**
     * Sets the position and size for this ImageFeature.
     * @param position New position of this image feature.
     * @param width Width of this image feature (in meters)
     * @param height Height of this image feature (in meters)
     */
    public void setPosition(LonLat position, float width, float height)
    {
        mPosition.set(position);
        mWidth = width;
        mHeight = height;
        mState = State.PositionSize;

        nativeSetPositionAndSize(mNativePtr, mPosition.longitude, mPosition.latitude, mWidth, mHeight);
    }

    /**
     * Sets the bounds for this ImageFeature.
     * This will cause anchor point to be ignored, but the ImageFeature will still be rotated.
     * The position of this ImageFeature will be set to the center of the bounds.
     * @param bounds New bounds for this image feature
     */
    public void setBounds(LonLatBounds bounds)
    {
        mPosition = bounds.getCenter();
        mWidth = -1;
        mHeight = -1;
        mBounds.set(bounds);
        mState = State.Bounds;

        nativeSetBounds(mNativePtr, mBounds.min.longitude, mBounds.min.latitude, mBounds.max.longitude, mBounds.max.latitude );
    }

    /**
     * @return Postion of the image feature.
     */
    public LonLat getPosition()
    {
        return new LonLat(mPosition);
    }

    /**
     * @return Anchor Point of the image feature.
     */
    public PointF getAnchorPoint()
    {
        return new PointF(mAnchorPoint.x, mAnchorPoint.y);
    }

    /**
     * @return Rotation of the image feature.
     */
    public float getRotation()
    {
        return mRotation;
    }

    /**
     * @return Alpha of the image feature.
     */
    public float getAlpha()
    {
        return mAlpha;
    }

    /**
     * @return Width of the image feature, or -1 if the feature is currently positioned by bounds.
     */
    public float getWidth()
    {
        return mWidth;
    }

    /**
     * @return Height of the image feature, or -1 if the feature is currently positioned by bounds.
     */
    public float getHeight()
    {
        return mHeight;
    }

    /**
     * @return Image of the image feature.
     */
    public Bitmap getImage()
    {
        return mImage;
    }

    private enum State
    {
        Position,
        PositionRatio,
        PositionSize,
        Bounds
    }
}