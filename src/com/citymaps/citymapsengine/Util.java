package com.citymaps.citymapsengine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;

class Util {
    public static final double DEFAULT_TOLERANCE = 0.0001;

    public static boolean equalWithinTolerance(double a, double b) {
        return Math.abs(a - b) < DEFAULT_TOLERANCE;
    }

    public static boolean equalWithinTolerance(double a, double b, double tolerance) {
        return Math.abs(a - b) < tolerance;
    }

    public static Bitmap createBitmapFromView(View view)
    {
        int width = view.getWidth();
        int height = view.getHeight();
        if (width == 0 || height == 0)
        {
            view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            width = view.getMeasuredWidth();
            height = view.getMeasuredHeight();
        }


        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        view.layout(0, 0, width, height);
        view.draw(canvas);

        return b;
    }

    public static void postToUIThread(Runnable runnable)
    {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void postDelayedToUIThread(Runnable runnable, long delayMillis)
    {
        new Handler(Looper.getMainLooper()).postDelayed(runnable, delayMillis);
    }


}
