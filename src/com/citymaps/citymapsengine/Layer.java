package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.LayerOptions;

import java.util.HashMap;

/** This abstract base class is the base class for all map layers.
 *
 * Layers are the foundation of all maps.  A map layer is a collection of data which is overlayed on the screen, and controlled by the map.
 * A map layer can range from anything as simple as an image overlay, to a complex data source.
 * Several types are layers are built into the Citymaps library, however new layer types can be built with any functionality imaginable.
 * <p>
 * Layer accepts the following options:
 *
 * <ul>
 *     <li>minZoom: The lower bound (most zoomed out) the map can zoom to.</li>
 *     <li>maxZoom: The upper bound (most zoomed in) the map can zoom to.</li>
 * </ul>
 */
public abstract class Layer
{
    static
    {
        CitymapsEngine.init();
    }

    protected long mNativePtr;

    private float mZIndex;
    private boolean mVisible;

    private native void nativeRelease(long nativePtr);

    private native void nativeSetZIndex(long nativePtr, float zIndex);
    private native void nativeSetVisible(long nativePtr, boolean visible);

    private LayerOptions mOptions;

    /**
     * Initializes a new layer from the given options
     *
     * @param options The options to build the layer
     * <p>
     * Layer accepts the following options:
     *
     * <ul>
     *     <li>minZoom: The lower bound (most zoomed out) the map can zoom to.</li>
     *     <li>maxZoom: The upper bound (most zoomed in) the map can zoom to.</li>
     * </ul>
     * Subclasses of Layer may require additional options.
     */
    public Layer(LayerOptions options)
    {
        if (options == null)
            throw new IllegalArgumentException("LayerOptions cannot be null. Please use an appropriate default options object.");

        mOptions = options;
        mVisible = true;
        mZIndex = 0;
        mNativePtr = this.createLayer(options);
    }

    protected void onDestroy() {
        Log.w("MapEngine", "Deleting Layer.");
        this.nativeRelease(mNativePtr);
    }

    protected abstract long createLayer(LayerOptions options);

    protected void applyOptions(LayerOptions options, LayerDescription desc)
    {
        desc.minZoom = options.getMinZoom();
        desc.maxZoom = options.getMaxZoom();
        desc.visible = options.isVisible();
    }

    protected class LayerDescription {
        public int minZoom = 0;
        public int maxZoom = 30;
        public boolean visible = true;
    }

    protected void preUpdate()
    {

    }

    protected void postUpdate()
    {

    }

    /**
     * Returns whether the layer is currently visible.
     */
    public boolean isVisible()
    {
        return mVisible;
    }

    /**
     * Sets the layer's visibility. Invisible layers will not be shown.
     */
    public void setVisible(boolean visible)
    {
        mVisible = visible;
        nativeSetVisible(mNativePtr, visible);
    }

    public void setZIndex(float zIndex)
    {
        mZIndex = zIndex;
        nativeSetZIndex(mNativePtr, zIndex);
    }

    public float getZIndex()
    {
        return mZIndex;
    }

}
