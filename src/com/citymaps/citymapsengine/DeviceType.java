package com.citymaps.citymapsengine;

import android.os.Build;
import android.content.Context;

/**
 * Provides helper methods to determine attributes about the user's device.
 */
public class DeviceType
{
    /** DPI that must be met to be considered Low DPI */
    public static final int LowDPI = 160;

    /** DPI that must be met to be considered Medium DPI */
    public static final int MediumDPI = 240;

    /** DPI that must be met to be considered High DPI */
    public static final int HighDPI = 320;

    /** DPI that must be met to be considered XHigh DPI */
    public static final int XHighDPI = 480;

    private static String mDeviceName = null;
    private static int mDeviceDPI = 0;

    /**
     * Returns the device's DPI.
     * @param context Current context of the application.
     * @return The device's DPI.
     */
    public static int getDeviceDPI(Context context) {
        if (mDeviceDPI == 0) {
            mDeviceDPI = WindowAndroid.getApplicationContext().getResources().getDisplayMetrics().densityDpi;
        }
        return mDeviceDPI;
    }

    /**
     * Returns whether the engine considers the device low end.
     * <p>
     * A low end device is one which does not meet the requirements of {@link #HighDPI}.
     * @param context Current context of the application.
     * @return Whether the device is considered low end.
     */
    public static boolean isLowEnd(Context context) {
        return context.getResources().getBoolean(R.bool.lowend);
    }

    public static int getFramerate(Context context) {
        return context.getResources().getInteger(R.integer.framerate);
    }
}
