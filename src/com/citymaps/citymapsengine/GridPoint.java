package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 2/12/14.
 */
public class GridPoint
{
    public int x;
    public int y;
    public int zoom;

    public GridPoint()
    {
        x = y = zoom = 0;
    }

    public GridPoint(int x, int y, int zoom)
    {
        this.x = x;
        this.y = y;
        this.zoom = zoom;
    }

    public boolean equals(Object other)
    {
        if (!(other instanceof GridPoint))
        {
            return false;
        }

        GridPoint gp = (GridPoint)other;
        return this.x == gp.x && this.y == gp.y && this.zoom == gp.zoom;
    }

    public int hashCode()
    {
        return x ^ y ^ zoom;
    }
}
