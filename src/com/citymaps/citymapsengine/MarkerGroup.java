package com.citymaps.citymapsengine;

import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.List;

/** A marker group is a container which can be used to add markers to the map.
 *
 * <p>Marker groups allow you to organize your markers so you can perform batch operations on multiple markers at once.  </p>
 * <p>
 *     Marker groups should NOT be explicitely constructed.  Instead, you should use {@link MapView#getMarkerGroup(String)} to obtain a marker group correctly.
 *
 *     @see MapView
 * </p>
 *
 */

public class MarkerGroup
{
    protected long mNativePointer;

    private String mName = null;
    private ArrayList<Marker> mMarkers = null;
    private MapView mMap = null;
    private boolean mCollisionsActive;

    private native void nativeAddMarker(long enginePointer, long markerPointer);
    private native void nativeRemoveMarker(long enginePointer, long markerPointer);
    private native void nativeRemoveAllMarkers(long enginePointer);
    private native void nativeSetCollisionsActive(long enginePointer, boolean active);

    protected MarkerGroup(long nativePointer, String name, MapView map)
    {
        mNativePointer = nativePointer;
        mName = name;
        mMarkers = new ArrayList<Marker>();
        mMap = map;
    }

    /** Add a marker to this marker group.
     *
     * @param marker The marker to be added.
     */
    public void addMarker(Marker marker) {
        synchronized (mMarkers) {
            mMarkers.add(marker);
            marker.setMap(mMap);
            nativeAddMarker(mNativePointer, marker.mNativePointer);
        }
    }

    /** Remove all markers from this marker group. */
    public void removeAllMarkers()
    {
        nativeRemoveAllMarkers(mNativePointer);
        synchronized (mMarkers) {
            for (Marker marker : mMarkers) {
                marker.setMap(null);
            }
            mMarkers.clear();
        }
    }

    /** Remove a marker from this marker group.
     *
     * @param marker The marker to be removed.
     */
    public void removeMarker(Marker marker) {
        synchronized (mMarkers) {
            mMarkers.remove(marker);
            marker.setMap(null);
            nativeRemoveMarker(mNativePointer, marker.mNativePointer);
        }
    }

    /** Get the name of the marker group
     *
     * @return The marker group's name
     */
    public String getName() {
        return mName;
    }

    public void setCollisionsActive(boolean active)
    {
        mCollisionsActive = active;
        nativeSetCollisionsActive(mNativePointer, active);
    }

    public boolean getCollisionsActive()
    {
        return mCollisionsActive;
    }

    protected void preUpdate()
    {
        synchronized (mMarkers) {
            for (Marker marker : mMarkers) {
                marker.preUpdate();
            }
        }
    }

    protected void postUpdate()
    {
        synchronized (mMarkers) {
            for (Marker marker : mMarkers) {
                marker.postUpdate();
            }
        }
    }

    protected void setMap(MapView map)
    {
        mMap = map;

        synchronized (mMarkers) {
            for (Marker marker : mMarkers) {
                marker.setMap(map);
            }
        }
    }
}
