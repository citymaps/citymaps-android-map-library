package com.citymaps.citymapsengine;

import android.content.Context;
import android.util.Log;
import com.citymaps.citymapsengine.options.LayerOptions;
import com.citymaps.citymapsengine.options.VectorLayerOptions;

import java.util.HashMap;

/** This is the base layer used by the Citymaps map view to draw the vector-based data received from the Citymaps tile server.
 *<p>
 *See {@link TileLayer} for a list of available options.
 */
public class VectorLayer extends TileLayer {
    private native long nativeCreateLayer(TileLayerDescription desc);
    private native void nativeMakeRoutingRequest(long ptr, Object request, Object listener);

    public VectorLayer(LayerOptions options) {
        super(options);
    }

    public VectorLayer(String apiKey)
    {
        super(new VectorLayerOptions(apiKey));
    }

    public VectorLayer(String apiKey, String styleFile)
    {
        super(new VectorLayerOptions(apiKey, styleFile));
    }

    public VectorLayer(Context context, int styleResourceId)
    {
        super(new VectorLayerOptions(context, styleResourceId));
    }

    private Route mCurrentRoute = null;
    private RoutingListener mRoutingListener = null;

    @Override
    protected long createLayer(LayerOptions options) {
        TileLayerDescription desc = new TileLayerDescription();
        this.applyOptions(options, desc);

        return this.nativeCreateLayer(desc);
    }

    @Override
    protected void onDestroy()
    {
        Log.w("MapEngine", "Destroying vector layer");
        super.onDestroy();
    }

    public void startRoutingRequest(RoutingRequest request, RoutingRequestListener listener)
    {
        nativeMakeRoutingRequest(mNativePtr, request, listener);
    }
}
