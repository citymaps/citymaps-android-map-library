package com.citymaps.citymapsengine;

import java.util.ArrayList;

/** A feature group is a container which can be used to add CEFeatures to the map.
 *
 * <p>Features groups allow you to organize your features so you can perform batch operations on multiple markers at once.
 * Features groups should NOT be explicitly constructed.  Instead, you should use {@link CanvasLayer#getFeatureGroup(String)} to obtain a feature group correctly. </p>

 */

public class FeatureGroup
{
    private native void nativeAddFeature(long ptr, long featurePtr);
    private native void nativeRemoveFeature(long ptr, long featurePtr);
    private native void nativeRemoveAllFeatures(long ptr);
    private native void nativeSetActive(long ptr, boolean active);
    private native boolean nativeIsActive(long ptr);

    protected long mNativePointer;
    private String mName;
    private ArrayList<CanvasFeature> mFeatures;

    protected FeatureGroup(long nativePointer, String name) {
        mNativePointer = nativePointer;
        mName = name;
        mFeatures = new ArrayList<CanvasFeature>();
    }

    /** Adds a feature to the feature group.
     *@param feature The feature to add to the feature group.
     */
    public void addFeature(CanvasFeature feature) {
        mFeatures.add(feature);
        nativeAddFeature(mNativePointer, feature.mNativePtr);
    }

    /** Removes a feature from the feature group.
     * @param feature The feature to remove from the feature group.
     */
    public void removeFeature(CanvasFeature feature) {
        mFeatures.remove(feature);
        nativeRemoveFeature(mNativePointer, feature.mNativePtr);
    }

    /** Removes all features from the feature group.
     */
    public void removeAllFeature() {
        mFeatures.clear();
        nativeRemoveAllFeatures(mNativePointer);
    }
    /**
     * Changes active status for the entire group. If the group is deactivated, no features in that group will appear.
     */
    public void setActive(boolean active) {
        nativeSetActive(mNativePointer, active);
    }

    /**
     * Returns whether the feature group is active.
     */
    public boolean isActive() {
        return nativeIsActive(mNativePointer);
    }

    /**
     * Returns the name of this feature group.
     */
    public String getName() {
        return mName;
    }
}
