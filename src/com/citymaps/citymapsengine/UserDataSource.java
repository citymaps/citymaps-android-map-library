package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.DataSourceOptions;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: adam
 * Date: 11/25/13
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserDataSource extends DataSource
{
    private native long nativeCreate();

    private DataListener mListener;

    public UserDataSource()
    {
        this(new DataSourceOptions());
    }

    public UserDataSource(DataSourceOptions options) {
        super(options);
    }

    public void setListener(DataListener listener)
    {
        mListener = listener;
    }

    @Override
    protected long createDataSource(DataSourceOptions options) {
        return nativeCreate();
    }

    private byte[] getTileData(int x, int y, int zoom)
    {
        if (mListener != null) {
            return mListener.onTileRequested(x, y, zoom);
        }

        return null;
    }

    public interface DataListener
    {
        public byte[] onTileRequested(int x, int y, int zoom);
    }
}
