package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 9/11/14.
 */
public interface RoutingRequestListener
{
    public void onRoutingRequestComplete(Route[] routes, Throwable throwable);
}
