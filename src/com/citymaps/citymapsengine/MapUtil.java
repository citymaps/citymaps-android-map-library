package com.citymaps.citymapsengine;

public class MapUtil {
    static final double DOTS_PER_INCH = 72.0;
    static final double INCHES_PER_METER = 39.3701;
    static final double METERS_PER_MILE = 1609.344;
    static final double WORLD_RESOLUTION = 156543.03390625;
    static final double WORLD_RADIUS_KM = 6373.0;

    public static double getResolutionFromScale(double scale) {
        if (scale == 0)
            return 0;

        double normScale = (scale > 1.0) ? (1.0 / scale) : scale;

        double resolution = 1 / (normScale * INCHES_PER_METER * DOTS_PER_INCH);

        return resolution;
    }

    public static double getScaleFromResolution(double res) {
        return res * INCHES_PER_METER * DOTS_PER_INCH;
    }

    public static double getResolutionForZoom(double zoom) {
        return (WORLD_RESOLUTION / Math.pow(2, zoom));
    }

    public static double getDistanceMeters(LonLat p1, LonLat p2) {
        double a = Math.sin(Math.toRadians(p2.longitude - p1.longitude) * 0.5);
        double b = Math.sin(Math.toRadians(p2.latitude - p1.latitude) * 0.5);

        double c = (b * b) + Math.cos(Math.toRadians(p1.latitude)) * Math.cos(Math.toRadians(p2.latitude)) * (a * a);
        double d = Math.atan2(Math.sqrt(c), Math.sqrt(1 - c)) * 2.0 * WORLD_RADIUS_KM;
        return d * 1000;
    }

    public static double getBearing(LonLat p1, LonLat p2)
    {
        double dlon = Math.toRadians(p2.longitude - p1.longitude);
        double lat1 = Math.toRadians(p1.latitude);
        double lat2 = Math.toRadians(p2.latitude);

        double theta = Math.atan2(Math.sin(dlon) * Math.cos(lat2), (Math.cos(lat1) * Math.sin(lat2)) - (Math.sin(lat1) * Math.cos(lat2) * Math.cos(dlon)));
        return Math.toDegrees(theta);
    }
}
