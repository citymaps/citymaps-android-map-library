package com.citymaps.citymapsengine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import com.citymaps.citymapsengine.events.CanvasFeatureEvent;
import com.citymaps.citymapsengine.options.CanvasFeatureOptions;

import java.util.HashMap;

/**
 This class is the base class for all user defined shapes supported by CECanvasLayer.
 */
public abstract class CanvasFeature {

    protected long mNativePtr;

    private int mFillColor;
    private int mStrokeColor;
    private float mStrokeWidth;
    private float mZIndex;
    private boolean mVisible;
    private CanvasFeatureOnTouchListener mListener;

    protected enum CanvasFeatureType {
        CanvasFeatureLine(0),
        CanvasFeaturePolygon(1),
        CanvasFeatureEllipse(2),
        CanvasFeatureCircle(3),
        CanvasFeatureRectangle(4),
        CanvasFeatureSquare(5),
        CanvasFeatureImage(6);

        private int mValue;

        private CanvasFeatureType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    protected native long nativeCreateShape(int type);
    private native void nativeRelease(long ptr);
    private native void nativeSetFillColor(long ptr, float r, float g, float b, float a);
    private native void nativeSetStrokeColor(long ptr, float r, float g, float b, float a);
    private native void nativeSetStrokeWidth(long ptr, float width);
    private native void nativeSetFillImageFromFile(long ptr, String file);
    private native void nativeSetFillImageFromBitmap(long ptr, int[] data, int width, int height);
    private native void nativeSetVisible(long ptr, boolean visible);
    private native void nativeSetZIndex(long ptr, float zIndex);
    private native void nativeSetCanvasFeatureListener(long ptr, boolean active);

    public CanvasFeature(CanvasFeatureOptions options)
    {
        mNativePtr = this.createFeature();
        this.setFillColor(options.getFillColor());
        this.setStrokeColor(options.getStrokeColor());
        this.setStrokeWidth(options.getStrokeWidth());
        this.setVisible(options.isVisible());
        this.setZIndex(options.getZIndex());
        if (options.getImage() != null)
        {
            this.setImageFromBitmap(options.getImage());
        }
    }

    public CanvasFeature() {
        mNativePtr = this.createFeature();
    }

    protected void finalize() throws Throwable
    {
        if (mNativePtr != 0)
        {
            nativeRelease(mNativePtr);
        }

        super.finalize();
    }

    protected abstract long createFeature();

    /** The color to fill the feature with.
     * @see Color
     */
    public void setFillColor(int color)
    {
        mFillColor = color;
        nativeSetFillColor(mNativePtr, Color.red(color) / 255.f, Color.green(color) / 255.f, Color.blue(color) / 255.f, Color.alpha(color) / 255.f);
    }

    /** The color to outline the feature with.
     * @see Color
     */
    public void setStrokeColor(int color)
    {
        mStrokeColor = color;
        nativeSetStrokeColor(mNativePtr, Color.red(color) / 255.f, Color.green(color) / 255.f, Color.blue(color) / 255.f, Color.alpha(color) / 255.f);
    }

    /** The width of the outline around this feature.*/
    public void setStrokeWidth(float width)
    {
        mStrokeWidth = width;
        nativeSetStrokeWidth(mNativePtr, width);
    }

    /**
     * Returns the current fill color.
     * @return Fill color of the shape.
     */
    public int getFillColor()
    {
        return mFillColor;
    }

    /**
     * Returns the current stroke color.
     * @return Stroke color of the shape.
     */
    public int getStrokeColor()
    {
        return mStrokeColor;
    }

    /**
     * Returns the current stroke width.
     * @return Stroke width of the shape.
     */
    public float getStrokeWidth()
    {
        return mStrokeWidth;
    }

    /**
     * Returns the z index of the feature.
     * @return Z index of the feature.
     */
    public float getZIndex()
    {
        return mZIndex;
    }

    /**
     Sets the image to be displayed from a resource.
     @param c The current activity context.
     @param id The drawable resource identifier to load.
     */
    public void setImageFromResource(Context c, int id) {
        Bitmap bitmap = BitmapFactory.decodeResource(c.getResources(), id);
        this.setImageFromBitmap(bitmap);
    }

    /**
     Sets the image to be displayed from a file.
     @param file The image file to load.
     */
    public void setImageFromFile(String file) {
        nativeSetFillImageFromFile(mNativePtr, file);
    }

    /**
     * Sets the image to be displayed from a {@link Bitmap}
     * @param bitmap The bitmap to display
     *               @see Bitmap
     */
    public void setImageFromBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size];

        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        nativeSetFillImageFromBitmap(mNativePtr, pixels, width, height);
    }

    /**
     * Whether this feature will be visible.
     * @param visible Visibility of the feature.
     */
    public void setVisible(boolean visible)
    {
        mVisible = visible;
        nativeSetVisible(mNativePtr, visible);
    }

    /**
     * Sets the Z index for this feature
     * @param zIndex Z Index of the feature.
     */
    public void setZIndex(float zIndex)
    {
        mZIndex = zIndex;
        nativeSetZIndex(mNativePtr, zIndex);
    }

    /**
     * Sets the touch listener for this feature
     * @param listener The new listener for this feature
     */
    public void setOnTouchListener(CanvasFeatureOnTouchListener listener)
    {
        mListener = listener;
        nativeSetCanvasFeatureListener(mNativePtr, listener != null);
    }

    /**
     * Returns whether this feature is currently visible.
     * @return Visibility of the feature.
     */
    public boolean isVisible()
    {
        return mVisible;
    }

    // Canvas Feature Listener JNI Callbacks
    protected void onTouchEvent(final int event)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mListener != null)
                {
                    CanvasFeatureEvent evt = new CanvasFeatureEvent(CanvasFeature.this);
                    evt.setType(event);
                    mListener.onTouchEvent(evt);
                }
            }
        });

    }
}
