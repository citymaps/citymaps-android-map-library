package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 9/10/14.
 */
public interface RoutingListener
{
    /**
     * Called when the user should be notified of an instruction.
     * @param notification The notification to post to the user.
     */
    public void onRouteNotification(String notification);

    /**
     * Called when routing has begun a new instruction.
     * @param index The index of the instruction that has been started.
     */
    public void onBeginRouteInstruction(int index);

    /**
     * Called periodically to update the state of routing.
     * @param state The current state of routing.
     */
    public void onRouteStateUpdate(RouteState state);
}
