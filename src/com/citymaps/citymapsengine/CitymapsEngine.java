package com.citymaps.citymapsengine;
import android.util.Log;

public class CitymapsEngine
{
    static boolean mInitialized;

    static
    {
        try
        {
            System.loadLibrary("MapEngine");
            mInitialized = true;
        }
        catch (UnsatisfiedLinkError e)
        {
           Log.e("MapEngine", e.getMessage());
        }
     }

    static boolean init()
    {
        return mInitialized;
    }
}