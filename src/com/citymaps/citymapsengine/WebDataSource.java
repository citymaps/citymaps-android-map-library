package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.DataSourceOptions;
import com.citymaps.citymapsengine.options.WebDataSourceOptions;

import java.util.HashMap;

/** A data source which receives its data from an external web address.
 * <p>
 * The web address is determined using a token replacement techinque.  The tokens are described as follows:
 * <ul>
 *     <li>{x} - The mercator x-coordinate of the tile</li>
 *     <li>{y} - The mercator y-coordinate of the tile</li>
 *     <li>{zoom} - The zoom level of the tile</li>
 * </ul>
 * <p>
 * An example URL:
 *
 * http://www.example.com/tiles/{zoom}/{x}/{y}.png
 * <p>
 * The following options are available at construction:
 *
 * <ul>
 *     <li>url - The tokenized tile URL for the data source.</li>
 * </ul>
 * <p>
 * See {@link DataSource} for more information about data sources in general, and for additional avaiable options.
 */
public class WebDataSource extends DataSource
{
    protected class WebDataSourceDescription extends DataSourceDescription
    {
        String url;
    }

    private String mURL;

    private native long nativeCreate(Object desc);
    private native void nativeSetURL(long ptr, String url);

    public WebDataSource(String url)
    {
        super(new WebDataSourceOptions(url));
    }

    public WebDataSource(WebDataSourceOptions options)
    {
        super(options);
    }

    @Override
    protected long createDataSource(DataSourceOptions options)
    {
        WebDataSourceDescription desc = new WebDataSourceDescription();
        this.applyOptions(options, desc);
        return nativeCreate(desc);
    }

    @Override
    protected void applyOptions(DataSourceOptions options, DataSourceDescription desc)
    {
        try {
            super.applyOptions(options, desc);
            WebDataSourceDescription webDesc = (WebDataSourceDescription) desc;
            WebDataSourceOptions webOptions = (WebDataSourceOptions)options;

            mURL = webOptions.getUrl();

            webDesc.url = mURL;

            if (webDesc.url == null)
            {
                throw new IllegalArgumentException("WebDataSource requires a valid tile URL.");
            }

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect parameter types. Must be a subclasses of WebDataSource*");
        }
    }

    public void setURL(String url)
    {
        if (url == null)
        {
            throw new IllegalArgumentException("WebDataSource requires a valid tile URL.");
        }

        mURL = url;
        nativeSetURL(this.mEnginePointer, mURL);
    }

    public String getURL() {
        return mURL;
    }
}
