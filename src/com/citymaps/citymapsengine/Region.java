package com.citymaps.citymapsengine;

/** Regions allow you to define geofencing areas on the application.
 *
 * <p>
 *     The idea of geofencing is that when a user enters or exits an area, the application will react to this in a certain way.
 *     Regions allow this by giving the application a means to track when a user enters or exits a circular area.
 *     Regions are defined by a longitude/latitude location and a radius.  Different shapes may be available in the future.
 * </p>
 */
public class Region {
    private boolean mActive;
    private LonLat mCenter;
    private double mRadius;
    private Object mIdentifier;
    private String mRegionDescription;

    /** Construct a new region with a center and radius
     *
     * @param center The longitude/latitude center of this region
     * @param radius The radius of this region in meters.
     */

    public Region(LonLat center, double radius) {
        mActive = true;
        mCenter = new LonLat(center.longitude, center.latitude);
        mRadius = radius;
        mIdentifier = null;
        mRegionDescription = new String();
    }

    /** Check whether the region contains a location.
     *
     * @param point The location to check against the region.
     * @return Whether the point is contained in the region.
     */

    public boolean containsPoint(LonLat point) {
        return MapUtil.getDistanceMeters(mCenter, point) <= mRadius;
    }

    /** Whether the region is active.
     *
     * @return The current status of the region.
     */

    public boolean isActive() {
        return mActive;
    }

    /** Set the status of the region.
     *
     * @param active The new status.
     */

    public void setActive(boolean active) {
        mActive = active;
    }

    /** Get the center position of the region.
     *
     * @return The region's current center position.
     */

    public LonLat getCenter() {
        return mCenter;
    }

    /** Set the center position of the region
     *
     * @param center The new center position of the region.
     */

    public void setCenter(LonLat center) {
        mCenter = center;
    }

    /** Get the radius of the region, in meters.
     *
     * @return The region's current radius, in meters.
     */

    public double getRadius() {
        return mRadius;
    }

    /** Set the radius of the region, in meters.
     *
     * @param radius The new radius of the region, in meters.
     */

    public void setRadius(double radius) {
        mRadius = radius;
    }

    /** Get the user-defined identifier object.
     *
     * @return The current identifier object.
     */

    public Object getIdentifier() {
        return mIdentifier;
    }

    /** Set the user-defined identifier object.
     *
     * @param identifier The new identifier object.
     */

    public void setIdentifier(Object identifier) {
        mIdentifier = identifier;
    }

    /** Get the user-defined region description.
     *
     * @return The current region description.
     */

    public String getRegionDescription() {
        return mRegionDescription;
    }

    /** Set the user-defined region description.
     *
     * @param regionDescription The new region description.
     */

    public void setRegionDescription(String regionDescription) {
        mRegionDescription = regionDescription;
    }
}
