package com.citymaps.citymapsengine;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class ResourceManager {

    public static String getCacheDir() {
        String internalPath = WindowAndroid.getApplicationContext().getFilesDir().getAbsolutePath();
        Log.i("MapEngine", "Internal storage path: " + internalPath);
        return internalPath;

    }

    public static byte[] loadResource(String file) {

        Context context = WindowAndroid.getApplicationContext();
        InputStream raw;
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {
            int extensionStartIndex = file.lastIndexOf('.');
            if (extensionStartIndex >= 0) {
                file = file.substring(0, extensionStartIndex);
            }

            int identifier = context.getResources().getIdentifier(file, "raw", context.getPackageName());

            raw = context.getResources().openRawResource(identifier);

            int numBytesRead = 0;

            byte[] fileBytes = new byte[1000];

            while ((numBytesRead = raw.read(fileBytes)) != -1)
            {
                output.write(fileBytes, 0, numBytesRead);
            }

            // File contents should be read.
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.i("tag", "Couldn't load file: " + file);
            e.printStackTrace();
        }
        return output.toByteArray();
    }

    /**
     * This function returns a bitmap to the asset specified by a resource ID
     *
     * @param resourceId The asset to load
     * @return a Bitmap containing the resource image data. See above for details.
     * @throws IOException
     */
    public static Bitmap loadImageResourceBitmap(int resourceId) {

        Context context = WindowAndroid.getApplicationContext();

        int dpiLevel = getDPILevel();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inDither = false;
        options.inTargetDensity = dpiLevel;
        options.inScreenDensity = dpiLevel;
        options.inScaled = true;

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);
        return bitmap;
    }

    /**
     * This function returns a bitmap to the asset specified by file.
     *
     * @param file The asset to load
     * @return a Bitmap containing the resource image data. See above for details.
     * @throws IOException
     */
    public static Bitmap loadImageResourceBitmap(String file) {

        Context context = WindowAndroid.getApplicationContext();
        int extensionStartIndex = file.lastIndexOf('.');
        if (extensionStartIndex >= 0) {
            file = file.substring(0, extensionStartIndex);
        }

        int identifier = context.getResources().getIdentifier(file, "drawable", context.getPackageName());

        Bitmap bitmap = loadImageResourceBitmap(identifier);

        return bitmap;
    }

    /**
     * This function returns a bitmap to the asset specified by file represented as an int array.
     * Indices 0 and 1 of the int[] are the display width and height of the bitmap.
     * Index 2 is the content scale of the bitmap.
     * The rest of the array is the pixel data in RGBA format.
     *
     * @param file The asset to load
     * @return and int[] containing the bitmap data. See above for details.
     * @throws IOException
     */
    public static int[] loadImageResource(String file) {

        int dpiLevel = getDPILevel();

        Bitmap bitmap = loadImageResourceBitmap(file);
        if (bitmap == null)
        {
            return null;
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size + 3];
        pixels[0] = width;
        pixels[1] = height;
        pixels[2] = (int)(100 * getScale(dpiLevel));

        bitmap.getPixels(pixels, 3, width, 0, 0, width, height);
        return pixels;
    }

    public static int getDPILevel() {
        int densityDPI = WindowAndroid.getApplicationContext().getResources().getDisplayMetrics().densityDpi;
        if (densityDPI <= 160)
            return 160;
        if (densityDPI <= 240)
            return 240;
        if (densityDPI <= 320)
            return 320;

        return 480;
    }

    public static float getScale(int dpiLevel) {
        switch (dpiLevel) {
            case 160:
                return 1.0f;
            case 240:
                return 1.5f;
            case 320:
                return 2.0f;
            case 480:
                return 3.0f;
            default:
                return 1.0f;
        }
    }
}
