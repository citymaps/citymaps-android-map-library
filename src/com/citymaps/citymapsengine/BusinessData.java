package com.citymaps.citymapsengine;

import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import com.citymaps.citymapsengine.events.BusinessEvent;
import com.citymaps.citymapsengine.events.BusinessTouchEvent;
import com.citymaps.citymapsengine.events.BusinessZoneEvent;

/** A description of a Citymaps business.

 Used by CEBusinessFilter to describe a business within a filter.  Also used by CEBusinessDelegate to return information when a business receives an event.
 */
public class BusinessData implements Parcelable
{
    private String mBusinessId = "";
    private String mName = "";
    private String mPhoneNumber = "";
    private String mAddress = "";
    private String mCity = "";
    private String mState = "";
    private int mLogoId = 0;
    private int mCategoryIconId = 0;
    private int mCategoryId = 0;
    private LonLat mLocation = new LonLat(0,0);
    private int mPartner = 0;

    public BusinessData() {
    }

    /**
     * Constructs a new fully complete BusinessData.
     * @param businessId Unique business ID.
     * @param name Name of the business.
     * @param phoneNumber Phone number of the business
     * @param address Address of the business.
     * @param city City of the business.
     * @param state State of the business.
     * @param logoId LogoID of the business. Zero is reserved for no icon.
     * @param categoryIconId Category icon ID of the business. Used if logoId is zero.
     * @param categoryId CategoryID of the business.
     * @param location Longitude and Latitude of the business.
     */
    public BusinessData(String businessId, String name, String phoneNumber, String address, String city, String state, int logoId, int categoryIconId, int categoryId, LonLat location, int partner) {
        this.setBusinessId(businessId);
        this.setName(name);
        this.setPhoneNumber(phoneNumber);
        this.setAddress(address);
        this.setCity(city);
        this.setState(state);
        this.setLogoId(logoId);
        this.setCategoryIconId(categoryIconId);
        this.setCategoryId(categoryId);
        this.setLocation(location);
        this.setPartner(partner);
    }

    BusinessData(String csv, int logoId, int categoryIconId, int categoryId, double lon, double lat, int partner)
    {
        String[] values = csv.split(",");
        mBusinessId = values[0];
        mName = values[1];
        mPhoneNumber = values[2];
        mAddress = values[3];
        mCity = values[4];
        mState = values[5];

        mLogoId = logoId;
        mCategoryIconId = categoryIconId;
        mCategoryId = categoryId;
        mLocation = new LonLat(lon, lat);
        mPartner = partner;
    }

    public BusinessData(BusinessData other)
    {
        this.setBusinessId(new String(other.mBusinessId));
        this.setName(new String(other.mName));
        this.setAddress(new String(other.mAddress));
        this.setCity(new String(other.mCity));
        this.setState(new String(other.mState));
        this.setLogoId(other.mLogoId);
        this.setCategoryIconId(other.mCategoryIconId);
        this.setCategoryId(other.mCategoryId);
        this.setLocation(other.mLocation);
        this.setPartner(other.mPartner);
    }

    private BusinessData(Parcel in) {
        mBusinessId = in.readString();
        mName = in.readString();
        mPhoneNumber = in.readString();
        mAddress = in.readString();
        mCity = in.readString();
        mState = in.readString();
        mLogoId = in.readInt();
        mCategoryIconId = in.readInt();
        mCategoryId = in.readInt();
        mLocation = in.readParcelable(LonLat.class.getClassLoader());
        mPartner = in.readInt();
    }

    /** The Citymaps business ID. */
    public String getBusinessId() {
        return mBusinessId;
    }

    /** The display name of the business.  */
    public String getName() {
        return mName;
    }

    /** The phone number of the business. */
    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    /** Street address of the business */
    public String getAddress() {
        return mAddress;
    }

    /** City of the business. */
    public String getCity() {
        return mCity;
    }

    /** State or province of the business. */
    public String getState() {
        return mState;
    }

    /** The Citymaps logo image identifier */
    public int getLogoId() {
        return mLogoId;
    }

    /** The Citymaps category icon identifer. */
    public int getCategoryIconId() { return mCategoryIconId;}

    /** The Citymaps category identifier. */
    public int getCategoryId() {
        return mCategoryId;
    }

    /** The location of the business. */
    public LonLat getLocation() {
        return new LonLat(mLocation.longitude, mLocation.latitude);
    }

    public int getPartner() { return mPartner;}

    /** The Citymaps business ID. */
    public void setBusinessId(String businessId) {
        this.mBusinessId = businessId == null ? "" : businessId;
    }

    /** The display name of the business.  */
    public void setName(String name) {
        this.mName = name == null ? "" : name;
    }

    /** The phone number of the business.  */
    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber == null ? "" : phoneNumber;
    }

    /** Street address of the business */
    public void setAddress(String address) {
        this.mAddress = address == null ? "" : address;
    }

    /** City of the business. */
    public void setCity(String city) {
        this.mCity = city == null ? "" : city;
    }

    /** State or province of the business. */
    public void setState(String state) {
        this.mState = state == null ? "" : state;
    }

    /** The Citymaps logo image identifier */
    public void setLogoId(int logoId) {
        this.mLogoId = logoId;
    }

    /** The Citymaps category identifier. */
    public void setCategoryId(int categoryId) {
        this.mCategoryId = categoryId;
    }

    /** The Citymaps category icon identifier. */
    public void setCategoryIconId(int categoryIconId) { this.mCategoryIconId = categoryIconId;}

    /** The location of the business. */
    public void setLocation(LonLat location) {
        this.mLocation = location == null ? new LonLat(0.0f, 0.0f) : location;
    }

    public void setPartner(int partner)
    {
        this.mPartner = partner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mBusinessId);
        dest.writeString(mName);
        dest.writeString(mPhoneNumber);
        dest.writeString(mAddress);
        dest.writeString(mCity);
        dest.writeString(mState);
        dest.writeInt(mLogoId);
        dest.writeInt(mCategoryIconId);
        dest.writeInt(mCategoryId);
        dest.writeParcelable(mLocation, 0);
        dest.writeInt(mPartner);
    }

    public static final Creator<BusinessData> CREATOR = new Creator<BusinessData>() {
        @Override
        public BusinessData createFromParcel(Parcel source) {
            return new BusinessData(source);
        }

        @Override
        public BusinessData[] newArray(int size) {
            return new BusinessData[size];
        }
    };
}
