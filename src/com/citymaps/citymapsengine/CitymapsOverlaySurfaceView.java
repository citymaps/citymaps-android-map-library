package com.citymaps.citymapsengine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 12/3/13
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class CitymapsOverlaySurfaceView extends SurfaceView implements SurfaceHolder.Callback
{
    private boolean mCanvasReady = false;

    public CitymapsOverlaySurfaceView(Context context)
    {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.setZOrderMediaOverlay(true);
        holder.setFormat(PixelFormat.TRANSLUCENT);
    }

    public Canvas lockCanvas()
    {
        if (mCanvasReady)
        {
            SurfaceHolder holder = getHolder();
            return holder.lockCanvas();
        }

        return null;
    }

    public void unlockCanvas(Canvas canvas)
    {
        if (canvas != null && mCanvasReady)
        {
            SurfaceHolder holder = getHolder();
            holder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCanvasReady = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCanvasReady = false;
    }
}
