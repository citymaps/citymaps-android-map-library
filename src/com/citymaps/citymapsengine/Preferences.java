package com.citymaps.citymapsengine;

public class Preferences {
    public static native int getInt(String key, int defaultValue);

    public static native float getFloat(String key, float defaultValue);

    public static native boolean getBoolean(String key, boolean defaultValue);

    public static native String getString(String key, String defaultValue);

    public static native int setInt(String key, int value);

    public static native float setFloat(String key, float value);

    public static native boolean setBoolean(String key, boolean value);

    public static native String setString(String key, String defaultValue);

    public static native void syncronize();
}
