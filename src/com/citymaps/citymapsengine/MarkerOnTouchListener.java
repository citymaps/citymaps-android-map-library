package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.events.MarkerEvent;

/** This listener used by {@link Marker} which will be notified when the user touches on a marker. */

public interface MarkerOnTouchListener {

    /** Notifies the listener when a touch event occurs on a marker
     *
     * @param event The touched marker.
     * @return whether the event was consumed
     */

    public boolean onTouchEvent(final MarkerEvent event);
}
