package com.citymaps.citymapsengine;

/**
 * An exception thrown by {@link MapView} and its subclasses when the map is used before {@link com.citymaps.citymapsengine.MapViewReadyListener#onMapViewReady(MapView)} has been called
 */

public class MapViewNotReadyException extends RuntimeException {
    public MapViewNotReadyException() {
        super("MapView was not yet ready. Please do not use MapView until you receive the onMapViewReady method");
    }
}
