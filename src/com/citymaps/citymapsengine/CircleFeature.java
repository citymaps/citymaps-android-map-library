package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.CircleFeatureOptions;

/** This class represents a user defined circle to be placed on a {@link CanvasLayer}.
 * @see CanvasFeature
 */
public class CircleFeature extends CanvasFeature {

    private LonLat mCenter = new LonLat(0,0);
    private float mRadius = 5;

    public CircleFeature(CircleFeatureOptions options) {
        super(options);
        this.setCenter(options.getCenter());
        this.setRadius(options.getRadius());
    }

    /**
     * Creates a new CircleFeature at the given point with the given radius
     * @param point Center of the circle.
     * @param radius Radius of the circle.
     */
    public CircleFeature(LonLat point, float radius)
    {
        this(new CircleFeatureOptions().center(point).radius(radius));
    }

    public CircleFeature()
    {
        this(new CircleFeatureOptions());
    }

    private native void nativeSetPosition(long ptr, double x, double y);
    private native void nativeSetRadius(long ptr, float radius);

    protected long createFeature() {
        long ptr = this.nativeCreateShape(CanvasFeatureType.CanvasFeatureCircle.getValue());
        return ptr;
    }

    /** Sets the center of the circle. */
    public void setCenter(LonLat point)
    {
        mCenter = new LonLat(point);
        this.nativeSetPosition(mNativePtr, point.longitude, point.latitude);
    }

    /** Sets the radius of the circle. */
    public void setRadius(float radius)
    {
        mRadius = radius;
        this.nativeSetRadius(mNativePtr, radius);
    }

    /** Returns the center of the circle. */
    public LonLat getPosition() {
        return new LonLat(mCenter);
    }

    /** Returns the radius of the circle. */
    public float getRadius() {
        return mRadius;
    }

}
