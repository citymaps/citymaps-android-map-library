package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.LayerOptions;
import com.citymaps.citymapsengine.options.RegionLayerOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/** This layer is used to render Citymaps regions at higher zoom levels.  Citymaps regions identify cities and towns across the USA with a unique marker.
 *
 * <p>See {@link TileLayer}.</p>
 *
 */

public class RegionLayer extends TileLayer {

    /*
     * JNI Native methods.
	 * Defined in CitymapsMapView.cpp
	 */
    private native long nativeCreateLayer(TileLayerDescription desc);

    public RegionLayer(LayerOptions options) {
        super(options);
    }

    public RegionLayer(String apiKey)
    {
        super(new RegionLayerOptions(apiKey));
    }

    @Override
    protected long createLayer(LayerOptions options) {
        RegionLayerDescription desc = new RegionLayerDescription();
        this.applyOptions(options, desc);

        return this.nativeCreateLayer(desc);
    }

    @Override
    protected void applyOptions(LayerOptions options, LayerDescription desc)
            throws IllegalArgumentException {
        try {
            super.applyOptions(options, desc);
            RegionLayerDescription regionDesc = (RegionLayerDescription) desc;
            RegionLayerOptions regionOptions = (RegionLayerOptions)options;

            regionDesc.imageHostname = regionOptions.getHostname();
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect layer description type.  Must be a subclass of RegionLayerDescription");
        }
    }

    protected class RegionLayerDescription extends TileLayerDescription {
        String imageHostname = "r.citymaps.com";
    }
}
