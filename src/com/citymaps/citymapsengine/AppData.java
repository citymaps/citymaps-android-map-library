package com.citymaps.citymapsengine;

/**
 * A utility class for retrieving information about the application
 */

public class AppData
{
    /** Get the application's package name
     *
     * @return the application package name
     */
    public static String getPackageName()
    {
        return WindowAndroid.getApplicationContext().getPackageName();
    }
}
