package com.citymaps.citymapsengine;

import android.view.MotionEvent;
import com.citymaps.citymapsengine.events.CanvasFeatureEvent;

/**
 * Created by adam on 5/8/14.
 */
public interface CanvasFeatureOnTouchListener
{
    /** Notifies the listener when a user touches up on a marker
     *
     * @param event The touch event object.
     * @return whether the event was consumed
     */

    public boolean onTouchEvent(final CanvasFeatureEvent event);
}
