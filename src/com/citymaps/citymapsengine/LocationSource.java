package com.citymaps.citymapsengine;

import android.location.Location;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 11/22/13
 * Time: 10:05 AM
 * To change this template use File | Settings | File Templates.
 */
public interface LocationSource
{
    public static interface LocationChangeListener
    {
        public void onLocationChanged(Location location);
    }

    public void activate(LocationChangeListener mListener);

    public void deactivate();

    public Location getLastKnownLocation();
}
