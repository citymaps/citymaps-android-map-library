package com.citymaps.citymapsengine;

/** This interface is used by {@link MapView} to receive events when an animation is complete.
 *
 */

public interface MapViewAnimationListener {

    /** Notifies the listener when the animation is complete
     * @param completed True if the animation was completed, false if it was cancelled.
     * */
    public void onAnimationEnd(boolean completed);
}
