package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.events.MapEvent;
import com.citymaps.citymapsengine.events.MapTouchEvent;

/** This listener is used by {@link MapView} to notify the application when certain events occur on the map */

public interface MapViewListener
{
    /**
     * Notifies the listener when a map event occurs
     *
     * @param event The map event
     */
    public void onMapEvent(final MapEvent event);

    /**
     * Notifies the listener when the user touches the map
     *
     * @param event The map touch event
     */
    public void onTouchEvent(final MapTouchEvent event);

    /**
     * Notifies the listener when the device's GPS information changes.
     *
     * @param location The new user location
     * */
    public void onUserLocationChange(final UserLocation location);

    /** Notifies the listener when the device's GPS location enters a geofencing region
     *
     * @see Region
     */
    public void onEnterRegion(int index);

    /** Notifies the listener when the device's GPS location exits a geofencing region
     *
     * @see Region
     */
    public void onExitRegion(int index);
}
