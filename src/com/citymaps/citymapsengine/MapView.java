package com.citymaps.citymapsengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.citymaps.citymapsengine.events.MapEvent;
import com.citymaps.citymapsengine.events.MapTouchEvent;
import com.citymaps.citymapsengine.options.MapViewOptions;

/**
 * This is the main class for representing a map within the view hierarchy.
 *
 * The following Android lifecycle methods must be called on MapView if using it directly.
 * <ul>
 *      <li>onCreate(Bundle)</li>
 *      <li>onResume()</li>
 *      <li>onPause()</li>
 *      <li>onDestroy()</li>
 *      <li>onSaveInstanceState(Bundle)</li>
 *      <li>onLowMemory()</li>
 * </ul>
 */

public class MapView extends WindowAndroid
{
    static
    {
        CitymapsEngine.init();
    }

    public static final float kMapViewCurrentZoom = -1.f;

    public static final int kDefaultAnimationDuration = 250;
    private static final String kDefaultMarkerGroup = "CitymapsDefaultMarkerGroup";

    public static final int MAP_OPTION_KINETIC_PAN = 0;
    public static final int MAP_OPTION_KINETIC_ZOOM = 1;
    public static final int MAP_OPTION_KINETIC_ROTATE = 2;
    public static final int MAP_OPTION_ANIMATION = 3;
    public static final int MAP_OPTION_UPDATE = 4;
    public static final int MAP_OPTION_ROTATION = 5;
    public static final int MAP_OPTION_PAN_GESTURE = 6;
    public static final int MAP_OPTION_ZOOM_GESTURE = 7;
    public static final int MAP_OPTION_ROTATE_GESTURE = 8;
    public static final int MAP_OPTION_TILT_GESTURE = 9;
    public static final int MAP_OPTION_MAX_VALUE = 10;

    public static final int MAP_OPTION_VALUE_OFF = 0;
    public static final int MAP_OPTION_VALUE_ON = 1;
    public static final int MAP_OPTION_VALUE_LOW = 2;
    public static final int MAP_OPTION_VALUE_MEDIUM = 3;
    public static final int MAP_OPTION_VALUE_HIGH = 4;
    public static final int MAP_OPTION_VALUE_MAX_VALUE = 4;

    public static final int MAP_TRACKING_MODE_NONE = 0;
    public static final int MAP_TRACKING_MODE_POSITION = 1;
    public static final int MAP_TRACKING_MODE_POSITION_ORIENTATION = 2;

    public static final int MAP_CAMERA_MODE_2D = 0;
    public static final int MAP_CAMERA_MODE_3D = 1;

    public static final int MAP_PROJECTION_MERCATOR = 0;
    public static final int MAP_PROJECTION_LONLAT = 1;

    public static final float MAX_TILT = 65.0f;

    protected class MapViewImpl
    {
        protected MapViewOptions mOptions;
        protected Layer mBaseLayer;
        protected ArrayList<Layer> mLayers;
        protected ArrayList<Region> mRegions;
        protected ArrayList<CanvasFeature> mFeatures;
        protected long mMapPointer;
        protected HashMap<String, MarkerGroup> mMarkerGroups;
        protected HashMap<String, FeatureGroup> mFeatureGroups;
        protected HashMap<Integer, Integer> mMapOptions;
        protected Size mSize = new Size(100, 100);

        @Override
        protected void finalize() {
            if (mMapPointer != 0) {
                for (Layer layer : mLayers) {
                    layer.onDestroy();
                }
                Log.i("MapEngine", "Deleting Map");
                nativeRelease(mMapPointer);
            }
        }
    }

    private ArrayList<Layer> mLayers;
    private ArrayList<Region> mRegions;
    private ArrayList<CanvasFeature> mFeatures;

    private HashMap<String, MarkerGroup> mMarkerGroups;
    private HashMap<String, FeatureGroup> mFeatureGroups;

    private MapViewListener mMapListener = null;
    private MapViewIdleListener mMapViewIdleListener = null;

    protected static MapViewImpl sImpl = null;

    private float mMinZoom = 3;
    private float mMaxZoom = 30;

	/*
     * JNI Native methods.
	 * Defined in MapView.cpp
	 */

    private native long nativeInit(long mapPtr);
    private native void nativeCreateMapListener(long mapPtr);
    private native void nativeEnable(long mapPtr, long appPtr);
    private native void nativeDisable(long mapPtr);
    private native long nativeCreateMap(MapDescription desc);
    private native void nativeRelease(long mapPtr);
    private native void nativeInvalidate(long mapPtr);

    // Native Layers
    private native void nativeAddLayer(long mapPtr, long layerPtr);
    private native void nativeSetBaseLayer(long mapPtr, long layerPtr);
    private native void nativeRemoveLayer(long mapPtr, long layerPtr);

    // Native Setters
    private native void nativeSetLocationMarkerEnabled(long mapPtr, boolean enabled);
    private native void nativeSetCenter(long mapPtr, double lon, double lat, float zoom, double duration, Object listener);
    private native void nativeSetZoom(long mapPointer, float zoom, double duration, Object listener);
    private native void nativeSetMapPosition(long mapPointer, double longitude, double latitude, float zoom,
                                             double orientation, double tilt, double duration, Object listener);
    private native void nativeZoomIn(long mapPointer, double duration);
    private native void nativeZoomOut(long mapPointer, double duration);
    private native void nativeSetOrientation(long mapPointer, double orientation, double duration, Object listener);

    private native void nativeSetTrackingMode(long mapPointer, int trackingMode);
    private native void nativeZoomToBounds(long mapPointer, double minLon, double minLat, double maxLon, double maxLat, int padding, double duration, Object listener);
    private native void nativeZoomToBoundsInBounds(long mapPointer, double minLon, double minLat, double maxLon, double maxLat, int width, int height, int padding, double duration, Object listener);
    private native float nativeZoomContainingBounds(long mapPonter, double minLon, double minLat, double maxLon, double maxLat);

    private native void nativeSetTilt(long mapPointer, double tilt);
    private native void nativeZoomTowardsPoint(long mapPointer, double x, double y, double duration, double zoomDelta, Object listener);

    // Native Utils
    private native void nativeMoveByPixels(long mapPtr, double x, double y, double duration, Object listener);
    private native void nativeMovePixelToPixel(long mapPtr, double x1, double y1, double x2, double y2, double duration, Object listener);
    private native void nativeMoveToScreenPosition(long mapPtr, double x, double y, double duration, Object listener);
    private native PointF nativeProject(long mapPtr, double lon, double lat);
    private native LonLat nativeUnproject(long mapPtr, double x, double y);
    private native void nativeSetMinZoom(long mapPtr, float zoom);
    private native void nativeSetMaxZoom(long mapPtr, float zoom);
    private native void nativeSetMapOption(long mapPtr, int option, int value);
    private native void nativeStopAnimations(long mapPtr);

    // Native Getters
    private native LonLat nativeGetCenter(long mapPtr);
    private native float nativeGetZoom(long mapPtr);
    private native double nativeGetResolution(long mapPtr);
    private native long nativeGetCameraSnapshot(long mapPtr);
    private native long nativeGetProjection(long mapPtr);

    private native double nativeGetOrientation(long mapPointer);
    private native UserLocation nativeGetUserLocation(long mapPointer);
    private native int nativeGetTrackingMode(long mapPointer);
    private native LonLatBounds nativeGetExtents(long mapPointer);
    private native double nativeGetTilt(long mapPointer);
    private native boolean nativeIsAnimating(long mapPointer);

    // Native Markers
    private native long nativeGetMarkerGroup(long mapPointer, String name);

    // Native Canvas
    private native void nativeAddCanvasFeature(long mapPointer, long featurePtr);
    private native void nativeRemoveCanvasFeature(long mapPointer, long featurePtr);
    private native void nativeRemoveAllCanvasFeatures(long mapPointer);
    private native long nativeGetCanvasFeatureGroup(long mapPointer, String name);

    // Native Regions
    private native void nativeAddRegion(long mapPointer, double lon, double lat, double radius);
    private native void nativeRemoveRegion(long mapPointer, int index);
    private native void nativeRemoveAllRegions(long mapPointer);

    private native void nativeSetPadding(long mapPointer, int left, int top, int right, int bottom);

    private native void nativeResetToPosition(long mapPointer, double longitude, double latitude, float zoom, double orientation, double tilt);

    /** Initialize a new map
     *
     * @param context The Android Context associated with this view
     * @param options An HashMap of options for the map. Accepted options are:
     *
     * <ul>
     *      <li>center: The default center of the map.</li>
     *      <li>zoom: The default zoom of the map.</li>
     *      <li>size: The pixel size of the map.</li>
     *      <li>minZoom: The lower bound (most zoomed out) the map can zoom to.</li>
     *      <li>maxZoom: The upper bound (most zoomed in) the map can zoom to.</li>
     * </ul>
     */
    public MapView(Context context, MapViewOptions options) {
        super(context);

        if (options == null)
            options = new MapViewOptions();

        mMapListener = null;

        if (sImpl == null)
        {
            sImpl = new MapViewImpl();
            sImpl.mFeatures = new ArrayList<CanvasFeature>();
            sImpl.mMarkerGroups = new HashMap<String, MarkerGroup>();
            sImpl.mFeatureGroups = new HashMap<String, FeatureGroup>();
            sImpl.mMapOptions = new HashMap<Integer, Integer>();
            sImpl.mOptions = options;
            sImpl.mBaseLayer = null;
            sImpl.mLayers = new ArrayList<Layer>();
            sImpl.mRegions = new ArrayList<Region>();
        }
        else
        {
            sImpl.mOptions = options;
        }

        mMarkerGroups = sImpl.mMarkerGroups;
        mFeatureGroups = sImpl.mFeatureGroups;
        mLayers = sImpl.mLayers;
        mRegions = sImpl.mRegions;
        mFeatures = sImpl.mFeatures;

        for(MarkerGroup group : mMarkerGroups.values())
        {
            group.setMap(this);
        }

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.onSizeChanged(size.x, size.y);
    }

    protected long createMap(MapViewOptions options) {
        MapDescription desc = new MapDescription();
        this.applyOptions(options, desc);

        return nativeCreateMap(desc);
    }

    protected MapViewOptions getOptions()
    {
        return sImpl.mOptions;
    }

    protected void applyOptions(MapViewOptions options, MapDescription desc)
    {
        desc.center = options.getPosition().center;
        desc.zoom = options.getPosition().zoom;
        desc.rotation = options.getPosition().orientation;
        desc.tilt = options.getPosition().tilt;
        desc.mapSize = sImpl.mSize;
        desc.minZoom = options.getMinZoom();
        desc.maxZoom = options.getMaxZoom();
        desc.projection = MapView.MAP_PROJECTION_MERCATOR;
    }

    /**
     * Sets whether to display the user location marker and radius.
     * @param enabled true to display the user location marker, false to hide it.
     */
    public void setLocationMarkerEnabled(boolean enabled)
    {
        this.nativeSetLocationMarkerEnabled(sImpl.mMapPointer, enabled);
    }

    /** Sets the base layer for the map. This layer is always rendered first.
     * @param baseLayer The base layer for this map. It should not already be added to the map.
     */
    public void setBaseLayer(Layer baseLayer)
    {
        if (sImpl.mBaseLayer == baseLayer)
            return;

        sImpl.mBaseLayer = baseLayer;
        if (sImpl.mMapPointer != 0)
        {
            if (baseLayer != null) {
                baseLayer.setVisible(true);
                nativeSetBaseLayer(sImpl.mMapPointer, baseLayer.mNativePtr);
            }
        }

        if (baseLayer == null)
            return;

        if (!mLayers.contains(baseLayer))
        {
            mLayers.add(baseLayer);
        }
    }

    /** Returns the current base layer, or null if there is none.
     *
     * @return The current base layer, or null if there is none.
     */
    public Layer getBaseLayer()
    {
        return sImpl.mBaseLayer;
    }

    /** Add a {@link Layer} to the map
     *
     * @param layer The layer to be added
     */

    public void addLayer(Layer layer)
    {
        synchronized (mLayers)
        {
            mLayers.add(layer);
        }

        if (sImpl.mMapPointer != 0) {
            this.nativeAddLayer(sImpl.mMapPointer, layer.mNativePtr);
        }
    }

    /** Remove a {@link Layer} from the map
     *
     * @param layer The layer to be removed
     */

    public void removeLayer(Layer layer)
    {
        synchronized (mLayers)
        {
            mLayers.remove(layer);
        }

        if (layer == sImpl.mBaseLayer)
        {
            sImpl.mBaseLayer = null;
        }

        if (sImpl.mMapPointer != 0) {
            this.nativeRemoveLayer(sImpl.mMapPointer, layer.mNativePtr);
        }
    }

    /** Add a feature to the default canvas layer
     *
     *@param feature - The feature to add
     */
    public void addFeature(CanvasFeature feature)
    {
        if (sImpl.mMapPointer == 0) {
            throw new MapViewNotReadyException();
        }

        mFeatures.add(feature);
        nativeAddCanvasFeature(sImpl.mMapPointer, feature.mNativePtr);
    }

    /** Remove a feature from the default canvas layer
     *
     *@param feature - The feature to remove
     */
    public void removeFeature(CanvasFeature feature)
    {
        if (sImpl.mMapPointer == 0) {
            throw new MapViewNotReadyException();
        }

        mFeatures.remove(feature);
        nativeRemoveCanvasFeature(sImpl.mMapPointer, feature.mNativePtr);
    }

    /** Remove all features from the default canvas layer */
    public void removeAllFeatures()
    {
        if (sImpl.mMapPointer == 0) {
            throw new MapViewNotReadyException();
        }

        mFeatures.clear();
        nativeRemoveAllCanvasFeatures(sImpl.mMapPointer);
    }

    /** Get a feature group with the given name from the default canvas layer.  If it does not exist, it will be created.
     *
     *@param name - The name of the feature group
     */
    public FeatureGroup getFeatureGroup(String name)
    {
        if (sImpl.mMapPointer == 0) {
            throw new MapViewNotReadyException();
        }

        if (mFeatureGroups.containsKey(name))
        {
            return mFeatureGroups.get(name);
        }

        long nativePointer = nativeGetCanvasFeatureGroup(sImpl.mMapPointer, name);
        FeatureGroup featureGroup = new FeatureGroup(nativePointer, name);
        mFeatureGroups.put(name, featureGroup);
        return featureGroup;
    }

    /** Add a geofencing region.
     *
     * @param region The region to be added
     */
    public void addRegion(Region region) {
        mRegions.add(region);

        if (sImpl.mMapPointer != 0) {
            nativeAddRegion(sImpl.mMapPointer, region.getCenter().longitude, region.getCenter().latitude, region.getRadius());
        }
    }

    /** Remove a geofencing region
     *
     * @param region The region to be removed
     */
    public void removeRegion(Region region) {
        int index = mRegions.indexOf(region);
        if (index > 0) {
            mRegions.remove(index);
            if (sImpl.mMapPointer != 0) {
                nativeRemoveRegion(sImpl.mMapPointer, index);
            }
        }
    }

    /** Remove all geofencing regions
     *
     * @see Region
     * */
    public void removeAllRegions() {
        mRegions.clear();
        if (sImpl.mMapPointer != 0) {
            nativeRemoveAllRegions(sImpl.mMapPointer);
        }
    }

    /** Get a geofencing region by index
     *
     * @return The region at the specified index, or null if the index is greater than the number of regions.
     * @see Region
     * */
    public Region getRegion(int index) {
        if (index >= 0 && index < mRegions.size()) {
            return mRegions.get(index);
        }

        return null;
    }

    /** Get a list of all active geofencing regions
     *
     * @return A list of all active regions
     * @see Region
     */
    public ArrayList<Region> getRegions() {
        return new ArrayList<Region>(mRegions);
    }

    /** Gets whether the map is currently performing an animation.
     * @return Whether the map is currently performing an animation.
     */
    public boolean isAnimating()
    {
        if (sImpl.mMapPointer != 0) {
            return nativeIsAnimating(sImpl.mMapPointer);
        }
        return false;
    }

    /** Set a map behavior option.
     *
     * Available map options are:
     *
     * <ul>
     *     <li>{@link MapView#MAP_OPTION_KINETIC_PAN}: Controls the kinetic movement of the map on a pan gesture. Should be set to either {@link MapView#MAP_OPTION_VALUE_OFF} or {@link MapView#MAP_OPTION_VALUE_ON} </li>
     *     <li>{@link MapView#MAP_OPTION_KINETIC_ZOOM}: Controls the kinetic movement of the map on a zoom gesture. Should be set to either {@link MapView#MAP_OPTION_VALUE_OFF} or {@link MapView#MAP_OPTION_VALUE_ON}.</li>
     *     <li>{@link MapView#MAP_OPTION_ANIMATION}: Controls the animated movements of the map, invoked by the application. Should be set to either {@link MapView#MAP_OPTION_VALUE_OFF} or {@link MapView#MAP_OPTION_VALUE_ON}.</li>
     *     <li>{@link MapView#MAP_OPTION_UPDATE}: Controls the amount of dynamic processing done by the map. Should be set to {@link MapView#MAP_OPTION_VALUE_LOW}, {@link MapView#MAP_OPTION_VALUE_MEDIUM}, or {@link MapView#MAP_OPTION_VALUE_HIGH}.</li>
     *     <li>{@link MapView#MAP_OPTION_ROTATION}: Controls the ability to orient the map. Should be set to either {@link MapView#MAP_OPTION_VALUE_OFF} or {@link MapView#MAP_OPTION_VALUE_ON}.</li>
     * </ul>
     *
     *
     * @param option The option key
     * @param value The value key
     */

    public void setMapOption(int option, int value) {
        if (option < 0 || option > MAP_OPTION_MAX_VALUE) {
            throw new IllegalArgumentException("Map Option is not a valid value");
        }
        if (value < 0 || value > MAP_OPTION_VALUE_MAX_VALUE) {
            throw new IllegalArgumentException("Map Option Value is not a valid value");
        }

        sImpl.mMapOptions.put(option, value);
        if (sImpl.mMapPointer != 0)
        {
            nativeSetMapOption(sImpl.mMapPointer, option, value);
        }
    }
    public int getMapOption(int option) {
        if(sImpl.mMapOptions.containsKey(option)) {
            return sImpl.mMapOptions.get(option);
        }

        return 0;
    }

/** Zooms the map to the given zoom level.
     * @param zoom Zoom level to zoom to.
     */
    public void zoomTo(float zoom)
    {
        this.setCenterAnimated(this.getCenter(), zoom, 0, null);
    }

    /** Zooms the map to the given zoom level.
     * @param zoom Zoom level to zoom to.
     * @param durationMs Duration of the animation.
     */
    public void zoomTo(float zoom, long durationMs)
    {
        this.setCenterAnimated(this.getCenter(), zoom, durationMs, null);
    }

    /** Zooms the map to the given zoom level.
     * @param zoom Zoom level to zoom to.
     * @param durationMs Duration of the animation.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomTo(float zoom, long durationMs, MapViewAnimationListener listener)
    {
        this.setCenterAnimated(this.getCenter(), zoom, durationMs, listener);
    }

    /** Set the map center.
     *
     * @param center The new map center.
     */
    public void setCenter(LonLat center) {
        this.setCenterAnimated(center, kMapViewCurrentZoom, 0, null);
    }

    /** Set the map center, with the option of animation.
     *
     * @param center The new map center.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void setCenterAnimated(LonLat center, long durationMs) {
        this.setCenterAnimated(center, kMapViewCurrentZoom, durationMs, null);
    }

    /** Set the map center and zoom.
     *
     * @param center The new map center.
     * @param zoom The new zoom level.
     */

    public void setCenter(LonLat center, float zoom) {
        this.setCenterAnimated(center, zoom, 0, null);
    }

    /** Set the map center, with the option of animation and a movement completion listener.
     *
     * @param center The new map center.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void setCenterAnimated(LonLat center, long durationMs, MapViewAnimationListener listener) {
        this.setCenterAnimated(center, kMapViewCurrentZoom, durationMs, listener);
    }

    /** Set the map center and zoom, with the option of animation.
     *
     * @param center The new map center.
     * @param zoom The new map zoom level.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void setCenterAnimated(LonLat center, float zoom, long durationMs) {
        this.setCenterAnimated(center, zoom, durationMs, null);
    }

    /** Set the map center and zoom, with the option of animation and a movement completion listener.
     *
     * @param center The new map center.
     * @param zoom The new zoom level.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void setCenterAnimated(LonLat center, float zoom, long durationMs, final MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeSetCenter(sImpl.mMapPointer, center.longitude, center.latitude, zoom, durationMs, new MapViewAnimationListener() {
                @Override
                public void onAnimationEnd(final boolean completed) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (listener != null) {
                                listener.onAnimationEnd(completed);
                            }
                        }
                    });
                }
            });
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Set the map zoom level.
     *
     * @param zoom The new zoom level.
     */

    public void setZoom(float zoom) {
        setZoom(zoom, 0, null);
    }

    /** Set the map zoom level with the option animation.
     *
     * @param zoom The new zoom level.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void setZoom(float zoom, long durationMs) {
        setZoom(zoom, durationMs, null);
    }

    /** Set the map zoom level with the option animation and a completion listener.
     *
     * @param zoom The new zoom level.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void setZoom(float zoom, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeSetZoom(sImpl.mMapPointer, zoom, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Move the map by a given pixel amount.
     *
     * @param point The pixel amount to move by
     */

    public void moveByPixels(PointF point) {
        this.moveByPixels(point.x, point.y);
    }

    /** Move the map by a given pixel amount.
     *
     * @param x The pixel amount to move by in the x direction.
     * @param y The pixel amount to move by in the y direction.
     */

    public void moveByPixels(double x, double y) {
        this.moveByPixels(x, y, 0, null);
    }

    /** Move the map by a given pixel amount, with the option of animation.
     *
     * @param x The pixel amount to move by in the x direction.
     * @param y The pixel amount to move by in the y direction.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void moveByPixels(double x, double y, long durationMs) {
        this.moveByPixels(x, y, durationMs, null);
    }

    /** Move the map by a given pixel amount, with the option of animation.
     *
     * @param x The pixel amount to move by in the x direction.
     * @param y The pixel amount to move by in the y direction.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void moveByPixels(double x, double y, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeMoveByPixels(sImpl.mMapPointer, x, y, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    public void movePixelToPixel(double x1, double y1, double x2, double y2)
    {
        this.movePixelToPixel(x1, y1, x2, y2, 0, null);
    }

    public void movePixelToPixel(double x1, double y1, double x2, double y2, long durationMs)
    {
        this.movePixelToPixel(x1, y1, x2, y2, durationMs, null);
    }

    public void movePixelToPixel(double x1, double y1, double x2, double y2, long durationMs, MapViewAnimationListener listener)
    {
        if (sImpl.mMapPointer != 0) {
            nativeMovePixelToPixel(sImpl.mMapPointer, x1, y1, x2, y2, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Move the map to a given screen pixel.
     *
     * <p>This method effectively moves the map to the nearest longitude/latitude location currently occupied by the given pixel.</p>
     *
     * @param point The pixel to move to.
     */

    public void moveToScreenPosition(PointF point) {
        this.moveToScreenPosition(point, 0, null);
    }

    /** Move the map to a given screen pixel, with the option of animation.
     *
     * <p>This method effectively moves the map to the nearest longitude/latitude location currently occupied by the given pixel.</p>
     *
     * @param point The pixel to move to.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void moveToScreenPosition(PointF point, long durationMs) {
        this.moveToScreenPosition(point, durationMs, null);
    }

    /** Move the map to a given screen pixel, with the option of animation and a completion listener.
     *
     * <p>This method effectively moves the map to the nearest longitude/latitude location currently occupied by the given pixel.</p>
     *
     * @param point The pixel to move to.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void moveToScreenPosition(PointF point, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeMoveToScreenPosition(sImpl.mMapPointer, point.x, point.y, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    public void setMapPosition(MapPosition position) {
        this.setMapPosition(position, 0, null);
    }

    public void setMapPosition(MapPosition position, long durationMs) {
        this.setMapPosition(position, durationMs, null);
    }

    public void setMapPosition(MapPosition position, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeSetMapPosition(sImpl.mMapPointer, position.center.longitude, position.center.latitude, position.zoom,
                    position.orientation, position.tilt, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Stops all animations.
     *
     */
    public void stopAnimations()
    {
        nativeStopAnimations(sImpl.mMapPointer);
    }

    /** Get the closest pixel to the given longitude/latitude position.
     *
     * @param lonlat longitude/latitude location.
     * @return The pixel location closest to this longitude/latitude position;
     */

    public PointF project(LonLat lonlat) {
        if (sImpl.mMapPointer != 0) {
            return nativeProject(sImpl.mMapPointer, lonlat.longitude, lonlat.latitude);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the closest longitude/latitude location to the given pixel.
     *
     * @param point The pixel location.
     * @return The longitude/latitude location closest to this pixel.
     */

    public LonLat unproject(PointF point) {
        if (sImpl.mMapPointer != 0) {
            return nativeUnproject(sImpl.mMapPointer, point.x, point.y);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Set the minimum (most zoomed out) zoom level.
     *
     * @param zoom The new minimum zoom level.
     */
    public void setMinZoom(float zoom) {
        if (sImpl.mMapPointer != 0) {
            nativeSetMinZoom(sImpl.mMapPointer, zoom);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Set the maximum (most zoomed in) zoom level
     *
     * @param zoom The new maximum zoom level.
     */
    public void setMaxZoom(float zoom) {
        if (sImpl.mMapPointer != 0) {
            nativeSetMaxZoom(sImpl.mMapPointer, zoom);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the minimum (most zoomed out) zoom level.
     *
     * @return The new minimum zoom level.
     */
    public float getMinZoom() {
        return mMinZoom;
    }

    /** Get the maximum (most zoomed in) zoom level
     *
     * @return The new maximum zoom level.
     */
    public float getMaxZoom() {
        return mMaxZoom;
    }

    /** Reset the map orientation back to true north. */

    public void resetOrientation() {
        this.setOrientation(0);
    }

    /** Reset the map orientation back to true north, with the option of animation.
     *
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void resetOrientation(long durationMs) {
        this.setOrientation(0, durationMs);
    }

    /** Reset the map orientation back to true north, with the option of animation and a completion listener.
     *
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void resetOrientation(long durationMs, MapViewAnimationListener listener) {
        this.setOrientation(0, durationMs, listener);
    }

    /**
     * Zooms the map by a given amount.
     * @param zoom Amount to zoom by.
     */
    public void zoomBy(float zoom)
    {
        this.zoomBy(zoom, 0, null);
    }

    /**
     * Zooms the map by a given amount.
     * @param zoom Amount to zoom by.
     * @param duration Duration of the animation.
     */
    public void zoomBy(float zoom, long duration)
    {
        this.zoomBy(zoom, duration, null);
    }

    /**
     * Zooms the map by a given amount.
     * @param zoom Amount to zoom by.
     * @param duration Duration of the animation.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomBy(float zoom, long duration, MapViewAnimationListener listener)
    {
        float currentZoom = this.getZoom();
        this.zoomTo(currentZoom + zoom, duration, listener);
    }

    /**
     * Zooms the map by a given amount.
     * @param position LonLat to zoom towards.
     * @param duration Duration of the animation.
     */
    public void zoomTowardsPoint(LonLat position, long duration)
    {
        this.zoomTowardsPoint(position, duration, 1.f, null);
    }

    /**
     * Zooms the map by a given amount.
     * @param position LonLat to zoom towards.
     * @param duration Duration of the animation.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomTowardsPoint(LonLat position, long duration, double zoomDelta, MapViewAnimationListener listener)
    {
        if (sImpl.mMapPointer != 0) {
            this.nativeZoomTowardsPoint(sImpl.mMapPointer, position.longitude, position.latitude, duration, zoomDelta, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Zoom the map in one zoom level.
     *
     * @param durationMs Duration of this animation in milliseconds.
     */
    public void zoomIn(long durationMs) {
        if (sImpl.mMapPointer != 0) {
            nativeZoomIn(sImpl.mMapPointer, durationMs);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Zoom the map in one zoom level.
     *
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener Callback listener for animation completion.
     */
    public void zoomIn(long durationMs, MapViewAnimationListener listener)
    {
        this.zoomBy(1.0f, durationMs, listener);
    }

    /** Zoom the map out one zoom level.
     *
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void zoomOut(long durationMs) {
        if (sImpl.mMapPointer != 0) {
            nativeZoomOut(sImpl.mMapPointer, durationMs);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Zoom the map in one zoom level.
     *
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener Callback listener for animation completion.
     */
    public void zoomOut(long durationMs, MapViewAnimationListener listener)
    {
        this.zoomBy(-1.0f, durationMs, listener);
    }

    /** Zoom the map so that the screen fully contains the bounds specified.
     *
     * @param extents The new bounds of the map
     */
    public void zoomToBounds(LonLatBounds extents) {
        this.zoomToBounds(extents, 0);
    }

    /** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
     *
     * @param extents The new bounds of the map
     * @param durationMs Duration of this animation in milliseconds.
     */
    public void zoomToBounds(LonLatBounds extents, long durationMs) {
        this.zoomToBounds(extents, 0, durationMs, null);
    }

    /** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
     *
     * @param extents The new bounds of the map
     * @param padding Additional pixel padding around the sides of the map.
     * @param durationMs Duration of this animation in milliseconds.
     */
    public void zoomToBounds(LonLatBounds extents, int padding, long durationMs) {
        this.zoomToBounds(extents, padding, durationMs, null);
    }

    /** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
     *
     * @param extents The new bounds of the map
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomToBounds(LonLatBounds extents, long durationMs, MapViewAnimationListener listener) {
        this.zoomToBounds(extents, 0, durationMs, listener);
    }

    /** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
     *
     * @param extents The new bounds of the map
     * @param padding Additional pixel padding around the sides of the map.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomToBounds(LonLatBounds extents, int padding, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeZoomToBounds(sImpl.mMapPointer, extents.min.longitude, extents.min.latitude, extents.max.longitude, extents.max.latitude, padding, durationMs, listener);
        } else
        {
            throw new MapViewNotReadyException();
        }
    }

    /** Zoom the map so that the screen fully contains the bounds specified, with the option of animation.
     *
     * @param extents The new bounds of the map.
     * @param width Width in pixels that the extents should occupy.
     * @param height Height in pixels that the extents should occupy.
     * @param padding Additional pixel padding around the sides of the map.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */
    public void zoomToBounds(LonLatBounds extents, int width, int height, int padding, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0)
        {
            nativeZoomToBoundsInBounds(sImpl.mMapPointer, extents.min.longitude, extents.min.latitude, extents.max.longitude, extents.max.latitude, width, height, padding, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the zoom level that can show the given bounds. Does not move the map.
     *
     * @param extents The bounds to test against.
     * @return The zoom level that can contian the given bounds on screen.
     */
    public float getZoomContainingBounds(LonLatBounds extents)
    {
        if (sImpl.mMapPointer != 0)
        {
            return nativeZoomContainingBounds(sImpl.mMapPointer, extents.min.longitude, extents.min.latitude, extents.max.longitude, extents.max.latitude);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Set the extends of the map.  The extents describe the current area contained on the screen by the map.
     *
     * @param extents The new map extents.
     */
    public void setExtents(LonLatBounds extents) {
        this.zoomToBounds(extents);
    }

    /** Get the current map center
     *
     * @return The current map center in longitude/latitude coordinates.
     */

    public LonLat getCenter() {
        if (sImpl.mMapPointer != 0) {
            return (LonLat) nativeGetCenter(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }

    }

    /** Get the current zoom level.
     *
     * @return The current zoom level.
     */

    public float getZoom() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetZoom(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the current map resolution (a.k.a "meters per pixel").
     *
     * @return The current map resolution.
     */

    public double getResolution() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetResolution(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the map's orientation.
     *
     * @return The map's current orientation.
     */

    public double getOrientation() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetOrientation(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the map's current extents
     *
     * @return The map's current extents.
     */

    public LonLatBounds getExtents() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetExtents(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the map's size.
     *
     * @return The size of the map.
     */

    public Size getSize() {
        return sImpl.mSize;
    }

    /** Rotate the map to a specific orientation.
     *
     * @param orientation The new orientation.
     */

    public void setOrientation(double orientation) {
        setOrientation(orientation, 0, null);
    }

    /** Rotate the map to a specific orientation.
     *
     * @param orientation The new orientation.
     * @param durationMs Duration of this animation in milliseconds.
     */

    public void setOrientation(double orientation, long durationMs) {
        setOrientation(orientation, durationMs, null);
    }

    /** Rotate the map to a specific orientation.
     *
     * @param orientation The new orientation.
     * @param durationMs Duration of this animation in milliseconds.
     * @param listener The listener to be notified when the animation is complete.
     */

    public void setOrientation(double orientation, long durationMs, MapViewAnimationListener listener) {
        if (sImpl.mMapPointer != 0) {
            nativeSetOrientation(sImpl.mMapPointer, orientation, durationMs, listener);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Tilt the map to the specified degree angle.
     *
     * @param angle The new tilt angle, in degrees.
     */

    public void setTilt(double angle) {
        if (sImpl.mMapPointer != 0) {
            nativeSetTilt(sImpl.mMapPointer, angle);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the current map tilt, in degrees.
     *
     * @return The map's current tilt, in degrees.
     */

    public double getTilt() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetTilt(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the device's current GPS information.
     *
     * @return The device's current GPS information.
     *
     * @see UserLocation
     */

    public UserLocation getUserLocation() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetUserLocation(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** A convenience method to go to the device's current GPS coordinates */

    public void goToGPSLocation() {
        if (sImpl.mMapPointer != 0) {
            UserLocation location = this.getUserLocation();
            this.setCenterAnimated(location.position, kDefaultAnimationDuration);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the marker group with the specified name.  If it does not exist, it will be created.
     *
     * @param name The name of the marker group.
     * @return The requested marker group.
     *
     * @see MarkerGroup
     */
    public MarkerGroup getMarkerGroup(String name)
    {
        if (sImpl.mMapPointer == 0) {
            throw new MapViewNotReadyException();
        }

        MarkerGroup group = null;

        synchronized (mMarkerGroups)
        {
            group = mMarkerGroups.get(name);
        }

        if (group != null)
        {
            return group;
        }

        long nativePointer = nativeGetMarkerGroup(sImpl.mMapPointer, name);
        MarkerGroup markerGroup = new MarkerGroup(nativePointer, name, this);

        synchronized (mMarkerGroups)
        {
            mMarkerGroups.put(name, markerGroup);
        }

        return markerGroup;
    }

    /** Add a marker to the default marker group
     *
     * @param marker The marker to be added.
     *
     * @see Marker
     */

    public void addMarker(Marker marker) {
        if (sImpl.mMapPointer != 0)
        {
            MarkerGroup defaultGroup = this.getMarkerGroup(kDefaultMarkerGroup);
            defaultGroup.addMarker(marker);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Remove a marker from the default marker group
     *
     * @param marker The marker to be removed.
     *
     * @see Marker
     */
    public void removeMarker(Marker marker)
    {
        if (sImpl.mMapPointer != 0)
        {
            MarkerGroup defaultGroup = this.getMarkerGroup(kDefaultMarkerGroup);
            defaultGroup.removeMarker(marker);
        } else
        {
            throw new MapViewNotReadyException();
        }
    }

    /** Remove all markers from the default marker group
     * @see Marker
     */
    public void removeAllMarkers()
    {
        if (sImpl.mMapPointer != 0)
        {
            MarkerGroup defaultGroup = this.getMarkerGroup(kDefaultMarkerGroup);
            defaultGroup.removeAllMarkers();
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Set the listener which will receive events on this map.
     *
     * @param listener The new listener to receive events.
     */
    public void setMapViewListener(MapViewListener listener)
    {
        mMapListener = listener;
    }

    /** Set the map's tracking mode.
     *
     * <p>The tracking mode may be one of the following options:</p>
     *
     * <ul>
     *     <li>{@link MapView#MAP_TRACKING_MODE_NONE} - Disables tracking.</li>
     *     <li>{@link MapView#MAP_TRACKING_MODE_POSITION} - The map will track the user's current GPS position, and automatically move each time the user's position changes.
     *     Any user interaction will break this tracking mode, and automatically set it back to {@link MapView#MAP_TRACKING_MODE_NONE}.</li>
     *     <li>{@link MapView#MAP_TRACKING_MODE_POSITION_ORIENTATION} - The map will track the user's current GPS position and compass orientation, and automatically move each time the user's position changes.
     *     Any user interaction will break this tracking mode, and automatically set it back to {@link MapView#MAP_TRACKING_MODE_NONE}.</li>
     * </ul>
     *
     * @param mode The new tracking mode.
     */

    public void setTrackingMode(int mode) {
        if (sImpl.mMapPointer != 0) {
            nativeSetTrackingMode(sImpl.mMapPointer, mode);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    /** Get the current tracking mode
     *
     * @return The current tracking mode.
     */

    public int getTrackingMode() {
        if (sImpl.mMapPointer != 0) {
            return nativeGetTrackingMode(sImpl.mMapPointer);
        } else {
            throw new MapViewNotReadyException();
        }
    }

    public MapCamera getCameraSnapshot()
    {
        long cameraPtr = nativeGetCameraSnapshot(sImpl.mMapPointer);
        long projectionPtr = nativeGetProjection(sImpl.mMapPointer);
        return new MapCamera(cameraPtr, projectionPtr);
    }

    public void setMapViewIdleListener(MapViewIdleListener listener)
    {
        if (this.isIdle())
        {
            listener.onMapViewIdle(this);
        }
        else
        {
            mMapViewIdleListener = listener;
        }
    }

    // Java lifecycle methods.

    /** Restores some map state from a previous instance. This should be called by the user.
     *
     */
    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    /**
     * Resume sensor services and rendering. If this MapView is not managed by a fragment, this should
     * be called by the user.
     */
    @Override
    public void onResume()
    {
        if (sImpl.mMapPointer != 0)
        {
            this.nativeInvalidate(sImpl.mMapPointer);
        }
        super.onResume();
    }

    /**
     * Pause sensor services and rendering. If this MapView is not managed by a fragment, this should
     * be called by the user.
     */
    @Override
    public void onPause()
    {
        super.onPause();
    }

    /**
     * Begins releasing all internal references. This should be called by the user.
     */
    @Override
    public void onDestroy()
    {
        // mLayers = null;
        // mRegions = null;
        // mFeatures = null;
        for(MarkerGroup group : mMarkerGroups.values())
        {
            group.setMap(null);
        }
        // mMarkerGroups = null;
        // mFeatureGroups = null;
        mMapListener = null;
        mMapViewIdleListener = null;

        super.onDestroy();
    }

    /**
     * Saves some state for the next instance. This should be called by the user.
     * @param bundle Bundle to save to.
     */
    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
    }

    /**
     * Informs the map to begin purging memory. This should be called by the user.
     */
    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom)
    {
        nativeSetPadding(sImpl.mMapPointer, left, top, right, bottom);
    }

    @Override
    protected void onInit(int width, int height)
    {
        sImpl.mSize = new Size(width, height);

        if (sImpl.mMapPointer == 0)
        {
            long mapPtr = createMap(sImpl.mOptions);
            sImpl.mMapPointer = nativeInit(mapPtr);

            if (sImpl.mBaseLayer != null)
            {
                this.nativeSetBaseLayer(sImpl.mMapPointer, sImpl.mBaseLayer.mNativePtr);
            }

            for (Layer layer : mLayers)
            {
                if (layer != sImpl.mBaseLayer)
                {
                    this.nativeAddLayer(sImpl.mMapPointer, layer.mNativePtr);
                }
            }

            for (Region region : mRegions) {
                LonLat center = region.getCenter();
                this.nativeAddRegion(sImpl.mMapPointer, center.longitude, center.latitude, region.getRadius());
            }

            for (Map.Entry<Integer, Integer> entry : sImpl.mMapOptions.entrySet()) {
                this.nativeSetMapOption(sImpl.mMapPointer, entry.getKey(), entry.getValue());
            }
        }
        else
        {
            MapPosition resetPosition = sImpl.mOptions.getPosition();
            this.nativeResetToPosition(sImpl.mMapPointer, resetPosition.center.longitude, resetPosition.center.latitude, resetPosition.zoom, resetPosition.orientation, resetPosition.tilt);
        }

        nativeCreateMapListener(sImpl.mMapPointer);
    }

    @Override
    protected void preUpdate()
    {
        super.preUpdate();

        synchronized (mLayers)
        {
            for(Layer layer : mLayers)
            {
                layer.preUpdate();
            }
        }

        synchronized (mMarkerGroups)
        {
            for(MarkerGroup group : mMarkerGroups.values())
            {
                group.preUpdate();
            }
        }
    }

    @Override
    protected void postUpdate()
    {
        super.postUpdate();

        synchronized (mLayers)
        {
            for(Layer layer : mLayers)
            {
                layer.postUpdate();
            }
        }

        synchronized (mMarkerGroups)
        {
            for(MarkerGroup group : mMarkerGroups.values())
            {
                group.postUpdate();
            }
        }
    }

    protected void onEnable(long applicationPtr) {
        if (sImpl.mMapPointer != 0) {
            this.nativeEnable(sImpl.mMapPointer, applicationPtr);
        }
    }

    protected void onDisable() {
        if (sImpl.mMapPointer != 0) {
            this.nativeDisable(sImpl.mMapPointer);
        }
    }

    protected void onIdle() {
        if (mMapViewIdleListener != null)
        {
            Util.postToUIThread(new Runnable() {
                @Override
                public void run() {
                    mMapViewIdleListener.onMapViewIdle(MapView.this);
                    mMapViewIdleListener = null;
                }
            });
        }
    }

    /*
     * Internal API Calls.
     * These will be called by the engine
     */
    private void onMapEvent(final int type)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mMapListener != null) {
                    MapEvent evt = new MapEvent(MapView.this);
                    MapPosition position = new MapPosition(getCenter(), getZoom(), getOrientation(), getTilt());
                    evt.setPosition(position);
                    evt.setType(type);
                    mMapListener.onMapEvent(evt);
                }
            }
        });
    }

    private void onMapTouchEvent(final int type, final LonLat p)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mMapListener != null) {
                    MapTouchEvent evt = new MapTouchEvent(MapView.this);
                    evt.setTouchPoint(p);
                    evt.setType(type);
                    mMapListener.onTouchEvent(evt);
                }
            }
        });
    }

    protected void onMoveStart()
    {
        onMapEvent(MapEvent.MOVE_START);
    }

    protected void onMoveEnd()
    {
        onMapEvent(MapEvent.MOVE_END);
    }

    protected void onZoomStart()
    {
        onMapEvent(MapEvent.ZOOM_START);
    }

    protected void onZoomEnd()
    {
        onMapEvent(MapEvent.ZOOM_END);
    }

    protected void onRotateStart(final double orientation)
    {
        onMapEvent(MapEvent.ROTATE_START);
    }

    protected void onOrientationChange(final double orientation)
    {
        onMapEvent(MapEvent.ROTATE_CHANGE);
    }

    protected void onRotateEnd(final double orientation)
    {
        onMapEvent(MapEvent.ROTATE_END);
    }

    protected void onTiltStart(final double tilt)
    {
        onMapEvent(MapEvent.TILT_START);
    }

    protected void onTiltChange(final double tilt)
    {
        onMapEvent(MapEvent.TILT_CHANGE);
    }

    protected void onTiltEnd(final double tilt)
    {
        onMapEvent(MapEvent.TILT_END);
    }

    protected void onUserLocationChange(final UserLocation location)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mMapListener != null) {
                    mMapListener.onUserLocationChange(location);
                }
            }
        });

    }

    protected void onEnterRegion(final int region)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mMapListener != null) {
                    mMapListener.onEnterRegion(region);
                }
            }
        });

    }

    protected void onExitRegion(final int region)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mMapListener != null) {
                    mMapListener.onExitRegion(region);
                }
            }
        });

    }

    protected void onTouchUp(final LonLat p)
    {
        onMapTouchEvent(MapTouchEvent.TOUCH_UP, p);
    }

    protected void onTouchDown(final LonLat p)
    {
        onMapTouchEvent(MapTouchEvent.TOUCH_DOWN, p);
    }

    protected void onTap(final LonLat p)
    {
        onMapTouchEvent(MapTouchEvent.TAP, p);
    }

    protected void onDoubleTap(final LonLat p)
    {
        onMapTouchEvent(MapTouchEvent.DOUBLE_TAP, p);
    }

    protected void onLongPress(final LonLat p)
    {
        onMapTouchEvent(MapTouchEvent.LONG_PRESS, p);
    }

    protected class MapDescription {
        public LonLat center = new LonLat(0, 0);
        public float zoom = 3;
        public double rotation = 0;
        public double tilt = 0;
        public float minZoom = 3;
        public float maxZoom = 30;
        public Size mapSize = new Size(0, 0);
        public int projection = MAP_PROJECTION_MERCATOR;
    }

    protected long getNativePointer() {
        return sImpl.mMapPointer;
    }
}
