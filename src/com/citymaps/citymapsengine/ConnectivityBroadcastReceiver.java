package com.citymaps.citymapsengine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 10/15/13
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */


class ConnectivityBroadcastReceiver extends BroadcastReceiver
{
    public enum NetworkStatus {
        NetworkStatusNone(0),
        NetworkStatusCellular(1),
        NetworkStatusWifi(2),
        NetworkStatusLAN(3),
        NetworkStatusTransient(4);

        private int mValue;

        private NetworkStatus(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    interface ConnectivityBroadcastReceiverListener {
        public void onNetworkStatusChange(NetworkStatus status);
    }

    private ConnectivityBroadcastReceiverListener mListener = null;
    private boolean mIsListening = false;
    private Context mContext = null;

    public ConnectivityBroadcastReceiver()
    {
        mIsListening = false;
        mContext = null;
    }

    @Override
    public synchronized void onReceive(Context context, Intent intent)
    {
        if (mListener == null)
            return;

        String action = intent.getAction();
        if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION) || mIsListening == false) {
            return;
        }

        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info == null) {
            mListener.onNetworkStatusChange(NetworkStatus.NetworkStatusNone);
            return;
        }

        int type = info.getType();
        if (!info.isConnected()) {
            mListener.onNetworkStatusChange(NetworkStatus.NetworkStatusNone);
            return;
        }

        if (type == ConnectivityManager.TYPE_WIFI) {
            mListener.onNetworkStatusChange(NetworkStatus.NetworkStatusWifi);
            return;
        }

        mListener.onNetworkStatusChange(NetworkStatus.NetworkStatusCellular);
    }

    public synchronized void activate(Context context, ConnectivityBroadcastReceiverListener listener)
    {
        if (context == null || listener == null)
            return;

        this.deactivate();
        mListener = listener;
        mContext = context;

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(this, filter);
        mIsListening = true;
    }

    public synchronized void deactivate()
    {

        try {
            if (mContext != null)
            {
                mContext.unregisterReceiver(this);
            }
        }
        catch (IllegalArgumentException e) {

        } finally {
            mContext = null;
            mIsListening = false;
            mListener = null;
        }
    }

    public NetworkStatus getCurrentStatus(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();

        if (info == null)
            return NetworkStatus.NetworkStatusNone;

        int type = info.getType();
        if (!info.isConnected()) {
            return NetworkStatus.NetworkStatusNone;
        }

        if (type == ConnectivityManager.TYPE_WIFI) {
            return NetworkStatus.NetworkStatusWifi;
        }

        return NetworkStatus.NetworkStatusCellular;
    }
}
