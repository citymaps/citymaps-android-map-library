package com.citymaps.citymapsengine;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.ViewGroup;
import com.citymaps.citymapsengine.events.MarkerEvent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 *  A class representing a marker on the map.  A marker is any arbitrary annotation on the map.  It is represented by an image, either from disk or from memory.
 */

public class Marker
{
    private static final float kOneOver255 = 1.0f / 255.f;

    protected long mNativePointer = 0;
    private MarkerOnTouchListener mListener = null;
    private ArrayList<MarkerAttachmentView> mAttachmentViews = new ArrayList<MarkerAttachmentView>();
    private PointF mAnchorPoint = new PointF(0,0);
    private PointF mAttachmentAnchorPoint = new PointF(0,0);
    private float mAlpha = 1.0f;
    private float mRotation = 0.0f;
    private boolean mVisible = true;
    private boolean mDraggable = false;
    private boolean mFlat = false;
    private boolean mDragging = false;
    private int mZIndex;
    private int mHighlightColor;
    private float mScale = 1.0f;
    private Object mTag;

    private WeakReference<MapView> mMap = null;
    private ArrayList<Marker> mChildren = null;
    private ArrayList<Label> mLabels = null;
    private Marker mParent = null;

    private native long nativeCreateMarker();
    private native void nativeReleaseMarker(long nativePointer);
    private native void nativeSetMarkerListener(long nativePointer, boolean active);

    private native void nativeSetImageFromResource(long nativePointer, String asset);
    private native void nativeSetImageFromBitmap(long nativePointer, int[] pixels, int width, int height);
    private native void nativeSetImageFromFilesystem(long nativePointer, String file);
    private native void nativeSetMarkerPosition(long nativePointer, double lon, double lat);
    private native void nativeSetMarkerSize(long nativePointer, int width, int height);
    private native void nativeSetMarkerAnchorPoint(long nativePointer, double x, double y);
    private native void nativeSetMarkerAlpha(long nativePointer, float alpha);
    private native void nativeSetMarkerRotation(long nativePointer, float rotation);
    private native void nativeSetMarkerDraggable(long nativePointer, boolean draggable);
    private native void nativeSetMarkerVisible(long nativePointer, boolean visible);
    private native void nativeSetMarkerFlat(long nativePointer, boolean flat);
    private native void nativeSetMarkerDragging(long nativePointer, boolean flat);
    private native void nativeSetZIndex(long nativePointer, int index);
    private native void nativeSetHighlightColor(long nativePointer, float r, float g, float b, float a);
    private native void nativeSetMarkerAttachmentAnchorPoint(long nativePointer, float u, float v);
    private native void nativeSetMarkerScale(long nativePointer, float scale);
    private native void nativeSetParentInheritance(long nativePointer, boolean scale, boolean alpha, boolean rotation);

    private native PointF nativeGetMarkerScreenPosition(long nativePointer);
    private native RectF nativeGetMarkerScreenBounds(long nativePointer);
    private native Object nativeGetMarkerSize(long nativePointer);
    private native void nativeAddAttachment(long nativePointer, long childPtr);
    private native void nativeRemoveAttachment(long nativePointer, long childPtr);

    private native Object nativeCalculateAttachmentPosition(long nativePointer, double u, double v);

    private native LonLat nativeGetMarkerPosition(long nativePointer);

    /** Create an empty marker
     * @return An empty marker.
     */
    public static Marker createMarkerWithoutImage() {
        return new Marker();
    }

    /** Create a marker with an image from a resource file.
     *
     * @param resource The resource image file.
     * @return A marker with the resource set as its image.
     */
    public static Marker createMarkerFromResource(String resource) {
        Marker marker = new Marker();
        marker.loadImageFromResource(resource);
        return marker;
    }

    /** Create a marker with an image from a resource id.
     *
     * @param resourceId The resource image id.
     * @return A marker with the resource set as its image.
     */
    public static Marker createMarkerFromResource(int resourceId) {
        Marker marker = new Marker();
        String resource = WindowAndroid.getApplicationContext().getResources().getResourceEntryName(resourceId);
        marker.loadImageFromResource(resource);
        return marker;
    }

    /** Create a marker with an image from a bitmap resource.
     *
     * @param bitmap The bitmap resource object.
     * @return A marker with the bitmap set as its image.
     */
    public static Marker createMarkerFromBitmap(Bitmap bitmap) {
        Marker marker = new Marker();
        marker.loadImageFromBitmap(bitmap);
        return marker;
    }

    /** Create a marker with an image from a file on disk.
     *
     * @param file The path of the file.
     * @return A marker with the file set as its image.
     */
    public static Marker createMarkerFromFile(String file) {
        Marker marker = new Marker();
        marker.loadImageFromFile(file);
        return marker;
    }

    private Marker()
    {
        mNativePointer = nativeCreateMarker();
        mChildren = new ArrayList<Marker>();
        mLabels = new ArrayList<Label>();

        mParent = null;
    }

    protected void setMap(MapView map)
    {
        MapView oldMap = mMap == null ? null : mMap.get();

        if (oldMap != map)
        {
            if (oldMap != null)
            {
                for(MarkerAttachmentView view : mAttachmentViews)
                {
                    oldMap.removeView(view);
                }
            }
            mMap = new WeakReference<MapView>(map);
            if(map != null)
            {
                for(MarkerAttachmentView view : mAttachmentViews)
                {
                    map.addView(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            }
            else
            {
                // Do not hold views past the life of the MapView.
                mAttachmentViews.clear();
            }
        }

    }

    public void addMarker(Marker child)
    {
        if (child.getParent() == this)
            return;

        if (child.getParent() != null)
        {
            child.removeFromParent();
        }

        nativeAddAttachment(mNativePointer, child.mNativePointer);
        child.setParent(this);
        mChildren.add(child);
    }

    public void removeMarker(Marker child)
    {
        if (child.getParent() != this)
            return;

        nativeRemoveAttachment(mNativePointer, child.mNativePointer);
        child.setParent(null);
        mChildren.remove(child);
    }

    public void removeAllChildren()
    {
        for(Marker child : mChildren)
        {
            nativeRemoveAttachment(mNativePointer, child.mNativePointer);
            child.setParent(null);
        }

        mChildren.clear();
    }

    public int getChildCount()
    {
        return mChildren.size();
    }

    public Marker getChild(int index)
    {
        if (index < 0 || index >= mChildren.size())
            throw new IllegalArgumentException("Index out of bounds.");

        return mChildren.get(index);
    }

    public int getLabelCount()
    {
        return mLabels.size();
    }

    /** Attaches an android view to the marker.
     * @param key Identifier for this attachment.
     * @param attachment View to attach.
     */
    public void addAttachmentView(String key, MarkerAttachmentView attachment)
    {
        attachment.setKey(key);
        mAttachmentViews.add(attachment);
        MapView map = mMap.get();
        attachment.setAlpha(0);
        if (map != null)
        {
            map.addView(attachment, ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    /** Removes an attachment view from the marker.
     * @param key Identifier for the attachment to be removed.
     */
    public void removeAttachmentView(String key)
    {
        for(MarkerAttachmentView view : mAttachmentViews)
        {
            if (view.getKey().equals(key))
            {
                mAttachmentViews.remove(view);

                MapView map = mMap.get();

                if (map != null)
                {
                    map.removeView(view);
                }
                return;
            }
        }
    }

    public Label getLabel(int index)
    {
        if (index < 0 || index >= mLabels.size())
            throw new IllegalArgumentException("Index out of bounds.");

        return mLabels.get(index);
    }

    public void addLabel(Label label)
    {
        if (label.getParent() == this)
            return;

        if (label.getParent() != null)
        {
            label.removeFromParent();
        }

        nativeAddAttachment(mNativePointer, label.mNativePointer);
        label.setParent(this);
        mLabels.add(label);
    }

    public void removeLabel(Label label)
    {
        if (label.getParent() != this)
            return;

        nativeRemoveAttachment(mNativePointer, label.mNativePointer);
        label.setParent(null);
        mLabels.remove(label);
    }

    public void removeAllLabels()
    {
        for(Label label : mLabels)
        {
            nativeRemoveAttachment(mNativePointer, label.mNativePointer);
            label.setParent(null);
        }

        mLabels.clear();
    }

    public void removeFromParent()
    {
        if (mParent == null)
            return;

        mParent.removeMarker(this);
        mParent = null;
    }

    public Marker getParent()
    {
        return mParent;
    }

    private void setParent(Marker parent)
    {
        mParent = parent;
    }

    @Override
    protected void finalize() {
        nativeReleaseMarker(mNativePointer);
    }

    /** Reload this marker with the given resource image file.
     *
     * @param resource The name of the resource file.
     */
    public void loadImageFromResource(String resource) {
        nativeSetImageFromResource(mNativePointer, resource);
    }

    /** Reload this marker with the given resource ID of an image file.
     *
     * @param resourceId The name of the resource file.
     */
    public void loadImageFromResource(int resourceId) {
        String resource = WindowAndroid.getApplicationContext().getResources().getResourceEntryName(resourceId);
        nativeSetImageFromResource(mNativePointer, resource);
    }

    /** Reload this marker with the given bitmap resource.
     *
     * @param bitmap The bitmap resource object.
     */
    public void loadImageFromBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size];

        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        nativeSetImageFromBitmap(mNativePointer, pixels, width, height);
    }

    /** Reload this marker with the given file from disk.
     *
     * @param file The path of the file.
     */
    public void loadImageFromFile(String file) {
        nativeSetImageFromFilesystem(mNativePointer, file);
    }

    /** Set the marker's longitude/latitude position.
     *
     * @param position The longitude/latitude position of the marker.
     */
    public void setPosition(LonLat position) {
        nativeSetMarkerPosition(mNativePointer, position.longitude, position.latitude);
    }

    /** Set the display size of this marker.
     *
     * @param size The new marker size.
     */
    public void setSize(Size size)
    {
        nativeSetMarkerSize(mNativePointer, (int) size.width, (int) size.height);
    }

    /** Set the anchor point of the marker.
     *
     * <p>
     *     The anchor point is the screen position the marker is drawn at relative to its own size.  Anchor point values is typically between 0 and 1.
     * </p>
     *
     * @param point The new anchor point.
     */
    public void setAnchorPoint(PointF point) {
        mAnchorPoint = new PointF(point.x, point.y);

        nativeSetMarkerAnchorPoint(mNativePointer, point.x, point.y);
    }

    /** Set the attachment anchor point of the marker.
     *
     * <p>
     *     This anchor point is the position on the marker's parent that it will anchor to.
     * </p>
     *
     * @param point The new anchor point.
     */
    public void setAttachmentAnchorPoint(PointF point)
    {
        mAttachmentAnchorPoint = new PointF(point.x, point.y);
        nativeSetMarkerAttachmentAnchorPoint(mNativePointer, point.x, point.y);
    }

    public PointF getAttachmentAnchorPoint()
    {
        return new PointF(mAttachmentAnchorPoint.x, mAttachmentAnchorPoint.y);
    }

    /** Set the alpha of the marker.
     *
     * @param alpha The new marker alpha.
     */
    public void setAlpha(float alpha) {
        mAlpha = alpha;

        nativeSetMarkerAlpha(mNativePointer, alpha);
    }

    /** Set the rotation of the marker.
     *
     * @param rotation The new marker rotation
     */
    public void setRotation(float rotation) {
        mRotation = rotation;

        nativeSetMarkerRotation(mNativePointer, rotation);
    }

    /** Sets the scale of the marker.
     *
     * @param scale The new marker scale
     */
    public void setScale(float scale) {
        mScale = scale;
        nativeSetMarkerScale(mNativePointer, scale);
    }

    /** Sets whether to inherit properties from the marker's parent.
     *
     * @param scale Whether to scale along with the parent.
     * @param alpha Whether to fade in and out along with the parent.
     * @param rotation Whether to rotate along with the parent.
     */
    public void setParentInheritance(boolean scale, boolean alpha, boolean rotation)
    {
        nativeSetParentInheritance(mNativePointer, scale, alpha, rotation);
    }

    /** Set whether the marker can be dragged.
     *
     * @param draggable
     */
    public void setDraggable(boolean draggable) {
        mDraggable = draggable;

        nativeSetMarkerDraggable(mNativePointer, draggable);
    }

    /** Set whether the marker is flat or billboard.
     *
     * @param flat
     */
    public void setFlat(boolean flat) {
        mFlat = flat;

        nativeSetMarkerFlat(mNativePointer, flat);
    }

    /** Set whether the marker is visible.
     *
     * @param visible
     */
    public void setVisible(boolean visible) {
        mVisible = visible;

        nativeSetMarkerVisible(mNativePointer, visible);
    }

    /** Set the marker to dragging state, or stop the marker from dragging.
     *
     * @param dragging The current drag state.
     */
    public void setDragging(boolean dragging) {
        mDragging = dragging;

        nativeSetMarkerDragging(mNativePointer, dragging);
    }

    /** Set the z index for this marker. Higher z indexed markers appear on top.
     *
     * @param index New z index.
     */
    public void setZIndex(int index)
    {
        mZIndex = index;
        nativeSetZIndex(mNativePointer, index);
    }

    /** Set highlight color of the marker. This color will be blended into the marker while it is pressed down.
     *
     * @param color Color to highlight the marker with.
     */
    public void setHighlightColor(int color)
    {
        mHighlightColor = color;
        nativeSetHighlightColor(mNativePointer, Color.red(color) * kOneOver255, Color.green(color) * kOneOver255, Color.blue(color) * kOneOver255, Color.alpha(color) * kOneOver255);
    }

    /** Set the marker's touch listener.
     *
     * <p>This listener will respond to touch events on the marker.</p>
     *
     * @param markerOnTouchListener
     *
     * @see MarkerOnTouchListener
     */
    public void setOnTouchListener(MarkerOnTouchListener markerOnTouchListener)
    {
        mListener = markerOnTouchListener;
        boolean active = mListener != null;
        nativeSetMarkerListener(mNativePointer, active);
    }

    /** Set a tag associated with this marker object
     * @param tag
     */

    public void setTag(Object tag) {
        mTag = tag;
    }

    /** Get the marker's z index. Z index is used to determine the rendering order of markers.
     *
     * @return The marker's z index.
     */
    public int getZIndex()
    {
        return mZIndex;
    }

    /** Get the marker's highlight color. This color will be applied to the marker when the marker is in the pressed state.
     *
     * @return The marker's highlight color.
     */
    public int getHighlightColor()
    {
        return mHighlightColor;
    }

    /** Get the marker's position.
     *
     * @return The marker's longitude/latitude position.
     */
    public LonLat getPosition() {
        return nativeGetMarkerPosition(mNativePointer);
    }

    /** Get the marker's screen position. Undefined value if the marker has not been placed on the screen yet.
     * @return The marker's screen position.
     */
    public PointF getScreenPosition()
    {
        return nativeGetMarkerScreenPosition(mNativePointer);
    }

    /** Get the marker's screen bounds. Undefined value if the marker has not been placed on the screen yet.
     * @return The marker's screen bounds.
     */
    public RectF getScreenBounds()
    {
        return nativeGetMarkerScreenBounds(mNativePointer);
    }

    /** Get the marker's size.
     *
     * @return The marker's current display size.
     */

    public Size getSize()
    {
        return (Size)nativeGetMarkerSize(mNativePointer);
    }

    /** Get the marker's alpha.
     *
     * @return The marker's current alpha value.
     */
    public float getAlpha() {
        return mAlpha;
    }

    /** Get the marker's rotation.
     *
     * @return The marker's current rotation.
     */
    public float getRotation() {
        return mRotation;
    }

    /** Get the marker's scale.
     *
     * @return The marker's current scale.
     */
    public float getScale() { return mScale;}

    /** Get whether the marker is draggable.
     *
     * @return Whether the marker is draggable.
     */

    public boolean isDraggable() {
        return mDraggable;
    }

    /** Get whether the marker is visible.
     *
     * @return Whether the marker is visible.
     */

    public boolean isVisible() {
        return mVisible;
    }

    /** Get whether the marker is flat or billboarded.
     *
     * @return Whether the marker is flat or billboarded.
     */

    public boolean isFlat() {
        return mFlat;
    }

    /** Get whether the marker is currently being dragged.
     *
     * @return Whether the marker is being dragged.
     */
    public boolean isDragging() {
        return mDragging;
    }

    /** Get the marker's anchor point.
     *
     * <p>
     *     The anchor point is the screen position the marker is drawn at relative to its own size.  Anchor point values is typically between 0 and 1.
     * </p>
     *
     * @return The marker's current anchor point.
     */

    public PointF getAnchorPoint() {
        return mAnchorPoint;
    }

    /** Get the marker's tag object
     *  @return The marker's tag object
     */

    public Object getTag() {
        return mTag;
    }

    // Run loop methods
    protected void preUpdate()
    {

    }

    protected void postUpdate()
    {
        for(final MarkerAttachmentView view : mAttachmentViews)
        {
            PointF attachmentPosition = (PointF)nativeCalculateAttachmentPosition(mNativePointer, view.getMarkerAnchorPoint().x, view.getMarkerAnchorPoint().y);
            int width = view.getWidth();
            int height = view.getHeight();
            if (width == 0)
            {
                width = view.getMeasuredWidth();
            }

            if (height == 0)
            {
                height = view.getMeasuredWidth();
            }

            if (width == 0 || height == 0)
                continue;

            PointF viewAnchorPoint = view.getViewAnchorPoint();
            PointF viewOffset = new PointF(width * viewAnchorPoint.x, height * viewAnchorPoint.y);
            final PointF viewPosition = new PointF(attachmentPosition.x - viewOffset.x, attachmentPosition.y - viewOffset.y);

            Util.postToUIThread(new Runnable() {
                @Override
                public void run() {
                    view.setX(viewPosition.x);
                    view.setY(viewPosition.y);
                    view.setAlpha(1.0f);
                    view.invalidate();
                }
            });

        }
    }

    // Marker Listener JNI Callbacks
    private void onTouchEvent(final int event)
    {
        Util.postToUIThread(new Runnable() {
            @Override
            public void run() {
                if (mListener != null)
                {
                    MarkerEvent evt = new MarkerEvent(Marker.this);
                    evt.setType(event);
                    mListener.onTouchEvent(evt);
                }
            }
        });

    }
}
