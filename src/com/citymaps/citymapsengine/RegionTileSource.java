package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.TileSourceOptions;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 10/29/13
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegionTileSource extends TileSource
{
    private native long nativeCreateFactory();

    public RegionTileSource(String apiKey)
    {
        super(CitymapsDataSource.defaultRegionDataSource(apiKey), new TileSourceOptions());
    }

    public RegionTileSource(DataSource dataSource, TileSourceOptions options)
    {
        super(dataSource, options);
    }

    @Override
    protected long createTileFactory(TileSourceOptions options)
    {
        return nativeCreateFactory();
    }
}
