package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.LayerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A layer to hold and display user defined shapes. These shapes are subclasses of {@link CanvasFeature}.
 */
public class CanvasLayer extends Layer
{
    private ArrayList<CanvasFeature> mFeatures;
    private HashMap<String, FeatureGroup> mFeatureGroups;

    private native long nativeCreateLayer(LayerDescription desc);

    private native void nativeAddFeature(long ptr, long featurePtr);

    private native void nativeRemoveFeature(long ptr, long featurePtr);

    private native void nativeRemoveAllFeatures(long ptr);

    private native long nativeGetFeatureGroup(long ptr, String name);

    public CanvasLayer()
    {
        this(new LayerOptions());
    }
    public CanvasLayer(LayerOptions options) {
        super(options);
        mFeatures = new ArrayList<CanvasFeature>();
        mFeatureGroups = new HashMap<String, FeatureGroup>();
    }

    protected long createLayer(LayerOptions options) {
        LayerDescription desc = new LayerDescription();
        this.applyOptions(options, desc);

        return nativeCreateLayer(desc);
    }

    /**
     * Adds a feature to the CanvasLayer
     * @param feature The feature to add to the CanvasLayer.
     */
    public void addFeature(CanvasFeature feature) {
        if (!mFeatures.contains(feature)) {
            mFeatures.add(feature);
            nativeAddFeature(mNativePtr, feature.mNativePtr);
        }
    }

    /**
     * Removes a feature from the CanvasLayer
     * @param feature The feature to remove from the CanvasLayer.
     */
    public void removeFeature(CanvasFeature feature) {
        if (mFeatures.contains(feature)) {
            mFeatures.remove(feature);
            nativeRemoveFeature(mNativePtr, feature.mNativePtr);
        }
    }

    /**
     * Removes all CEFeatures from the CanvasLayer
     */
    public void removeAllFeatures() {
        mFeatures.clear();
        nativeRemoveAllFeatures(mNativePtr);
    }

    /** Gets the feature group with the specified name.  If it does not exist, it will be created.
     *
     * @param name The name of the feature group.
     */
    public FeatureGroup getFeatureGroup(String name) {
        if (mFeatureGroups.containsKey(name)) {
            return mFeatureGroups.get(name);
        }

        long featureGroupPtr = nativeGetFeatureGroup(mNativePtr, name);
        FeatureGroup newGroup = new FeatureGroup(featureGroupPtr, name);
        mFeatureGroups.put(name, newGroup);
        return newGroup;
    }

    @Override
    protected void onDestroy() {
        Log.w("MapEngine", "Destroying canvas layer");
        super.onDestroy();
    }
}
