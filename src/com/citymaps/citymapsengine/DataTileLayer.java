package com.citymaps.citymapsengine;

import android.util.Log;
import com.citymaps.citymapsengine.options.LayerOptions;
import com.citymaps.citymapsengine.options.TileLayerOptions;

/**
 * Created by eddiekimmel on 2/12/14.
 */
public class DataTileLayer extends TileLayer
{
    private DataTileLayerListener mListener = null;

    public DataTileLayer(TileLayerOptions options)
    {
        super(options);
    }

    private native long nativeCreateLayer(TileLayerDescription desc);
    private native void nativeSetListener(long nativePtr, Object listener);

    @Override
    protected long createLayer(LayerOptions options) {
        TileLayerDescription desc = new TileLayerDescription();
        this.applyOptions(options, desc);

        return this.nativeCreateLayer(desc);
    }

    public void setListener(DataTileLayerListener listener)
    {
        nativeSetListener(mNativePtr, listener);
        mListener = listener;
    }

    public void removeListener()
    {
        nativeSetListener(mNativePtr, null);
        mListener = null;
    }
}