package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 12/9/13.
 */
public interface MapViewIdleListener
{
    public void onMapViewIdle(MapView view);
}
