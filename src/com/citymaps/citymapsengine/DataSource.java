package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.DataSourceOptions;

import java.util.HashMap;

/** An abstract base class for all data sources.
 *
 *A data source provides a way for tile sources to retreive their data.
 *They serve as an encapsulation of the data retreival process, and can be served from multiple sources (i.e. network, disk).
 *A data source is required for {@link TileSource} to properly retreive data.
 *<p>
 *The following options are available at construction:
 *
 * <ul>
 *     <li>maxCacheSize - The maximum size of the memory cache, measured in number of tiles.</li>
 * </ul>
 *<p>
 *See {@link TileSource} for more information about tile and data sources in general.
 */

public abstract class DataSource
{
    static
    {
        CitymapsEngine.init();
    }

    protected class DataSourceDescription
    {
        int memoryCacheSize = 15;
    }

    protected long mEnginePointer;
    private boolean mOwnedByTileSource = false;
    protected native void nativeRelease(long nativePtr);


    public DataSource(DataSourceOptions options)
    {
        if (options == null)
            throw new IllegalArgumentException("DataSourceOptions cannot be null. Please use the appropriate default options object instead.");

        mEnginePointer = this.createDataSource(options);
    }

    protected abstract long createDataSource(DataSourceOptions options);

    protected void applyOptions(DataSourceOptions options, DataSourceDescription desc)
    {
        desc.memoryCacheSize = options.getMaxDataCache();
    }

    protected void setOwnedByTileSource(boolean owned)
    {
        mOwnedByTileSource = owned;
    }

    protected void finalize()
    {
        if (mEnginePointer != 0 && !mOwnedByTileSource)
        {
            nativeRelease(mEnginePointer);
        }
    }
}
