package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 2/12/14.
 */
public interface DataTileLayerListener
{
    public static final int DataTileNotFound = 0;
    public static final int DataTileTimedOut = 1;
    public static final int DataTileUnknownError = 2;

    public void onTileLoaded(DataTileLayer layer, GridPoint gridPoint, byte[] data);

    public void onTileFailed(DataTileLayer layer, GridPoint gridPoint, int reason);

    public void onTileRemoved(DataTileLayer layer, GridPoint gridPoint);
}