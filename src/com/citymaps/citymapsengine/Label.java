package com.citymaps.citymapsengine;

import android.graphics.Color;
import android.graphics.PointF;

/**
 *  A class representing a label on the map.
 */
public class Label
{
    private static final float kOneOver255 = 1.0f / 255.f;

    public static final int kLabelHorizontalAlignmentLeft = 0;
    public static final int kLabelHorizontalAlignmentCenter = 1;
    public static final int kLabelHorizontalAlignmentRight =  2;

    public static final int kLabelVerticalAlignmentTop = 0;
    public static final int kLabelVerticalAlignmentMiddle = 1;
    public static final int kLabelVerticalAlignmentBottom = 2;

    protected long mNativePointer = 0;
    private String mText = null;
    private int mFontSize = 10;
    private int mOutlineSize = 0;
    private int mTextColor = Color.BLACK;
    private int mOutlineColor = Color.WHITE;
    private int mHorizontalAlignment = kLabelHorizontalAlignmentCenter;
    private int mVerticalAlignment = kLabelVerticalAlignmentMiddle;
    private Size mMaxSize = new Size(100, 30);
    private PointF mAnchorPoint = new PointF(0, 0);
    private float mAlpha = 1.0f;
    private Marker mParent = null;


    private static native long nativeCreateLabel();
    private static native void nativeReleaseLabel(long ptr);
    private static native void nativeSetText(long ptr, String text);
    private static native void nativeSetFontSize(long ptr, int size);
    private static native void nativeSetOutlineSize(long ptr, int size);
    private static native void nativeSetTextColor(long ptr, float r, float g, float b, float a);
    private static native void nativeSetOutlineColor(long ptr, float r, float g, float b, float a);
    private static native void nativeSetHorizontalAlignment(long ptr, int align);
    private static native void nativeSetVerticalAlignment(long ptr, int align);
    private static native void nativeSetSize(long ptr, int width, int height);
    private static native void nativeSetAnchorPoint(long ptr, float u, float v);
    private static native void nativeSetAlpha(long ptr, float alpha);

    public Label()
    {
        mNativePointer = nativeCreateLabel();
    }

    public void finalize()
    {
        try {
            nativeReleaseLabel(mNativePointer);
            super.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void setText(String text)
    {
        mText = new String(text);
        nativeSetText(mNativePointer, mText);
    }

    public void setFontSize(int size)
    {
        if (size <= 0)
            throw new IllegalArgumentException("Font Size must be greater than zero.");
        mFontSize = size;

        nativeSetFontSize(mNativePointer, mFontSize);
    }

    public void setOutlineSize(int size)
    {
        if (size <= 0)
            throw new IllegalArgumentException("Outline Size must be greater than zero.");

        mOutlineSize = size;
        nativeSetOutlineSize(mNativePointer, mOutlineSize);
    }

    public void setTextColor(int color)
    {
        mTextColor = color;
        nativeSetTextColor(mNativePointer, Color.red(color) * kOneOver255, Color.green(color) * kOneOver255, Color.blue(color) * kOneOver255, Color.alpha(color) * kOneOver255);
    }

    public void setOutlineColor(int color)
    {
        mOutlineColor = color;
        nativeSetOutlineColor(mNativePointer, Color.red(color) * kOneOver255, Color.green(color) * kOneOver255, Color.blue(color) * kOneOver255, Color.alpha(color) * kOneOver255);
    }

    public void setHorizontalAlignment(int align)
    {
        if (align < 0 || align > 2)
        {
            throw new IllegalArgumentException("Align must be one of kLabelHorizontalAlignmentLeft, kLabelHorizontalAlignmentCenter, or kLabelHorizontalAlignmentRight");
        }
        mHorizontalAlignment = align;
        nativeSetHorizontalAlignment(mNativePointer, mHorizontalAlignment);
    }

    public void setVerticalAlignment(int align)
    {
        if (align < 0 || align > 2)
        {
            throw new IllegalArgumentException("Align must be one of kLabelVerticalAlignmentTop, kLabelVerticalAlignmentMiddle, or kLabelVerticalAlignmentBottom");
        }
        mVerticalAlignment = align;
        nativeSetVerticalAlignment(mNativePointer, mVerticalAlignment);
    }

    public void setMaxSize(Size size)
    {
        if (size.width <= 0 || size.height <= 0)
            throw new IllegalArgumentException("Width and height must be greater than zero.");

        mMaxSize = new Size(size);
        nativeSetSize(mNativePointer, (int)size.width, (int)size.height);
    }

    public void setAnchorPoint(PointF anchor)
    {
        mAnchorPoint = new PointF(anchor.x, anchor.y);
        nativeSetAnchorPoint(mNativePointer, mAnchorPoint.x, mAnchorPoint.y);
    }

    public void setAlpha(float alpha)
    {
        mAlpha = alpha;
        nativeSetAlpha(mNativePointer, mAlpha);
    }

    public String getText()
    {
        return new String(mText);
    }

    public int getFontSize()
    {
        return mFontSize;
    }

    public int getOutlineSize()
    {
        return mOutlineSize;
    }

    public int getTextColor()
    {
        return mTextColor;
    }

    public int getOutlineColor()
    {
        return mOutlineColor;
    }

    public int getHorizontalAlignment()
    {
        return mHorizontalAlignment;
    }

    public int getVerticalAlignment()
    {
        return mVerticalAlignment;
    }

    public Size getMaxSize()
    {
        return new Size(mMaxSize);
    }

    public PointF getAnchorPoint()
    {
        return new PointF(mAnchorPoint.x, mAnchorPoint.y);
    }

    public float getAlpha()
    {
        return mAlpha;
    }

    public Marker getParent()
    {
        return mParent;
    }
    protected void setParent(Marker parent)
    {
        mParent = parent;
    }

    public void removeFromParent()
    {
        if (mParent == null)
            return;

        mParent.removeLabel(this);
        mParent = null;
    }
}