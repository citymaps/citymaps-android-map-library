package com.citymaps.citymapsengine;

/** A class to be used to define a sequence of animated movements. */
public class AnimationScript
{
    private long mNativePointer;

    private native long nativeCreatePointer();

    private native void nativeReleasePointer(long ptr);

    private native void nativeMoveTo(long ptr, double lon, double lat, float delay);

    private native void nativeMoveToAndZoom(long ptr, double lon, double lat, float zoom, float delay);

    private native void nativeZoomTo(long ptr, float zoom, float delay);

    private native void nativeRotateTo(long ptr, double orientation, float delay);

    private native void nativePlay(long ptr, long mapPtr);

    public AnimationScript() {
        mNativePointer = nativeCreatePointer();
    }

    @Override
    protected void finalize() {
        nativeReleasePointer(mNativePointer);
    }

    /**
     * Add an animated pan movement to the script.
     * @param point The CELonLat to move to.
     */
    public void moveTo(LonLat point) {
        this.moveTo(point, 0.0f);
    }

    /**
     * Add an animated pan movement to the script.
     * @param point The CELonLat to move to.
     * @param delay The amount of time (in seconds) the script should wait before starting this step.
     */
    public void moveTo(LonLat point, float delay) {
        this.nativeMoveTo(mNativePointer, point.longitude, point.latitude, delay);
    }

    /**
     * Add an animated pan and zoom movement to the script.
     * @param point The CELonLat to move to.
     * @param zoom The zoom level to zoom to.
     */
    public void moveToAndZoom(LonLat point, float zoom) {
        this.moveToAndZoom(point, zoom, 0);
    }

    /**
     * Add an animated pan and zoom movement to the script.
     * @param point The CELonLat to move to.
     * @param zoom The zoom level to zoom to.
     * @param delay The amount of time (in seconds) the script should wait before starting this step.
     */
    public void moveToAndZoom(LonLat point, float zoom, float delay) {
        this.nativeMoveToAndZoom(mNativePointer, point.longitude, point.latitude, zoom, delay);
    }

    /**
     * Add an animated zoom movement to the script.
     * @param zoom The zoom level to zoom to.
     */
    public void zoomTo(float zoom) {
        this.zoomTo(zoom, 0);
    }

    /**
     * Add an animated zoom movement to the script.
     * @param zoom The zoom level to zoom to.
     * @param delay The amount of time (in seconds) the script should wait before starting this step.
     */
    public void zoomTo(float zoom, float delay) {
        this.nativeZoomTo(mNativePointer, zoom, delay);
    }

    /**
     * Add an animated rotation to the script.
     * @param orientation The orientation to rotate to.
     */
    public void rotateTo(double orientation) {
        this.rotateTo(orientation, 0);
    }

    /**
     * Add an animated rotation to the script.
     * @param orientation The orientation to rotate to.
     * @param delay The amount of time (in seconds) the script should wait before starting this step.
     */
    public void rotateTo(double orientation, float delay) {
        this.nativeRotateTo(mNativePointer, orientation, delay);
    }

    /**
     * Play the animation script.
     * @param map The map to play the animation on.
     */
    public void play(MapView map) {
        this.nativePlay(mNativePointer, map.getNativePointer());
    }
}
