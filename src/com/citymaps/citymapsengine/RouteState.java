package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 9/10/14.
 */
public class RouteState
{
    public int instructionIndex;
    public float distanceToNextInstruction;
    public float distanceToDestination;
    public float timeToNextInstruction;
    public float timeUntilDestination;
    public float routeHeading;
    public float userVelocity;

    public RouteState()
    {}

}
