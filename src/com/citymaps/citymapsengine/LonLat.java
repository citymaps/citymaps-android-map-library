package com.citymaps.citymapsengine;

import android.os.Parcel;
import android.os.Parcelable;

/** Simple object representing a longitude/latitude location. */

public class LonLat implements Parcelable {
    public double longitude;
    public double latitude;

    /** Construct a LonLat object with coordinates.
     *
     * @param lon
     * @param lat
     */
    public LonLat(double lon, double lat) {
        this.longitude = lon;
        this.latitude = lat;
    }

    public LonLat(LonLat lonLat)
    {
        this.longitude = lonLat.longitude;
        this.latitude = lonLat.latitude;
    }

    private LonLat(Parcel in) {
        this.longitude = in.readDouble();
        this.latitude = in.readDouble();
    }

    /** Construct a LonLat object
     *
     *  <p>Both longitude and latitude will be set to 0.</p>
     *
     */
    public LonLat()
    {
        this.longitude = 0;
        this.latitude = 0;
    }

    /** Sets data from an existing LonLat
     * @param lonLat LonLat to copy from.
     */
    public void set(LonLat lonLat)
    {
        longitude = lonLat.longitude;
        latitude = lonLat.latitude;
    }

    /** Gets the distance from this lon lat to another.
     * @param other The LonLat to get the distance to.
     * @return Distance in meters.
     */
    public double getDistanceMeters(LonLat other)
    {
        return MapUtil.getDistanceMeters(this, other);
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof LonLat) {
            LonLat lonlat = (LonLat)other;
            return lonlat.longitude == longitude &&
                    lonlat.latitude == latitude;
        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
    }

    public static final Creator<LonLat> CREATOR = new Creator<LonLat>() {
        @Override
        public LonLat createFromParcel(Parcel source) {
            return new LonLat(source);
        }

        @Override
        public LonLat[] newArray(int size) {
            return new LonLat[size];
        }
    };
}
