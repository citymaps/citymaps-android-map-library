package com.citymaps.citymapsengine;

import android.content.Context;
import com.citymaps.citymapsengine.options.CitymapsDataSourceOptions;
import com.citymaps.citymapsengine.options.TileSourceOptions;
import com.citymaps.citymapsengine.options.VectorTileSourceOptions;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 10/29/13
 * Time: 11:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class VectorTileSource extends TileSource {

    private native long nativeCreateFactory(String configFile);

    public VectorTileSource(String apiKey)
    {
        super(CitymapsDataSource.defaultVectorDataSource(apiKey), new VectorTileSourceOptions());
    }

    public VectorTileSource(String apiKey, String styleResource)
    {
        super(CitymapsDataSource.defaultVectorDataSource(apiKey), new VectorTileSourceOptions().config(styleResource));
    }

    public VectorTileSource(String apiKey, Context context, int resourceId)
    {
        this(apiKey, WindowAndroid.getApplicationContext().getResources().getResourceEntryName(resourceId));
    }

    public VectorTileSource(CitymapsDataSource dataSource, VectorTileSourceOptions options)
    {
        super(dataSource, options);
    }

    public static VectorTileSource createWithURL(String url)
    {
        CitymapsDataSourceOptions options = new CitymapsDataSourceOptions();
        options.signedURLs(false);
        options.url(url);

        CitymapsDataSource source = new CitymapsDataSource(options);
        return new VectorTileSource(source, new VectorTileSourceOptions());
    }

    @Override
    protected long createTileFactory(TileSourceOptions options)
    {

        String configFile = "map_config.xml";
        try
        {
            VectorTileSourceOptions vectorOptions = (VectorTileSourceOptions)options;

            if (vectorOptions.getConfig() != null)
            {
                configFile = vectorOptions.getConfig();
            }
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect parameter type.  Must be a subclass of VectorTileSourceOptions");
        }

        return nativeCreateFactory(configFile);
    }
}
