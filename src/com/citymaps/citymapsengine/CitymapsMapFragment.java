package com.citymaps.citymapsengine;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.citymaps.citymapsengine.options.CitymapsMapViewOptions;

import java.util.HashMap;

/**
 * Android {@link Fragment} implementation around a {@link CitymapsMapView}.
 * <p>The implementation of the CitymapsMapView will persist past the life of this fragment.
 * <p>It can and should be reused by always using the same fragment tag.
 */
public class CitymapsMapFragment extends Fragment
{
    static
    {
        CitymapsEngine.init();
    }

    private CitymapsMapView mMapView = null;
    private CitymapsMapViewOptions mOptions = null;
    private MapViewReadyListener mListener = null;

    /**
     * Creates a new CitymapsMapFragment with the given tag, options, and listener.
     * @param options The options to pass to the CitymapsMapView. See {@link CitymapsMapView} and its subclasses for option details.
     * @param listener The listener to receive notice when the CitymapsMapView is ready for use.
     * @return A newly constructed fragment.
     */
    public static CitymapsMapFragment newInstance(CitymapsMapViewOptions options, MapViewReadyListener listener)
    {
        CitymapsMapFragment fragment = new CitymapsMapFragment();
        fragment.mOptions = options;
        fragment.mListener = listener;

        return fragment;
    }

    /**
     * Creates a new CitymapsMapFragment with the given options.
     * @param options The options to pass to the CitymapsMapView. See {@link CitymapsMapView} and its subclasses for option details.
     * @return A newly constructed fragment.
     */
    public static CitymapsMapFragment newInstance(CitymapsMapViewOptions options)
    {
        CitymapsMapFragment fragment = new CitymapsMapFragment();
        fragment.mOptions = options;

        return fragment;
    }

    public CitymapsMapFragment()
    {
        super();
        mMapView = null;
        mOptions = new CitymapsMapViewOptions();
        mListener = null;
        this.setRetainInstance(true);
    }

    /**
     * Returns the CitymapsMapView managed by this fragment, or null if the CitymapsMapView has not been created yet.
     * @return The CitymapsMapView managed by this fragment.
     */
    public CitymapsMapView getMapView()
    {
        return mMapView;
    }

    /**
     * Resets the MapViewReadyListener for this fragment.
     * @param listener New listener to this fragment. Cannot be null.
     */
    public void setMapViewReadyListener(MapViewReadyListener listener)
    {
        mListener = listener;
        if (mMapView != null)
        {
            mListener.onMapViewReady(mMapView);
        }
    }

    /**
     * Called by the Android platform when the view managed by this fragment is needed. It should not be called by the user.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.w("MapEngine", "On create view");

        mMapView = new CitymapsMapView(inflater.getContext(), mOptions);
        if (container != null)
        {
            mMapView.setLayoutParams(container.getLayoutParams());
        }

        mMapView.onCreate(savedInstanceState);

        if (mListener != null)
        {
            mListener.onMapViewReady(mMapView);
        }

        return mMapView;
    }

    /**
     * Called by the Android system when the view managed by this fragment is no longer required.
     * The user should not call this method.
     */
    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        Log.w("MapEngine", "On destroy view");
        mMapView.onDestroy();
        mMapView = null;
        mListener = null;
    }

    /**
     * Called by the android system when the instance of this fragment needs to be saved.
     * The tag the fragment was created with will be saved so that the same map can be retrieved.
     */
    @Override
    public void onSaveInstanceState(Bundle outBundle)
    {
        super.onSaveInstanceState(outBundle);

        if (mMapView != null)
        {
            mMapView.onSaveInstanceState(outBundle);
        }
    }

    /**
     * Called by the Android system when an instance of this fragment has been created.
     * If we had a previous instance of this fragment, its tag will be recovered from the bundle.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        Log.w("MapEngine", "CitymapsMapFragment on destroy");
        super.onDestroy();
    }


    @Override
    public void onLowMemory()
    {
        super.onLowMemory();

        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    /**
     * Called by the Android system when the fragment has been paused.
     * It will pause the managed CitymapsMapView if one exists.
     */
    @Override
    public void onPause() {
        Log.w("MapEngine", "CitymapsMapFragment on pause");
        super.onPause();

        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    /**
     * Called by the Android system when the fragment has been resumed.
     * It will resume the managed CitymapsMapView if one exists.
     */
    @Override
    public void onResume() {
        Log.w("MapEngine", "CitymapsMapFragment on resume");
        super.onResume();

        if (mMapView != null) {
            mMapView.onResume();
        }
    }
}
