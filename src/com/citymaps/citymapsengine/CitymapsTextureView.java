package com.citymaps.citymapsengine;

import android.content.Context;
import android.util.Log;
import android.view.TextureView;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class CitymapsTextureView extends VariableFramerateTextureView
{
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

    private CitymapsContextFactory mContextFactory = null;
    private CitymapsRenderer mRenderer = null;

    public CitymapsTextureView(Context context, CitymapsGLSurfaceListener listener, int framesPerSecond)
    {
        super(context);

        mContextFactory = new CitymapsContextFactory();
        mRenderer = new CitymapsRenderer();

        mContextFactory.setListener(listener);
        mRenderer.setListener(listener);

        this.setPreserveEGLContextOnPause(true);
        this.setFramerate(framesPerSecond);
        this.setEGLConfigChooser(new CitymapsConfigChooser());
        this.setEGLContextFactory(mContextFactory);
        this.setRenderer(mRenderer);
    }

    private static final class CitymapsConfigChooser implements EGLConfigChooser
    {
        private int mRedSize = 8;
        private int mGreenSize = 8;
        private int mBlueSize = 8;
        private int mAlphaSize = 8;
        private int mDepthSize = 8;
        private int mStencilSize = 8;

        @Override
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
        {
            int[] multiSampledConfigAttribs =
                    {       EGL10.EGL_RED_SIZE, mRedSize,
                            EGL10.EGL_GREEN_SIZE, mGreenSize,
                            EGL10.EGL_BLUE_SIZE, mBlueSize,
                            EGL10.EGL_DEPTH_SIZE, mDepthSize,
                            EGL10.EGL_ALPHA_SIZE, mAlphaSize,
                            EGL10.EGL_STENCIL_SIZE, mStencilSize,
                            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                            EGL10.EGL_SAMPLE_BUFFERS, 1,
                            EGL10.EGL_SAMPLES, 4,
                            EGL10.EGL_NONE};

            int[] basicConfigAttribs =
                    {       EGL10.EGL_RED_SIZE, mRedSize,
                            EGL10.EGL_GREEN_SIZE, mGreenSize,
                            EGL10.EGL_BLUE_SIZE, mBlueSize,
                            EGL10.EGL_DEPTH_SIZE, mDepthSize,
                            EGL10.EGL_ALPHA_SIZE, mAlphaSize,
                            EGL10.EGL_STENCIL_SIZE, mStencilSize,
                            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                            EGL10.EGL_NONE};

            int[] num_config = new int[1];

            egl.eglChooseConfig(display, multiSampledConfigAttribs, null, 0, num_config);

            int numConfigs = num_config[0];

            if (numConfigs <= 0)
            {
                Log.i("MapEngine", "Cannot create Multisampled context..");

                egl.eglChooseConfig(display, basicConfigAttribs, null, 0, num_config);

                numConfigs = num_config[0];
                if (numConfigs <= 0)
                {
                    throw new IllegalArgumentException("Unable to create valid OpenGL Context.");
                }

                EGLConfig[] configs = new EGLConfig[numConfigs];
                egl.eglChooseConfig(display, basicConfigAttribs, configs, numConfigs,
                        num_config);
                return chooseConfig(egl, display, configs);
            }

            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, multiSampledConfigAttribs, configs, numConfigs,
                    num_config);
            return chooseConfig(egl, display, configs);
        }

        private EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs)
        {
            for (EGLConfig config : configs)
            {
                int d = findConfigAttrib(egl, display, config,
                        EGL10.EGL_DEPTH_SIZE, 0);
                int s = findConfigAttrib(egl, display, config,
                        EGL10.EGL_STENCIL_SIZE, 0);

                // We need at least mDepthSize and mStencilSize bits
                if (d < 8 || s < 8)
                    continue;

                // We want an *exact* match for red/green/blue/alpha
                int r = findConfigAttrib(egl, display, config,
                        EGL10.EGL_RED_SIZE, 0);
                int g = findConfigAttrib(egl, display, config,
                        EGL10.EGL_GREEN_SIZE, 0);
                int b = findConfigAttrib(egl, display, config,
                        EGL10.EGL_BLUE_SIZE, 0);
                int a = findConfigAttrib(egl, display, config,
                        EGL10.EGL_ALPHA_SIZE, 0);

                int sampleBuffers = findConfigAttrib(egl, display, config, EGL10.EGL_SAMPLE_BUFFERS, 0);
                int samples = findConfigAttrib(egl, display, config, EGL10.EGL_SAMPLES, 0);
                if (r == mRedSize && g == mGreenSize && b == mBlueSize
                        && a == mAlphaSize)
                    return config;
            }

            throw new IllegalArgumentException("Unable to find a valid EGL configuration.");
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display,
                                     EGLConfig config, int attribute, int defaultValue) {

            int[] value = new int[1];
            if (egl.eglGetConfigAttrib(display, config, attribute, value)) {
                return value[0];
            }
            return defaultValue;
        }
    }

    private static final class CitymapsContextFactory implements EGLContextFactory
    {
        CitymapsGLSurfaceListener mListener = null;

        public void setListener(CitymapsGLSurfaceListener listener)
        {
            mListener = listener;
        }

        @Override
        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig)
        {
            int[] attrib_list =
                    {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE};

            EGLContext context = egl.eglCreateContext(display, eglConfig,
                    egl.EGL_NO_CONTEXT, attrib_list);

            return context;
        }

        @Override
        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context)
        {
            if (mListener != null)
            {
                mListener.onContextLost();
            }
            egl.eglDestroyContext(display, context);
        }
    }

    private static final class CitymapsRenderer implements Renderer
    {
        CitymapsGLSurfaceListener mListener = null;

        public void setListener(CitymapsGLSurfaceListener listener)
        {
            mListener = listener;
        }
        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config)
        {
            if (mListener == null)
                return;

            mListener.onSurfaceCreated();
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height)
        {
            if (mListener == null)
                return;

            mListener.onSurfaceChanged(width, height);
        }

        @Override
        public boolean onDrawFrame(GL10 gl)
        {
            if (mListener == null)
                return false;

            return mListener.onStep(gl);
        }
    }
}
