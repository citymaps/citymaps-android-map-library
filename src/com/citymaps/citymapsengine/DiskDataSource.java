package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.DataSourceOptions;
import com.citymaps.citymapsengine.options.DiskDataSourceOptions;

import java.util.HashMap;

/** A data source which receives its data from a local disk.
 * <p>
 * The file path is determined using a token replacement techinque.  The tokens are described as follows:
 *
 * <ul>
 *     <li>{x} - The mercator x-coordinate of the tile</li>
 *     <li>{y} - The mercator y-coordinate of the tile</li>
 *     <li>{zoom} - The zoom level of the tile</li>
 * </ul>
 *<p>
 *An example filepath:
 *
 *- /opt/data/tiles/{zoom}/{x}/{y}.png
 *
 * <p>
 *The following options are available at construction:
 * <ul>
 *     <li>filePath - The tokenized file path for the data source.</li>
 * </ul>
 *<p>
 * See {@link DataSource} for more information about data sources in general, and for additional available options.
 */
public class DiskDataSource extends DataSource
{
    public class DiskDataSourceDescription extends DataSourceDescription
    {
        String uri;
    }

    private native long nativeCreate(Object desc);

    public DiskDataSource(DiskDataSourceOptions options)
    {
        super(options);
    }

    @Override
    protected long createDataSource(DataSourceOptions options)
    {
        DiskDataSourceDescription desc = new DiskDataSourceDescription();
        this.applyOptions(options, desc);
        return nativeCreate(desc);
    }

    @Override
    protected void applyOptions(DataSourceOptions options, DataSourceDescription desc)
    {
        try {
            super.applyOptions(options, desc);
            DiskDataSourceDescription diskDesc = (DiskDataSourceDescription) desc;
            DiskDataSourceOptions diskOptions = (DiskDataSourceOptions) options;

            diskDesc.uri = diskOptions.getFilepath();

            if (diskDesc.uri == null)
            {
                throw new IllegalArgumentException("DiskDataSource requires a valid filepath.");
            }

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect parameter types.  Must be a subclass of DiskDataSource*");
        }
    }
}
