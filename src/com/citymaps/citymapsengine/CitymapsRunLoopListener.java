package com.citymaps.citymapsengine;

public interface CitymapsRunLoopListener
{
    public void onStep();
}
