package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.TileSourceOptions;

import java.util.HashMap;

/** This is an abstract base class for all tile source.
 * <p>
 * A TileSource provides the information needed for a {@link TileLayer} to render properly.  A tile source consists of two parts: A data source and a tile factory.
 * <p>
 * The tile factory is internal to the engine is not a detail of concern for the user of this SDK.  All that needs to be known is that each derived class of TileSource must implement a tile factory.
 * <p>
 * The data source, implemented as a CEDataSource, is a description object which allows the user to specify a source for tile data.  This data source must be a Mercator based tile source.  What this means is that each tile must be accessible with an x and y coordinate in the Mercator projection of the world, plus a zoom level.  An example of a mercator tile source is the OpenStreetMap web tile server.
 */

public abstract class TileSource
{
    static
    {
        CitymapsEngine.init();
    }

    protected DataSource mDataSource;
    protected long mTileFactoryPointer;
    protected long mEnginePointer;

    private native long nativeCreate(long dataSourcePtr, long tileFactoryPtr);
    private native void nativeReleaseTileSource(long enginePtr);
    private native void nativeReleaseTileFactory(long enginePtr);

    /**
     * Initializes a tile source with the data source and a list of options
     * @param dataSource The data source to be used to create tiles.
     * @param options A set of options to be used by the TileSource
     */
    public TileSource(DataSource dataSource, TileSourceOptions options)
    {
        mDataSource = dataSource;
        mDataSource.setOwnedByTileSource(true);
        mTileFactoryPointer = this.createTileFactory(options);
        mEnginePointer = this.nativeCreate(mDataSource.mEnginePointer, mTileFactoryPointer);
    }

    protected abstract long createTileFactory(TileSourceOptions options);

    public DataSource getDataSource()
    {
        return mDataSource;
    }

    @Override
    protected void finalize()
    {
        if (mEnginePointer != 0)
        {
            nativeReleaseTileSource(mEnginePointer);
        }

        if (mTileFactoryPointer != 0)
        {
            nativeReleaseTileFactory(mTileFactoryPointer);
        }

        if (mDataSource.mEnginePointer != 0)
        {
            mDataSource.nativeRelease(mDataSource.mEnginePointer);
        }
    }
}