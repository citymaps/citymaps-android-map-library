package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.LayerOptions;
import com.citymaps.citymapsengine.options.TileLayerOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/** This abstract base class is used for layers which receive tiled data from external data sources.  In order to render a tile layer, a tile source must be provided
 *
 * TileLayer accepts the following options:
 * <ul>
 *     <li>tileSource - A TileLayer object describing how this layer should load and render its data.</li>
 *     <li>tileSize - The size of the tiles used by the tile source.</li>
 *     <li>buffer - An optional buffer of tiles to load around the current screen.  Use this only if bandwidth and performance are not an issue.</li>
 * </ul>
 * <p>
 * See {@link Layer} for more available options.
 */

public abstract class TileLayer extends Layer
{
    private TileSource mTileSource;

    private native void nativeSetTileSource(long layerPtr, long sourcePtr);
    private native void nativeResetTiles(long layerPtr);
    private native GridPoint nativeGetGridPointForLonLat(long layerPtr, double lon, double lat, int zoom);

    public TileLayer(LayerOptions options) {
        super(options);
    }

    public TileSource getTileSource()
    {
        return mTileSource;
    }

    public void setTileSource(TileSource source)
    {
        nativeSetTileSource(this.mNativePtr, source.mEnginePointer);
        mTileSource = source;
    }

    @Override
    protected void applyOptions(LayerOptions options, LayerDescription desc)
            throws IllegalArgumentException {
        try {
            super.applyOptions(options, desc);
            TileLayerDescription tileDesc = (TileLayerDescription) desc;
            TileLayerOptions tileOptions = (TileLayerOptions) options;

            mTileSource = tileOptions.getTileSource();
            if (mTileSource == null)
            {
                throw new IllegalArgumentException("TileSource cannot be null when options are applied to the TileLayer.");
            }
            else
            {
                tileDesc.tileSource = mTileSource.mEnginePointer;
            }

            tileDesc.tileSize = tileOptions.getTileSize();
            tileDesc.buffer = tileOptions.getBuffer();

            tileDesc.lodZooms = new int[tileOptions.getLODZooms().size()];
            int i = 0;
            for(Integer zoom : tileOptions.getLODZooms())
            {
                tileDesc.lodZooms[i] = zoom;
                i++;
            }

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect parameter types.  Must be a subclass of TileLayer*");
        }
    }

    public void reloadTiles()
    {
        this.nativeResetTiles(mNativePtr);
    }

    public GridPoint getTileContainingLonLat(LonLat location, int zoomLevel)
    {
        return this.nativeGetGridPointForLonLat(mNativePtr, location.longitude, location.latitude, zoomLevel);
    }

    protected class TileLayerDescription extends LayerDescription {
        public long tileSource = 0;
        public int tileSize = 256;
        public int buffer = 0;
        public int[] lodZooms;
    }
}
