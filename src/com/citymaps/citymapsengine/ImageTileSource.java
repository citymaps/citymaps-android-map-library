package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.DiskDataSourceOptions;
import com.citymaps.citymapsengine.options.TileSourceOptions;
import com.citymaps.citymapsengine.options.WebDataSourceOptions;

import java.util.HashMap;

/** A tile source for an image-based tile source

 This type of data source is required for {@link ImageTileLayer}.

 See {@link TileSource} for more information about tile sources in general.
 @see TileSource
 */
public class ImageTileSource extends TileSource
{
    private native long nativeCreateFactory();

    public ImageTileSource(DataSource dataSource, TileSourceOptions options)
    {
        super(dataSource, options);
    }

    public static ImageTileSource createFromURL(String url)
    {
        return new ImageTileSource(new WebDataSource(new WebDataSourceOptions().url(url)), new TileSourceOptions());
    }

    public static ImageTileSource createFromFilepath(String path)
    {
        return new ImageTileSource(new DiskDataSource(new DiskDataSourceOptions().filepath(path)), new TileSourceOptions());
    }

    @Override
    protected long createTileFactory(TileSourceOptions options) {
        return nativeCreateFactory();
    }
}
