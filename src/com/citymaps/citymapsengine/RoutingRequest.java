package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 9/10/14.
 */
public class RoutingRequest
{
    public static final int UNIT_SYSTEM_IMPERIAL = 0;
    public static final int UNIT_SYSTEM_METRIC = 1;

    public static final int MODE_DRIVING = 1 << 1;
    public static final int MODE_WALKING = 1 << 2;
    public static final int MODE_PUBLIC_TRANSPORT = 1 << 3;
    public static final int MODE_BICYCLE = 1 << 4;
    public static final int MODE_SHORTEST = 1 << 5;
    public static final int MODE_FASTEST = 1 << 6;
    public static final int MODE_TRAFFIC_ENABLED = 1 << 7;
    public static final int MODE_TRAFFIC_DISABLED = 1 << 8;

    /** The starting position for routing. Needs to be the user's location to work with turn by turn navigation. */
    public LonLat start;
    /** The ending position for routing.*/
    public LonLat end;

    /** The number of alternative routes to return in the response. */
    public int numAlternatives = 0;

    /** The unit system to report notifications in. */
    public int units = UNIT_SYSTEM_IMPERIAL;

    /** The modes of transportation to support in the routes. */
    public int mode;

    public RoutingRequest()
    {}
}
