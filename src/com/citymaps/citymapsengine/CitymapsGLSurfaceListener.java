package com.citymaps.citymapsengine;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

interface CitymapsGLSurfaceListener
{
    public void onSurfaceCreated();

    public void onSurfaceChanged(int width, int height);

    public void onContextLost();

    public boolean onStep(GL10 gl);
}
