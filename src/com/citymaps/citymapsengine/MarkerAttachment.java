package com.citymaps.citymapsengine;

import android.graphics.Bitmap;

/** This class is used to attach an image to a marker.
 *
 */
public final class MarkerAttachment
{
    public Bitmap bitmap;
    public double imageAnchorU;
    public double imageAnchorV;
    public double markerAnchorU;
    public double markerAnchorV;
    public double width;
    public double height;
    public float alpha;
    public float rotation;
}