package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.TileSourceOptions;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: eddiekimmel
 * Date: 10/29/13
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class BusinessTileSource extends TileSource
{
    private native long nativeCreateFactory();

    public BusinessTileSource(String apiKey)
    {
        super(CitymapsDataSource.defaultBusinessDataSource(apiKey), new TileSourceOptions());
    }

    public BusinessTileSource(DataSource dataSource, TileSourceOptions options)
    {
        super(dataSource, options);
    }

    public static BusinessTileSource createWithURL(String url)
    {
        return new BusinessTileSource(new WebDataSource(url), new TileSourceOptions());
    }

    @Override
    protected long createTileFactory(TileSourceOptions options)
    {
        return nativeCreateFactory();
    }
}
