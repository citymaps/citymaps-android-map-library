package com.citymaps.citymapsengine;

import android.util.SparseArray;

import java.util.ArrayList;

/**
 * Created by eddiekimmel on 9/10/14.
 */
public class RouteInstruction
{
    public enum Direction
    {
        Forward,
        BearRight,
        LightRight,
        Right,
        HardRight,
        TurnLeft,
        HardLeft,
        Left,
        LightLeft,
        BearLeft,
        EnterTransit,
        ExitTransit,
        Unknown
    }

    public enum Action
    {
        TransitEnter,
        TransitExit,
        Depart,
        Arrive,
        ArriveLeft,
        ArriveRight,
        LeftLoop,
        LeftUTurn,
        SharpLeftTurn,
        LeftTurn,
        SlightLeftTurn,
        Continue,
        SlightRightTurn,
        RightTurn,
        SharpRightTurn,
        RightUTurn,
        RightLoop,
        LeftExit,
        RightExit,
        LeftRamp,
        RightRamp,
        LeftFork,
        MiddleFork,
        RightFork,
        LeftMerge,
        RightMerge,
        NameChange,
        TrafficCircle,
        Ferry,
        Unknown
    }

    public String description;
    public String roadName;
    public LonLat location;
    public LonLatBounds boundingBox;
    public Direction direction = Direction.Unknown;
    public Action action = Action.Unknown;
    public float distance;
    public int duration;
    public boolean isStart;
    public boolean isEnd;
    public boolean isTransitStart;
    public boolean isTransitEnd;
    public LonLat[] points;

    public void setDirection(int dir)
    {
        direction = Direction.values()[dir];
    }

    public void setAction(int act)
    {
        action = Action.values()[act];
    }
}