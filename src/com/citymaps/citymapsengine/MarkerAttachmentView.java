package com.citymaps.citymapsengine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

public class MarkerAttachmentView extends FrameLayout
{
    private PointF mViewAnchorPoint = new PointF(0,0);
    private PointF mMarkerAnchorPoint = new PointF(0,0);
    private String mKey = "";

    public MarkerAttachmentView(Context c)
    {
        super(c);
        this.setDrawingCacheEnabled(true);
    }

    public MarkerAttachmentView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public MarkerAttachmentView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public void setViewAnchorPoint(PointF p)
    {
        mViewAnchorPoint.set(p);
    }

    public void setMarkerAnchorPoint(PointF p)
    {
        mMarkerAnchorPoint.set(p);
    }

    public void setKey(String key)
    {
        mKey = key;
    }

    public PointF getViewAnchorPoint()
    {
        return new PointF(mViewAnchorPoint.x, mViewAnchorPoint.y);
    }

    public PointF getMarkerAnchorPoint()
    {
        return new PointF(mMarkerAnchorPoint.x, mMarkerAnchorPoint.y);
    }

    public String getKey()
    {
        return mKey;
    }
}
