package com.citymaps.citymapsengine;

/**
 * Created by eddiekimmel on 2/12/14.
 */

import com.citymaps.citymapsengine.options.DiskDataSourceOptions;
import com.citymaps.citymapsengine.options.TileSourceOptions;
import com.citymaps.citymapsengine.options.WebDataSourceOptions;

/** A tile source for user-defined tiles

 This type of data source is required for {@link DataTileLayer}.

 See {@link TileSource} for more information about tile sources in general.
 @see TileSource
 */
public class DataTileSource extends TileSource
{
    private native long nativeCreateFactory();

    public DataTileSource(DataSource dataSource, TileSourceOptions options)
    {
        super(dataSource, options);
    }

    public static DataTileSource createFromURL(String url)
    {
        return new DataTileSource(new WebDataSource(new WebDataSourceOptions().url(url)), new TileSourceOptions());
    }

    public static DataTileSource createFromFilepath(String path)
    {
        return new DataTileSource(new DiskDataSource(new DiskDataSourceOptions().filepath(path)), new TileSourceOptions());
    }

    @Override
    protected long createTileFactory(TileSourceOptions options) {
        return nativeCreateFactory();
    }
}
