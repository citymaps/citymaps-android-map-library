package com.citymaps.citymapsengine;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.citymaps.citymapsengine.options.MapViewOptions;

import java.util.HashMap;

/**
 * Android {@link Fragment} implementation around a {@link MapView}.
 * <p>The implementation of the MapView will persist past the life of this fragment.
 * <p>It can and should be reused by always using the same fragment tag.
 */
public class MapFragment extends Fragment
{
    static
    {
        CitymapsEngine.init();
    }

    private MapView mMapView = null;
    private MapViewOptions mOptions = null;
    private MapViewReadyListener mListener = null;
    private Bundle mSavedBundle = null;

    /**
     * Creates a new MapFragment with the given tag, options, and listener.
     * @param options The options to pass to the MapView. See {@link MapView} and its subclasses for option details.
     * @param listener The listener to receive notice when the MapView is ready for use.
     * @return A newly constructed fragment.
     */
    public static Fragment newInstance(String tag, MapViewOptions options, MapViewReadyListener listener)
    {
        MapFragment fragment = new MapFragment();
        fragment.mOptions = options;
        fragment.mListener = listener;

        return fragment;
    }

    /**
     * Creates a new MapFragment with the given options and listener.
     * @param options The options to pass to the MapView. See {@link MapView} and its subclasses for option details.
     * @param listener The listener to receive notice when the MapView is ready for use.
     * @return A newly constructed fragment.
     */
    public static MapFragment newInstance(MapViewOptions options, MapViewReadyListener listener)
    {
        MapFragment fragment = new MapFragment();
        fragment.mOptions = options;
        fragment.mListener = listener;

        return fragment;
    }

    /**
     * Creates a new MapFragment with the given options.
     * @param options The options to pass to the MapView. See {@link MapView} and its subclasses for option details.
     * @return A newly constructed fragment.
     */
    public static MapFragment newInstance(MapViewOptions options)
    {
        MapFragment fragment = new MapFragment();
        fragment.mOptions = options;

        return fragment;
    }

    public MapFragment()
    {
        super();
        mMapView = null;
        mOptions = new MapViewOptions();
        mListener = null;
        this.setRetainInstance(true);
    }

    /**
     * Returns the MapView managed by this fragment, or null if the MapView has not been created yet.
     * @return The MapView managed by this fragment.
     */
    public MapView getMapView()
    {
        return mMapView;
    }

    /**
     * Resets the MapViewReadyListener for this fragment.
     * @param listener New listener to this fragment. Cannot be null.
     */
    public void setMapViewReadyListener(MapViewReadyListener listener)
    {
        mListener = listener;
        if (mMapView != null)
        {
            mListener.onMapViewReady(mMapView);
        }
    }

    /**
     * Called by the Android platform when the view managed by this fragment is needed. It should not be called by the user.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.w("MapEngine", "On create view");

        mMapView = new MapView(inflater.getContext().getApplicationContext(), mOptions);

        if (mSavedBundle != null)
        {
            mMapView.onCreate(mSavedBundle);
        }

        if (mListener != null)
        {
            mListener.onMapViewReady(mMapView);
        }

        return mMapView;
    }

    /**
     * Called by the Android system when the view managed by this fragment is no longer required.
     * The user should not call this method.
     */
    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    /**
     * Called by the android system when the instance of this fragment needs to be saved.
     * The tag the fragment was created with will be saved so that the same map can be retrieved.
     */
    @Override
    public void onSaveInstanceState(Bundle outBundle) {
        super.onSaveInstanceState(outBundle);
    }

    /**
     * Called by the Android system when an instance of this fragment has been created.
     * If we had a previous instance of this fragment, its tag will be recovered from the bundle.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
        }
        else
        {
            mSavedBundle = savedInstanceState;
        }
    }

    /**
     * Called by the Android system when this fragment instance is finished and will be destroyed. The map will still
     * be saved by tag, so that the next fragment can reclaim it.
     */
    @Override
    public void onDestroy() {
        Log.w("MapEngine", "MapFragment on destroy");

        Log.w("MapEngine", "On destroy view");
        mMapView.onDestroy();
        mMapView = null;
        mListener = null;
        if (this.isAdded())
        {
            super.onDestroy();
        }
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    /**
     * Called by the Android system when the fragment has been paused.
     * It will pause the managed MapView if one exists.
     */
    @Override
    public void onPause() {
        Log.w("MapEngine", "MapFragment on pause");
        super.onPause();

        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    /**
     * Called by the Android system when the fragment has been resumed.
     * It will resume the managed MapView if one exists.
     */
    @Override
    public void onResume() {
        Log.w("MapEngine", "MapFragment on resume");
        super.onResume();

        if (mMapView != null) {
            mMapView.onResume();
        }
    }
}