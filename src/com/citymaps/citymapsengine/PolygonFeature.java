package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.PolygonFeatureOptions;

import java.util.ArrayList;
import java.util.List;

/** This class represents a user defined polygon to be placed on a {@link CanvasLayer}.
 *  <p>
 *      The polygon should be non-self-intersecting. The holes should not go outside the bounds of the containing polygon.
 *  </p>
 */
public class PolygonFeature extends CanvasFeature
{
    private List<PolygonFeature> mHoles;
    private List<LonLat> mPoints;

    private native void nativeAddPoint(long ptr, double x, double y);
    private native void nativeAddPoints(long ptr, double[] points, int count);
    private native void nativeSetPoints(long ptr, double[] points, int count);
    private native void nativeAddHole(long ptr, long holePtr);
    private native void nativeRemoveHole(long ptr, long holePtr);
    private native void nativeRemoveAllHoles(long ptr);

    public PolygonFeature()
    {
        this(new PolygonFeatureOptions());
    }

    public PolygonFeature(PolygonFeatureOptions options)
    {
        super(options);
        mHoles = options.getHoles();
        mPoints = options.getPoints();
    }

    protected long createFeature()
    {
        return this.nativeCreateShape(CanvasFeatureType.CanvasFeaturePolygon.getValue());
    }

    /**
     * Adds a point to this polygon.
     * @param point The point to add to the polygon
     */
    public void addPoint(LonLat point)
    {
        mPoints.add(point);
        this.nativeAddPoint(mNativePtr, point.longitude, point.latitude);
    }

    /**
     * Adds a list of points to this polygon.
     * @param points The points to add to the polygon
     */
    public void addPoints(List<LonLat> points)
    {
        double[] doubleArray = new double[points.size() * 2];

        for(int i = 0; i < points.size(); ++i)
        {
            LonLat point = points.get(i);
            mPoints.add(point);
            int index = i * 2;
            doubleArray[index] = point.longitude;
            doubleArray[index + 1] = point.latitude;
        }

        this.nativeAddPoints(mNativePtr, doubleArray, points.size());
    }

    /**
     * Adds a list of points to this polygon.
     * @param points The points to add to the polygon
     */
    public void addPoints(LonLat... points)
    {
        double[] doubleArray = new double[points.length * 2];

        for(int i = 0; i < points.length; ++i)
        {
            LonLat point = points[i];
            mPoints.add(point);
            int index = i * 2;
            doubleArray[index] = point.longitude;
            doubleArray[index + 1] = point.latitude;
        }

        this.nativeAddPoints(mNativePtr, doubleArray, points.length);
    }

    /**
     * Sets the points of this polygons
     * @param points The points to set as the polygon
     */
    public void setPoints(List<LonLat> points)
    {
        double[] doubleArray = new double[points.size() * 2];
        mPoints.clear();

        for(int i = 0; i < points.size(); ++i)
        {
            LonLat point = points.get(i);
            mPoints.add(new LonLat(point.longitude, point.latitude));
            int index = i * 2;
            doubleArray[index] = point.longitude;
            doubleArray[index + 1] = point.latitude;
        }

        this.nativeSetPoints(mNativePtr, doubleArray, points.size());
    }

    /**
     * Returns a copy of the points of this polygon.
     * @return Copy of the points of this polygon.
     */
    public ArrayList<LonLat> getPoints()
    {
        return new ArrayList<LonLat>(mPoints);
    }

    /**
     * Add a hole to this polygon. The hole should be completely contained within the polygon and should not be self-intersecting. The hole should not contain any holes itself.
     * @param hole The polygon to add to this polygon as a hole.
     */
    public void addHole(PolygonFeature hole) {
        if (hole == this)
            return;
        if (!mHoles.contains(hole)) {
            mHoles.add(hole);
            this.nativeAddHole(this.mNativePtr, hole.mNativePtr);
        }
    }

    /**
     * Removes a hole from this polygon.
     * @param hole The polygon to remove from this polygon.
     */
    public void removeHole(PolygonFeature hole) {
        if (hole == this)
            return;

        if (mHoles.contains(hole)) {
            mHoles.remove(hole);
            this.nativeRemoveHole(this.mNativePtr, hole.mNativePtr);
        }
    }

    /**
     * Removes all holes from this polygon.
     */
    public void removeAllHoles()
    {
        mHoles.clear();
        this.nativeRemoveAllHoles(this.mNativePtr);
    }

    public ArrayList<PolygonFeature> getHoles()
    {
        return new ArrayList<PolygonFeature>(mHoles);
    }
}