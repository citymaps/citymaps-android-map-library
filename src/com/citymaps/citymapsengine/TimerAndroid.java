package com.citymaps.citymapsengine;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class TimerAndroid {
    private long mTimer;

    Timer mJavaTimer;
    boolean mIsCancelled;
    int mInterval;

    //int mC;
    //static int c = 0;
    private static native void nativeTimerCallback(long timer);

    public TimerAndroid(final int interval, final long timer) {
        mTimer = timer;
        mIsCancelled = false;
        mJavaTimer = null;
        mInterval = interval;
        //mC = c;
        //Log.i("MapEngine", "New TimerAndroid: " + mC);
        //c++;
    }

    public synchronized void StartTimer() {

        //Log.i("MapEngine", "Requesting new timer: " + mC);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (TimerAndroid.this) {
                    if (mIsCancelled) {
                        //Log.i("MapEngine", "New timer was cancelled: " + mC);
                        mIsCancelled = false;
                        return;
                    }
                    //Log.i("MapEngine", "New timer: " + mC);
                    mJavaTimer = new Timer(true);
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            synchronized (TimerAndroid.this) {
                                if (!mIsCancelled) {
                                    //Log.i("MapEngine", "Callback: " + mC);
                                    nativeTimerCallback(mTimer);
                                } else {
                                    //Log.i("MapEngine", "Timer cancelled, not making callback: " + mC);
                                }
                            }
                        }
                    };
                    mJavaTimer.scheduleAtFixedRate(task, mInterval, mInterval);
                }
            }
        };

        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public synchronized void RemoveTimer() {
        //Log.i("MapEngine", "Cancelling Timer: " + mC);
        if (mJavaTimer != null) {
            mJavaTimer.cancel();
            mJavaTimer.purge();
        }

        mIsCancelled = true;
        mJavaTimer = null;
    }

    @Override
    protected void finalize() {
        try {
            super.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //Log.i("MapEngine", "Finalize timer");
        this.RemoveTimer();
    }
}