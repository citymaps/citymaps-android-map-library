package com.citymaps.citymapsengine;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import com.citymaps.citymapsengine.options.*;

import java.util.HashMap;

/**
 * This class allows a user to create a new map view which is already predefined to work with Citymaps.  No setup is required by the user.
 *
 */

public class CitymapsMapView extends MapView {

    static
    {
        CitymapsEngine.init();
    }

    protected class CitymapsMapViewImpl {
        private VectorLayer mBaseLayer;
        private BusinessLayer mBusinessLayer;
        private RegionLayer mRegionLayer;
    }

    private static CitymapsMapViewImpl sImpl = null;

    private VectorLayer mBaseLayer;
    private BusinessLayer mBusinessLayer;
    private RegionLayer mRegionLayer;

    /** Create a new Citymaps map view.
     *
     *@param context The context for the running application.
     *@param options Options for the map and layers. See {@link MapView} and the Layers for more details about options.
     *                @see MapView
     *                @see VectorLayer
     *                @see BusinessLayer
     *                @see RegionLayer
     *                @see CanvasLayer
     *                @see TileLayer
     *                @see Layer
     */
    public CitymapsMapView(Context context, CitymapsMapViewOptions options)
    {
        super(context, options);

        CitymapsMapView.setApplicationContext(context);

        // Apply map options
        if (DeviceType.isLowEnd(context))
        {
            this.setMapOption(MAP_OPTION_UPDATE, MAP_OPTION_VALUE_LOW);
            this.setMapOption(MAP_OPTION_ANIMATION, MAP_OPTION_VALUE_OFF);
            this.setMapOption(MAP_OPTION_KINETIC_PAN, MAP_OPTION_VALUE_OFF);
            this.setMapOption(MAP_OPTION_KINETIC_ZOOM, MAP_OPTION_VALUE_OFF);
            this.setMapOption(MAP_OPTION_KINETIC_ROTATE, MAP_OPTION_VALUE_OFF);
            this.setMapOption(MAP_OPTION_ROTATION, MAP_OPTION_VALUE_OFF);
        }
    }

    @Override
    protected void onInit(int width, int height)
    {
        this.init((CitymapsMapViewOptions)this.getOptions());
        super.onInit(width, height);
    }

    private void init(CitymapsMapViewOptions options)
    {
        if (sImpl == null)
        {
            sImpl = new CitymapsMapViewImpl();

            String apiKey = options.getAPIKey();
            if (apiKey.length() == 0) {
                try {
                    ApplicationInfo ai = this.getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), PackageManager.GET_META_DATA);
                    Bundle bundle = ai.metaData;
                    if (bundle.containsKey("com.citymaps.citymapsengine.API_KEY")) {
                        apiKey = bundle.getString("com.citymaps.citymapsengine.API_KEY");
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }

            mBaseLayer = new VectorLayer(apiKey, options.getStyleFile());
            sImpl.mBaseLayer = mBaseLayer;
            this.setBaseLayer(sImpl.mBaseLayer);

            mBusinessLayer = new BusinessLayer(apiKey);
            sImpl.mBusinessLayer = mBusinessLayer;
            this.addLayer(sImpl.mBusinessLayer);

            mRegionLayer = new RegionLayer(apiKey);
            sImpl.mRegionLayer = mRegionLayer;
            this.addLayer(sImpl.mRegionLayer);
        }
        else
        {
            mBaseLayer = sImpl.mBaseLayer;
            mBusinessLayer = sImpl.mBusinessLayer;
            mRegionLayer = sImpl.mRegionLayer;
        }

        mBaseLayer.setVisible(options.getBaseLayerVisible());
        mBusinessLayer.setVisible(options.getBusinessLayerVisible());
        mRegionLayer.setVisible(options.getRegionLayerVisible());

        this.setBackgroundColor(Color.argb(255, 232, 241, 245));
        this.startUpdatingOrientation();
        this.startUpdatingLocation();

        CitymapsMapView.setApplicationContext(this.getContext().getApplicationContext());
    }

    /**
     * Set the delegate for the BusinessLayer. The delegate will receive touch events that occur on displayed businesses.
     * @param listener The listener to receive events.
     */
    public void setBusinessOnTouchListener(BusinessOnTouchListener listener)
    {
        mBusinessLayer.setBusinessListener(listener);
    }

    public BusinessLayer getBusinessLayer()
    {
        return mBusinessLayer;
    }

    public RegionLayer getRegionLayer()
    {
        return mRegionLayer;
    }

    public VectorLayer getVectorLayer()
    {
        return mBaseLayer;
    }

    /** Apply a business filter to the map
     *
     *@param filter The filter to apply
     */
    public void applyBusinessFilter(BusinessFilter filter) {
        if (filter == null)
        {
            removeBusinessFilter();
        }
        else
        {
            mBusinessLayer.applyBusinessFilter(filter);
            mRegionLayer.setVisible(false);
        }
    }

    /** Remove any active business filter */
    public void removeBusinessFilter() {
        mBusinessLayer.removeBusinessFilter();
        mRegionLayer.setVisible(true);
    }

    /** Current active business Filter */
    public BusinessFilter getFilter() {
        return mBusinessLayer.getFilter();
    }

    /** Add an additional business to an active filter
     * @param data The business data for this business
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void addBusinessToActiveFilter(BusinessData data, int state)
    {
        mBusinessLayer.addBusinessToActiveFilter(data, state);
    }

    /** Remove a business from an active filter
     * @param bid The business ID to remove
     */
    public void removeBusinessFromActiveFilter(String bid)
    {
        mBusinessLayer.removeBusinessFromActiveFilter(bid);
    }

    /**
     * Set the state of a particulate business.
     *
     * @param bid The business ID
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.  Set this to remove a business state.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void setBusinessState(String bid, int state) {
        mBusinessLayer.setBusinessState(bid, state);
    }

    /** Reset the state of all businesses to {@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} */
    public void resetBusinessStates() {
        mBusinessLayer.resetBusinessStates();
    }

    /** Add a business ID to the blacklist.  Once a business is added to the blacklist, it will not show up for this session.
     *
     *@param bid - The business ID to blacklist
     */
    public void addBusinessToBlacklist(String bid) {
        mBusinessLayer.addBusinessToBlacklist(bid);
    }

    /** Remove a business ID from the blacklist.
     *
     * @param bid - The business ID to remove from the blacklist.
     */
    public void removeBusinessFromBlacklist(String bid) {
        mBusinessLayer.removeBusinessFromBlacklist(bid);
    }
}