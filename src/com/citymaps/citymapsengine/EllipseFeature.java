package com.citymaps.citymapsengine;

import android.graphics.PointF;
import com.citymaps.citymapsengine.options.EllipseFeatureOptions;

/** This class represents a user defined ellipse to be placed on a {@link CanvasLayer}. */
public class EllipseFeature extends CanvasFeature {

    private LonLat mCenter = new LonLat(0, 0);
    private Size mRadii = new Size(10, 5);


    public EllipseFeature() {
        this(new EllipseFeatureOptions());
    }

    public EllipseFeature(LonLat center, Size radii)
    {

    }

    public EllipseFeature(EllipseFeatureOptions options)
    {
        super(options);
        this.setCenter(options.getCenter());
        this.setRadii(options.getRadii());
    }

    private native void nativeSetPosition(long ptr, double x, double y);

    private native void nativeSetRadii(long ptr, float radiusX, float radiusY);

    protected long createFeature() {
        long ptr = this.nativeCreateShape(CanvasFeatureType.CanvasFeatureEllipse.getValue());
        return ptr;
    }

    /**
     * Sets the center of the ellipse.
     */
    public void setCenter(LonLat point) {
        this.nativeSetPosition(mNativePtr, point.longitude, point.latitude);
    }

    /**
     * Sets the x and y radii of the ellipse in mercator coordinates.
     */
    public void setRadii(Size radii) {
        this.nativeSetRadii(mNativePtr, (float)radii.width, (float)radii.height);
    }

    /**
     * Gets the center of the ellipse.
     */
    public LonLat getPosition() {
        return new LonLat(mCenter);
    }

    /**
     * Gets the x and y radii of the ellipse.
     */
    public Size getRadii() {
        return mRadii;
    }
}
