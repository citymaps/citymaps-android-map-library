package com.citymaps.citymapsengine;

/** Simple object representing a size. */

public class Size {
    public double width;
    public double height;

    /** Construct a new size with a width and height.
     *
     * @param width
     * @param height
     */

    public Size(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public Size(Size s)
    {
        this.width = s.width;
        this.height = s.height;
    }

}
