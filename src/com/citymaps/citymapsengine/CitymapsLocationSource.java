package com.citymaps.citymapsengine;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

public class CitymapsLocationSource implements LocationSource, LocationListener
{
    private LocationChangeListener mListener = null;
    private LocationManager mLocationManager = null;
    private Location mLastKnownLocation = null;
    LocationParams mParams = null;

    public CitymapsLocationSource(final LocationParams param, Context context)
    {
        mParams = param;
        if (mParams == null)
            throw new IllegalArgumentException("LocationParams cannot be null.");

        // Acquire a reference to the system Location Manager
        mLocationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
    }

    public LocationParams getParams()
    {
        return mParams;
    }

    public void setParams(LocationParams params)
    {
        mParams = params;
    }

    public void onLocationChanged(Location location)
    {
        if (mListener != null)
        {
            mLastKnownLocation = location;
            mListener.onLocationChanged(location);
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {

    }

    public void onProviderDisabled(String provider) {

    }

    public void activate(final LocationChangeListener listener)
    {
        if (listener == null)
        {
            throw new IllegalArgumentException("LocationChangeListener cannot be null in activate.");
        }

        this.deactivate();

        mListener = listener;
        Location bestLocation = getLastKnownLocation();
        if (bestLocation != null && mListener != null)
        {
            mListener.onLocationChanged(bestLocation);
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run()
            {
                switch (mParams.getAccuracy())
                {
                    case LocationParams.LOCATION_ACCURACY_FINE:
                        mLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                (long) (mParams.getMinTime() * 1000),
                                mParams.getMinDistance(), CitymapsLocationSource.this);
                        break;
                    case LocationParams.LOCATION_ACCURACY_COURSE:
                        mLocationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                (long) (mParams.getMinTime() * 1000),
                                mParams.getMinDistance(), CitymapsLocationSource.this);
                }
            }
        };

        new Handler(Looper.getMainLooper()).post(runnable);

    }

    public void deactivate()
    {
        if (mListener == null)
            return;

        mListener = null;
        final CitymapsLocationSource instance = this;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mLocationManager.removeUpdates(instance);
            }
        });
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public Location getLastKnownLocation()
    {
        if (mLastKnownLocation != null)
            return mLastKnownLocation;

        Location bestLocation = null;

        List<String> providers = mLocationManager.getAllProviders();
        for (String provider : providers) {
            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location != null) {
                if (bestLocation == null) {
                    bestLocation = location;
                } else {
                    if (location.getAccuracy() > bestLocation.getAccuracy()) {
                        bestLocation = location;
                    }
                }
            }
        }

        return bestLocation;
    }
}
