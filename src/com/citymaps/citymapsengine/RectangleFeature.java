package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.RectangleFeatureOptions;

/**
 * This class represents a user defined rectangle to be placed on a {@link CanvasLayer}.
 */
public class RectangleFeature extends CanvasFeature
{
    LonLat mPosition = new LonLat(0,0);
    Size mSize = new Size(50, 50);

    public RectangleFeature() {
        this(new RectangleFeatureOptions());
    }

    public RectangleFeature(RectangleFeatureOptions options)
    {
        super(options);
        this.setPosition(options.getPosition());
        this.setSize(options.getSize());
    }

    private native void nativeSetPosition(long ptr, double x, double y);
    private native void nativeSetSize(long ptr, float x, float y);

    protected long createFeature() {
        return this.nativeCreateShape(CanvasFeatureType.CanvasFeatureRectangle.getValue());
    }

    /** Sets the center of this rectangle. */
    public void setPosition(LonLat point) {
        mPosition = new LonLat(point);
        this.nativeSetPosition(mNativePtr, point.longitude, point.latitude);
    }

    /** Sets the half-extents of this rectangle. */
    public void setSize(float width, float height) {
        mSize = new Size(width ,height);
        this.nativeSetSize(mNativePtr, width, height);
    }

    /** Sets the half-extents of this rectangle. */
    public void setSize(Size size) {
        mSize = size;
        this.nativeSetSize(mNativePtr, (float)size.width, (float)size.height);
    }

    /** Gets the center of this rectangle. */
    public LonLat getPosition() {
        return new LonLat(mPosition);
    }

    /** Gets the half-extents of this rectangle. */
    public Size getSize() {
        return new Size(mSize);
    }
}
