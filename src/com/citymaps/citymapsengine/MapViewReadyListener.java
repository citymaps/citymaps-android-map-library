package com.citymaps.citymapsengine;

/** This listener is used by {@link MapView} to notify the application when the map is ready for use
 *
 */

public interface MapViewReadyListener {

    /** Notifies the listener that the map is ready for use */
    public void onMapViewReady(MapView view);
}
