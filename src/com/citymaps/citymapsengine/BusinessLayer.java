package com.citymaps.citymapsengine;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.citymaps.citymapsengine.events.BusinessEvent;
import com.citymaps.citymapsengine.events.BusinessTouchEvent;
import com.citymaps.citymapsengine.events.BusinessZoneEvent;
import com.citymaps.citymapsengine.options.BusinessLayerOptions;
import com.citymaps.citymapsengine.options.LayerOptions;
import org.apache.http.auth.InvalidCredentialsException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/** A Citymaps-specific layer.  Used to load and show businesses from the Citymaps database. Some control can be exercised over which businesses show up and how they display.

 See {@link TileLayer} for a list of available options.
 @see TileLayer
 */
public class BusinessLayer extends TileLayer
{
    public class BusinessState
    {
        /** The default state of businesses.  Set this to remove a business state. */
        public static final int BusinessStateNormal = 0;

        /** Doubles the size of the business.*/
        public static final int BusinessState2X = 1;

        /** Puts a blue halo around the business. */
        public static final int BusinessStateSelected = 2;

        /** Hides the business from view. */
        public static final int BusinessStateHidden = 3;
    }

    /*
     * JNI Native methods.
	 * Defined in CitymapsMapView.cpp
	 */
    private native long nativeCreateLayer(TileLayerDescription desc);
    private native void nativeApplyBusinessFilter(long layerPointer, long filterPointer);
    private native void nativeRemoveBusinessFilter(long layerPointer);
    private native void nativeSetBusinessState(long layerPointer, String bid, int state);
    private native void nativeResetBusinessStates(long layerPointer);
    private native void nativeAddBusinessToBlacklist(long layerPointer, String bid);
    private native void nativeRemoveBusinessFromBlacklist(long layerPointer, String bid);
    private native void nativeAddBusinessToActiveFilter(long layerPointer, BusinessData bid, int state);
    private native void nativeRemoveBusinessFromActiveFilter(long layerPointer, String bid);
    private native void nativeSetBusinessListener(long layerPointer, Object listener);

    private native void nativeAddMarkerGroupConstraint(long layerPointer, long markerGroupPointer);
    private native void nativeRemoveMarkerGroupConstraint(long layerPointer, long markerGroupPointer);

    private native void nativeAddMarkerToBusiness(long layerPointer, String bid, long markerPointer);
    private native void nativeRemoveMarkerFromBusiness(long layerPointer, String bid, long markerPointer);
    private native void nativeRemoveMarkersFromBusiness(long layerPointer, String bid);
    private native void nativeRemoveAllMarkersFromBusinesses(long layerPointer);
    private native boolean nativeAddBusiness(long layerPointer, BusinessData data, short visibility);
    private native void nativeSetVisibilityRating(long layerPointer, String bid, short visibility);

    private BusinessOnTouchListener mBusinessListener = null;
    private BusinessFilter mFilter = null;
    private ArrayList<MarkerGroup> mMarkerConstraints = new ArrayList<MarkerGroup>();
    private HashMap<String, ArrayList<Marker>> mBusinessMarkerAttachments = new HashMap<String, ArrayList<Marker>>();

    public BusinessLayer(LayerOptions options) {
        super(options);
    }

    public BusinessLayer(String apiKey)
    {
        this(new BusinessLayerOptions(apiKey));
    }

    /**
     * Set the delegate for the BusinessLayer. The delegate will receive touch events that occur on displayed businesses.
     * @param listener The listener to receive events.
     */
    public void setBusinessListener(BusinessOnTouchListener listener)
    {
        mBusinessListener = listener;
        nativeSetBusinessListener(mNativePtr, mBusinessListener);
    }

    /** Add an additional business to an active filter
     * @param data The business data for this business
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.</li>
     *  <li>{@link BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void addBusinessToActiveFilter(BusinessData data, int state)
    {
        if (mFilter != null) {
            mFilter.addBusiness(data, state);
            nativeAddBusinessToActiveFilter(mNativePtr, data, state);
        }
    }

    /** Remove a business from an active filter
     * @param bid The business ID to remove
     */
    public void removeBusinessFromActiveFilter(String bid)
    {
        if (mFilter != null) {
            mFilter.removeBusiness(bid);
            nativeRemoveBusinessFromActiveFilter(mNativePtr, bid);
        }
    }

    /** Apply a business filter to the map
     * @param filter The filter to apply
     */
    public void applyBusinessFilter(BusinessFilter filter) {
        if (filter == null) {
            removeBusinessFilter();
        }
        else {
            mFilter = filter;
            this.nativeApplyBusinessFilter(mNativePtr, filter.mBusinessFilterPointer);
        }
    }

    /** Remove any active business filter */
    public void removeBusinessFilter() {
        this.nativeRemoveBusinessFilter(mNativePtr);
        mFilter = null;
    }

    /** Current active business Filter */
    public BusinessFilter getFilter() {
        return mFilter;
    }

    /**
     * Set the state of a particulate business.
     *
     * @param bid The business ID
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.  Set this to remove a business state.</li>
     *  <li>{@link BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void setBusinessState(String bid, int state) {
        if (bid == null || state > 3) {
            return;
        }

        this.nativeSetBusinessState(mNativePtr, bid, state);
    }

    /** Reset the state of all businesses to {@link BusinessState#BusinessStateNormal BusinessStateNormal} */
    public void resetBusinessStates() {
        this.nativeResetBusinessStates(mNativePtr);
    }

    public void addMarkerGroupConstraint(MarkerGroup group)
    {
        if (mMarkerConstraints.contains(group)) {
            return;
        }

        mMarkerConstraints.add(group);
        nativeAddMarkerGroupConstraint(mNativePtr, group.mNativePointer);
    }

    public void removeMarkerGroupConstraint(MarkerGroup group)
    {
        mMarkerConstraints.remove(group);
        nativeRemoveMarkerGroupConstraint(mNativePtr, group.mNativePointer);
    }

    public void removeAllMarkerGroupConstraints()
    {
        for(MarkerGroup group : mMarkerConstraints) {
            nativeRemoveMarkerGroupConstraint(mNativePtr, group.mNativePointer);
        }

        mMarkerConstraints.clear();
    }

    /** Adds a child marker to the given business. If a marker is added in this way, it must be removed with removeMarkerFromBusiness.
     * You should not call removeFromParent on a marker added in this way.
     * @param businessId The business to add the marker to.
     * @param marker The marker to add.
     */
    public void addMarkerToBusiness(String businessId, Marker marker)
    {
        if (businessId == null || marker == null) {
            return;
        }

        ArrayList<Marker> markers = mBusinessMarkerAttachments.get(businessId);
        if (markers == null) {
            markers = new ArrayList<Marker>();
            mBusinessMarkerAttachments.put(businessId, markers);
        }

        if (!markers.contains(marker)) {
            nativeAddMarkerToBusiness(mNativePtr, businessId, marker.mNativePointer);
            markers.add(marker);
        }
    }

    /** Removes a marker from the given business.
     * @param businessId The business to remove the marker from.
     * @param marker The marker to remove.
     */
    public void removeMarkerFromBusiness(String businessId, Marker marker)
    {
        if (businessId == null || marker == null) {
            return;
        }

        ArrayList<Marker> markers = mBusinessMarkerAttachments.get(businessId);
        if (markers == null) {
            return;
        }

        if (markers.contains(marker)) {
            nativeRemoveMarkerFromBusiness(mNativePtr, businessId, marker.mNativePointer);
            markers.remove(marker);

            if (markers.size() == 0)
                mBusinessMarkerAttachments.remove(businessId);
        }
    }

    /**
     * Removes all markers from a given business.
     * @param businessId The business to remove markers from.
     */
    public void removeAllMarkersFromBusiness(String businessId)
    {
        if (businessId == null) {
            return;
        }

        nativeRemoveMarkersFromBusiness(mNativePtr, businessId);
        mBusinessMarkerAttachments.remove(businessId);
    }

    /**
     * Removes all markers from all businesses.
     */
    public void removeAllMarkersFromBusinesses()
    {
        nativeRemoveAllMarkersFromBusinesses(mNativePtr);
        mBusinessMarkerAttachments.clear();
    }

    public ArrayList<Marker> getMarkersForBusinesss(String business)
    {
        if (business == null || !mBusinessMarkerAttachments.containsKey(business)) {
            return null;
        }

        return new ArrayList<Marker>(mBusinessMarkerAttachments.get(business));
    }

    /** Add a business ID to the blacklist.  Once a business is added to the blacklist, it will not show up for this session.

     @param bid The business ID to blacklist
     */
    public void addBusinessToBlacklist(String bid) {
        if (bid == null) {
            return;
        }

        nativeAddBusinessToBlacklist(mNativePtr, bid);
    }

    /** Remove a business ID from the blacklist.

     @param bid The business ID to remove from the blacklist.
     */
    public void removeBusinessFromBlacklist(String bid) {
        if (bid == null)
            return;

        nativeRemoveBusinessFromBlacklist(mNativePtr, bid);
    }

    public boolean addBusiness(Context context, BusinessData data, short visibility) throws InvalidCredentialsException {
        if (data == null)
            return false;

        if (!context.getPackageName().contains("citymaps"))
        {
            throw new InvalidCredentialsException("You are not authorized to use this API.");
        }
        else
        {
            return nativeAddBusiness(mNativePtr, data, visibility);
        }
    }

    /** Add a custom visibility score to a business.
     *
     * @param businessId The business id to add the visibility score to.
     * @param visibility The visibility score to add. By default, businesses have a visibility range of 0-100. The visibility parameter will be added to the original value to determine the final value.
     */
    public void setVisibilityRating(String businessId, short visibility)
    {
        this.nativeSetVisibilityRating(mNativePtr, businessId, visibility);
    }

    @Override
    protected long createLayer(LayerOptions options) {
        BusinessLayerDescription desc = new BusinessLayerDescription();
        this.applyOptions(options, desc);

        return this.nativeCreateLayer(desc);
    }

    @Override
    protected void applyOptions(LayerOptions options, LayerDescription desc)
            throws IllegalArgumentException {
        try {
            super.applyOptions(options, desc);
            BusinessLayerDescription businessDesc = (BusinessLayerDescription) desc;
            BusinessLayerOptions businessOptions = (BusinessLayerOptions)options;

            businessDesc.imageHostname = businessOptions.getHostname();
            businessDesc.logoURL = businessOptions.getLogoURL();
            businessDesc.categoryURL = businessOptions.getCategoryURL();

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect layer description type.  Must be a subclass of BusinessLayerDescription");
        }
    }

    private void onBusinessEvent(final BusinessData data, final int event)
    {
        if (mBusinessListener != null) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (mBusinessListener != null) {
                        mBusinessListener.onBusinessEvent(new BusinessEvent(data, event));
                    }
                }
            };

            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    private void onTouchEvent(final BusinessData data, final int event)
    {
        if (mBusinessListener != null) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                if (mBusinessListener != null)
                    mBusinessListener.onTouchEvent(new BusinessTouchEvent(data, event));
                }
            };

            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    private void onZoneEvent(final BusinessData data, final int event, final int zone)
    {
        if (mBusinessListener != null) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (mBusinessListener != null)
                        mBusinessListener.onZoneEvent(new BusinessZoneEvent(data, event, zone));
                }
            };

            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    protected void onBusinessTapped(BusinessData data) {
        this.onTouchEvent(data, BusinessTouchEvent.TAP);
    }

    protected void onBusinessDoubleTapped(BusinessData data) {
        this.onTouchEvent(data, BusinessTouchEvent.DOUBLE_TAP);
    }

    protected void onBusinessLongPressed(BusinessData data) {
        this.onTouchEvent(data, BusinessTouchEvent.LONG_PRESS);
    }

    protected void onBusinessEnterZone(BusinessData data, int zone)
    {
        this.onZoneEvent(data, BusinessZoneEvent.ENTER, zone);
    }

    protected void onBusinessExitZone(BusinessData data, int zone)
    {
        this.onZoneEvent(data, BusinessZoneEvent.EXIT, zone);
    }

    protected void onBusinessAdded(BusinessData data)
    {
        this.onBusinessEvent(data, BusinessEvent.ADDED);
    }

    protected void onBusinessRemoved(BusinessData data)
    {
        this.onBusinessEvent(data, BusinessEvent.REMOVED);
    }

    protected class BusinessLayerDescription extends TileLayerDescription {
        String imageHostname = "r.citymaps.com";
        String logoURL = "/riak/business_logos/{id}_150x150";
        String categoryURL = "/riak/category_icons5_app/{id}";
    }
}
