package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.SquareFeatureOptions;

/**
 * This class represents a user defined square to be placed on a {@link CanvasLayer}.
 */
public class SquareFeature extends CanvasFeature
{
    LonLat mPosition = new LonLat(0,0);
    float mSize = 50;

    public SquareFeature() {
        this(new SquareFeatureOptions());
    }

    public SquareFeature(SquareFeatureOptions options)
    {
        super(options);
        this.setPosition(options.getPosition());
        this.setSize(options.getSize());
    }

    private native void nativeSetPosition(long ptr, double x, double y);

    private native void nativeSetSize(long ptr, float r);

    protected long createFeature() {
        long ptr = this.nativeCreateShape(CanvasFeatureType.CanvasFeatureSquare.getValue());
        return ptr;
    }

    /**
     * Sets the center of the square.
    */
    public void setPosition(LonLat point)
    {
        mPosition = new LonLat(point);
        this.nativeSetPosition(mNativePtr, point.longitude, point.latitude);
    }

    /**
     * Sets the half-width of the square.
     */
    public void setSize(float length)
    {
        mSize = length;
        this.nativeSetSize(mNativePtr, length);
    }

    /**
     * Gets the center of the square.
     */
    public LonLat getPosition()
    {
        return new LonLat(mPosition);
    }

    /**
     * Gets the half-width of the square.
     */
    public float getSize() {
        return mSize;
    }
}
