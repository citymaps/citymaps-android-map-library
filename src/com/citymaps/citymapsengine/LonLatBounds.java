package com.citymaps.citymapsengine;

import android.os.Parcel;
import android.os.Parcelable;

/** Simple object to represent a longitude/latitude bounding box. */
public class LonLatBounds implements Parcelable {
    public LonLat min;
    public LonLat max;

    /** Create a new bounds with min/max positions
     *
     * @param min
     * @param max
     */

    public LonLatBounds(LonLat min, LonLat max) {
        this.min = new LonLat(min.longitude, min.latitude);
        this.max = new LonLat(max.longitude, max.latitude);
    }

    /** Create a new bounds with min/max coordinates.
     *
     * @param minLon
     * @param minLat
     * @param maxLon
     * @param maxLat
     */

    public LonLatBounds(double minLon, double minLat, double maxLon, double maxLat) {
        this.min = new LonLat(minLon, minLat);
        this.max = new LonLat(maxLon, maxLat);
    }

    private LonLatBounds(Parcel in) {
        this.min = in.readParcelable(LonLat.class.getClassLoader());
        this.max = in.readParcelable(LonLat.class.getClassLoader());
    }

    /** Get the center of the bounding box.
     *
     * @return The center point of the bounding box.
     */

    public LonLat getCenter() {
        return new LonLat((min.longitude + max.longitude) / 2.0, (min.latitude + max.latitude) / 2.0);
    }

    public void set(LonLatBounds bounds)
    {
        min.set(bounds.min);
        max.set(bounds.max);
    }

    /** Whether this bounds contains another
     *
     * @param other
     * @return Whether this bounds contains the other
     */

    public boolean contains(LonLatBounds other)
    {
        return !(min.longitude > other.min.longitude ||
                min.latitude > other.min.latitude ||
                max.longitude < other.max.longitude ||
                max.latitude < other.max.latitude);
    }

    /** Whether this bounds contains a point
     *
     * @param point
     * @return Whether this bounds contains the point
     */

    public boolean contains(LonLat point)
    {
        return !(min.longitude > point.longitude ||
                min.latitude > point.latitude ||
                max.longitude < point.longitude ||
                max.latitude < point.latitude);
    }

    /** Whether this bounds intersects another
     *
     * @param other
     * @return Whether these two intersect
     */

    public boolean intersects(LonLatBounds other)
    {
        return !(min.longitude > other.max.longitude ||
                min.latitude > other.max.latitude ||
                max.longitude < other.min.longitude ||
                max.latitude < other.min.latitude);
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof LonLatBounds) {
            LonLatBounds bounds = (LonLatBounds)other;
            return min.equals(bounds.min) &&
                    max.equals(bounds.max);
        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(min, 0);
        dest.writeParcelable(max, 0);
    }

    public static final Creator<LonLatBounds> CREATOR = new Creator<LonLatBounds>() {
        @Override
        public LonLatBounds createFromParcel(Parcel source) {
            return new LonLatBounds(source);
        }

        @Override
        public LonLatBounds[] newArray(int size) {
            return new LonLatBounds[size];
        }
    };
}
