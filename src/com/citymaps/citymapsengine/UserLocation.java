package com.citymaps.citymapsengine;

import android.os.Parcel;
import android.os.Parcelable;

/** A simple object to represent a user's location */

public class UserLocation implements Parcelable {
    public LonLat position;
    public double orientation;
    public double altitude;

    /** Construct a new UserLocation object with the given arguments.
     *
     * @param lon The user's longitude position.
     * @param lat The user's latitude position.
     * @param orientation The user's compass orientation.
     * @param altitude The user's altitude, in meters.
     */
    public UserLocation(double lon, double lat, double orientation, double altitude) {
        position = new LonLat(lon, lat);
        this.orientation = orientation;
        this.altitude = altitude;
    }

    private UserLocation(Parcel in) {
        position = in.readParcelable(LonLat.class.getClassLoader());
        orientation = in.readDouble();
        altitude = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(position, 0);
        dest.writeDouble(orientation);
        dest.writeDouble(altitude);
    }

    public static final Creator<UserLocation> CREATOR = new Creator<UserLocation>() {
        @Override
        public UserLocation createFromParcel(Parcel source) {
            return new UserLocation(source);
        }

        @Override
        public UserLocation[] newArray(int size) {
            return new UserLocation[size];
        }
    };
}
