package com.citymaps.citymapsengine;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** An object used to describe a filter which can be applied to the Citymaps business layer.

 This allows the application to show only the specified businesses on the map.  Only one filter can be active at a time.
 */
public class BusinessFilter implements Parcelable
{
    protected long mBusinessFilterPointer;
    private List<BusinessData> mBusinesses;

    public BusinessFilter()
    {
        mBusinessFilterPointer = this.nativeInitialize();
        mBusinesses = new ArrayList<BusinessData>();
    }

    public BusinessFilter(BusinessFilter other)
    {
        mBusinessFilterPointer = this.nativeInitialize();
        mBusinesses = new ArrayList<BusinessData>();

        for(BusinessData otherBusiness : other.mBusinesses)
        {
            mBusinesses.add(new BusinessData(otherBusiness));
        }
    }

    private BusinessFilter(Parcel in) {
        mBusinessFilterPointer = in.readLong();
        mBusinesses = Arrays.asList((BusinessData[])in.readParcelableArray(BusinessData.class.getClassLoader()));
    }

    /** Gets a copy of a BusinessData from the filter.
     * @param index Index of the requested BusinessData.
     * @return A copy of the BusinessData at the specified index or null if index was out of bounds.
     */
    public BusinessData getBusiness(int index)
    {
        if (index < 0 || index >= mBusinesses.size())
            return null;

        return new BusinessData(mBusinesses.get(index));
    }

    /** Returns the number of businesses in the filter. */
    public int getNumberOfBusinesses()
    {
        return mBusinesses.size();
    }

    /** Add a business to the filter.
     *
     *@param data A CEBusinessData object representing the business
     *@param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.  Set this to remove a business state.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void addBusiness(BusinessData data, int state) {
        if (data == null || state > 3)
            return;

        this.nativeAddBusinessFromData(mBusinessFilterPointer, data, state);
        mBusinesses.add(data);
    }

    /** Add a business to the filter.
     *
     * @param bid The Citymaps business ID.
     * @param location The location of the business.
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.  Set this to remove a business state.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void addBusiness(String bid, LonLat location, int state) {
        if (bid == null || location == null || state > 3)
            return;
        BusinessData data = new BusinessData();
        data.setBusinessId(bid);
        data.setLocation(location);

        mBusinesses.add(data);
        this.nativeAddBusinessFromBid(mBusinessFilterPointer, bid, location.longitude, location.latitude, state);
    }

    /** Add a business to the filter.
     *
     * @param bid The Citymaps business ID.
     * @param location The location of the business.
     * @param logoImageId The Citymaps logo image identifier for this business.
     * @param state The state of the business.   The business state may be one of the following values:
     *
     *<ul>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateNormal BusinessStateNormal} - The default state of businesses.  Set this to remove a business state.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessState2X BusinessState2X} - Doubles the size of the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateSelected BusinessStateSelected} - Puts a blue halo around the business.</li>
     *  <li>{@link BusinessLayer.BusinessState#BusinessStateHidden BusinessStateHidden} - Hides the business from view.</li>
     *</ul>
     */
    public void addBusiness(String bid, LonLat location, int logoImageId, int state) {
        if (bid == null || location == null || state > 3)
            return;
        BusinessData data = new BusinessData();
        data.setBusinessId(bid);
        data.setLocation(location);
        data.setLogoId(logoImageId);

        mBusinesses.add(data);
        this.nativeAddBusinessFromBidWithLogoImage(mBusinessFilterPointer, bid, location.longitude, location.latitude, logoImageId, state);
    }

    /** Remove a business from this filter.
     * @param businessId The Citymaps business ID.
     */
    public void removeBusiness(String businessId)
    {
        int numBusinesses = mBusinesses.size();
        for(int i = 0; i < numBusinesses; ++i)
        {
            if (mBusinesses.get(i).getBusinessId().equals(businessId))
            {
                mBusinesses.remove(i);
                nativeRemoveBusiness(mBusinessFilterPointer, businessId);
                return;
            }
        }
    }

    @Override
    protected void finalize()
    {
        this.nativeRelease(mBusinessFilterPointer);
    }

    private native long nativeInitialize();

    private native void nativeRelease(long mBusinessFilterPointer);

    private native void nativeAddBusinessFromData(long mBusinessFilterPointer, BusinessData data, int state);
    private native void nativeAddBusinessFromBid(long mBusinessFilterPointer, String bid, double lon, double lat, int state);
    private native void nativeAddBusinessFromBidWithLogoImage(long mBusinessFilterPointer, String bid, double lon, double lat, int logoImageId, int state);
    private native void nativeRemoveBusiness(long businessFilterPointer, String bid);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mBusinessFilterPointer);

        BusinessData[] data = new BusinessData[mBusinesses.size()];
        mBusinesses.toArray(data);
        dest.writeParcelableArray(data, 0);
    }

    public static final Creator<BusinessFilter> CREATOR = new Creator<BusinessFilter>() {
        @Override
        public BusinessFilter createFromParcel(Parcel source) {
            return new BusinessFilter(source);
        }

        @Override
        public BusinessFilter[] newArray(int size) {
            return new BusinessFilter[size];
        }
    };
}
