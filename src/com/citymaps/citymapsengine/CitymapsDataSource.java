package com.citymaps.citymapsengine;

import com.citymaps.citymapsengine.options.CitymapsDataSourceOptions;
import com.citymaps.citymapsengine.options.DataSourceOptions;

import java.util.HashMap;

/** Abstract base class for Citymaps data sources.
 * Additional required options include:
 * <ul>
 *     <li>cacheVersion - The cache version to use when requesting tiles.</li>
 * </ul>
 * See {@link WebDataSource} for more option details.
 * */
public class CitymapsDataSource extends WebDataSource
{
    protected class CitymapsDataSourceDescription extends WebDataSourceDescription
    {
        int cacheVersion;
        boolean signedURLs;
        String signedURLPattern;
        String apiKey;
    }

    private native long nativeCreate(Object desc);

    public CitymapsDataSource(String url, int cacheVersion)
    {
        super(new CitymapsDataSourceOptions().cacheVersion(cacheVersion).url(url));
    }

    public CitymapsDataSource(CitymapsDataSourceOptions options)
    {
        super(options);
    }

    public static CitymapsDataSource defaultVectorDataSource(String apiKey)
    {
        CitymapsDataSourceOptions options = new CitymapsDataSourceOptions(apiKey);
        options.url("http://tile.citymaps.com/map/{zoom}/{x}/{zoom}_{x}_{y}.xz");
        options.signedURLs(true).signedURLPattern("http://tile.citymaps.com/map/*");
        return new CitymapsDataSource(options);
    }

    public static CitymapsDataSource defaultBusinessDataSource(String apiKey)
    {
        CitymapsDataSourceOptions options = new CitymapsDataSourceOptions(apiKey);
        options.url("http://tile.citymaps.com/venue/{zoom}/{x}/{zoom}_{x}_{y}.json.gz");
        options.signedURLs(true).signedURLPattern("http://tile.citymaps.com/venue/*");
        return new CitymapsDataSource(options);
    }

    public static CitymapsDataSource defaultRegionDataSource(String apiKey)
    {
        CitymapsDataSourceOptions options = new CitymapsDataSourceOptions(apiKey);
        options.url("http://tile.citymaps.com/venue/{zoom}/{x}/{zoom}_{x}_{y}.json.gz");
        options.signedURLs(true).signedURLPattern("http://tile.citymaps.com/venue/*");
        return new CitymapsDataSource(options);
    }

    @Override
    protected long createDataSource(DataSourceOptions options)
    {
        CitymapsDataSourceDescription desc = new CitymapsDataSourceDescription();
        this.applyOptions(options, desc);
        return nativeCreate(desc);
    }

    @Override
    protected void applyOptions(DataSourceOptions options, DataSourceDescription desc)
    {
        try {
            super.applyOptions(options, desc);
            CitymapsDataSourceDescription cmDesc = (CitymapsDataSourceDescription) desc;
            CitymapsDataSourceOptions cmOptions = (CitymapsDataSourceOptions)options;

            cmDesc.apiKey = cmOptions.getAPIKey();
            cmDesc.cacheVersion = cmOptions.getCacheVersion();
            cmDesc.signedURLs = cmOptions.isSignedURLs();
            cmDesc.signedURLPattern = cmOptions.getSignedURLPattern();

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Incorrect parameter types.  Must be a subclass of CitymapsDataSource*");
        }
    }
}
