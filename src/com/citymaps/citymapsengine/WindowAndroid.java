package com.citymaps.citymapsengine;

import javax.microedition.khronos.opengles.GL10;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.*;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.ArrayList;

/**
 This is the base class for {@link MapView}. It manages the rendering context as well as operating system services.
 *<p>
 * The following lifecycle methods must be called on WindowAndroid if using it directly.
 * <ul>
 *      <li>onCreate(Bundle)</li>
 *      <li>onResume()</li>
 *      <li>onPause()</li>
 *      <li>onDestroy()</li>
 *      <li>onSaveInstanceState(Bundle)</li>
 *      <li>onLowMemory()</li>
 * </ul>
 *</p>

 */
public abstract class WindowAndroid extends RelativeLayout implements CitymapsGLSurfaceListener, LocationSource.LocationChangeListener, CitymapsOrientationListener.OrientationChangeListener, ConnectivityBroadcastReceiver.ConnectivityBroadcastReceiverListener
{
    static
    {
        CitymapsEngine.init();
    }
    // Data Members
    private static long msApplicationPointer = 0;
    private static Context msApplicationContext;

    private static final int LOCATION_STATUS_ENABLED = 0;
    private static final int LOCATION_STATUS_DISABLED = 1;

    protected class WindowAndroidImpl
    {
        private boolean mWindowInitialized = false;
        private boolean mLocationUpdatesActive = false;
        private boolean mOrientationUpdatesActive = false;
        private int mBackgroundColor = Color.BLACK;
        private ConnectivityBroadcastReceiver.NetworkStatus mNetworkStatus = ConnectivityBroadcastReceiver.NetworkStatus.NetworkStatusNone;
    }

    private static WindowAndroidImpl sImpl = null;

    public interface SnapshotReadyListener
    {
        public void onSnapshotReady(Bitmap snapshot);
    }

    private boolean mSubclassInitialized = false;
    private boolean mIdle = false;
    private float mDeviceDensity = 1.0f;
    private CitymapsOrientationListener mOrientationListener = null;
    private ConnectivityBroadcastReceiver mConnectivityReceiver = null;
    private CitymapsGLSurfaceView mSurfaceView = null;

    //private CitymapsTextureView mSurfaceView;
    private LocationSource mLocationSource = null;
    private boolean mEnabled = false;
    private SnapshotReadyListener mSnapshotListener = null;
    private Bitmap mSnapshotBitmap = null;
    private ArrayList<CitymapsRunLoopListener> mRunLoopListeners = null;


    /**
     * Native JNI methods Defined in WindowAndroid.cpp
     */
    private native long nativeCreate(double density, String s, int screenSize);
    private native void nativeInit(long appPointer, int width, int height);
    private native void nativeRelease(long appPointer);
    private native void nativeResize(long appPointer, int width, int height);
    private native void nativeOnContextLost(long appPointer);
    private native void nativeUpdate(long applicationPointer);
    private native boolean nativeRender(long applicationPointer);
    private native void nativeTouchStart(long windowPointer, double[] coords);
    private native void nativeTouchMove(long windowPointer, double[] coords);
    private native void nativeTouchEnd(long windowPointer, double[] coords);
    private native void nativeProcessGraphicsJob(long applicationPointer);
    private native void nativeSetBackgroundColor(long applicationPointer, int red, int green, int blue, int alpha);
    private native void nativeSetNetworkStatus(long applicationPointer, int status);
    private native void nativeOnLocationChanged(long applicationPointer, double longitude, double latitude, double accuracy);
    private native void nativeOnAltitudeChanged(long applicationPointer, double altitude);
    private native void nativeOnOrientationChanged(long applicationPointer, double orientation);
    private native void nativeOnLocationStatusChanged(long msApplicationPointer, int status);
    private native void nativeInvalidate(long applicationPointer);

    /**
     * Derived classes of WindowAndroid should initialize their Engine extension
     * on this call.
     */
    protected abstract void onInit(int width,
                                   int height);
    protected abstract void onDisable();

    /**
     * Derived classes should not rely on an application being available or ready until this method is called.
     * @param applicationPointer The memory location to a shared pointer to the Application on
     *                           the native side.
     */
    protected abstract void onEnable(long applicationPointer);
    protected abstract void onIdle();

    // Constructor
    public WindowAndroid(Context context)
    {
        super(context);
        if (sImpl == null)
        {
            sImpl = new WindowAndroidImpl();
        }

        this.init(context);
    }

    private void init(final Context context)
    {
        mSurfaceView = new CitymapsGLSurfaceView(context, this, DeviceType.getFramerate(context));

        this.addView(mSurfaceView);

        setApplicationContext(context.getApplicationContext());

        mDeviceDensity = context.getResources().getDisplayMetrics().density;
        mEnabled = false;

        mSnapshotListener = null;
        mOrientationListener = new CitymapsOrientationListener(context);
        mConnectivityReceiver = new ConnectivityBroadcastReceiver();
        mLocationSource = new CitymapsLocationSource(LocationParams.LOCATION_PARAMS_STRONG, context);

        mRunLoopListeners = new ArrayList<CitymapsRunLoopListener>();

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, final MotionEvent event) {
                if (msApplicationPointer == 0)
                    return true;

                int leftPadding = WindowAndroid.this.getPaddingLeft();
                int topPadding = WindowAndroid.this.getPaddingTop();
                int numPoints = event.getPointerCount();

                final double[] array = new double[numPoints * 2];

                for (int i = 0; i < numPoints; ++i)
                {
                    float touchX = event.getX(i) - leftPadding;
                    float touchY = event.getY(i) - topPadding;
                    // "Normalize the points" to our MapEngine's size.
                    array[2 * i] =  touchX / mDeviceDensity;
                    array[2 * i + 1] = touchY / mDeviceDensity;
                }

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        nativeTouchStart(msApplicationPointer, array);
                        break;

                    case MotionEvent.ACTION_UP:
                        nativeTouchEnd(msApplicationPointer, array);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        nativeTouchMove(msApplicationPointer, array);
                        break;
                }

                return true;
            }
        });

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_PROVIDER_CHANGED);
        intentFilter.addAction( LocationManager.PROVIDERS_CHANGED_ACTION);
        getContext().registerReceiver(mLocationBroadcastReceiver, intentFilter);
    }

    /**
     * *************
     * User API calls
     * *************
     */

    /** Returns whether the window is currently idle.
     *
     * @return Whether the window is currently idle.
     */
    public boolean isIdle()
    {
        return mIdle;
    }

    /**
     * Adds a listener to the run loop.
     * @param listener New listener to add to the run loop.
     */
    public void addToRunLoop(CitymapsRunLoopListener listener)
    {
        if (!mRunLoopListeners.contains(listener))
            mRunLoopListeners.add(listener);
    }

    /**
     * Removes a listener from the run loop.
     * @param listener New listener to remove from the run loop.
     */
    public void removeFromRunLoop(CitymapsRunLoopListener listener)
    {
            mRunLoopListeners.remove(listener);
    }

    /**
     * Sets the source of GPS location updates.
     * @param source The source of GPS location updates to be used on the map.
     */
    public void setLocationSource(LocationSource source)
    {
        mLocationSource.deactivate();

        if (source != null)
        {
            mLocationSource = source;
        }
        else
        {
            mLocationSource = new CitymapsLocationSource(LocationParams.LOCATION_PARAMS_STRONG, this.getContext());
        }

        if (sImpl.mLocationUpdatesActive)
        {
            mLocationSource.activate(this);
        }
    }

    /**
     * Begin tracking user's GPS location. It will use the default {@link LocationParams}.
     */
    public void startUpdatingLocation()
    {
        mLocationSource.activate(WindowAndroid.this);
        sImpl.mLocationUpdatesActive = true;
    }

    /**
     * Begin tracking user's orientation.
     */
    public void startUpdatingOrientation()
    {
        mOrientationListener.activate(WindowAndroid.this);
        sImpl.mOrientationUpdatesActive = true;
    }

    /**
     * Stop tracking user's GPS location.
     */
    public void stopUpdatingLocation()
    {
        mLocationSource.deactivate();
        sImpl.mLocationUpdatesActive = false;
    }

    /**
     * Stop tracking user's orientation.
     */
    public void stopUpdatingOrientation()
    {
        mOrientationListener.deactivate();
        sImpl.mOrientationUpdatesActive = false;
    }

    /**
     * Returns whether GPS service is currently enabled on this device.
     */
    public boolean isGPSServiceEnabled() {
        LocationManager locationManager = (LocationManager) this.getContext()
                .getSystemService(Context.LOCATION_SERVICE);

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public Location getLastKnownLocation()
    {
        return mLocationSource.getLastKnownLocation();
    }

    /**
     * Sets the background color of the window.
     * @see Color
     */
    public void setBackgroundColor(int color) {
        sImpl.mBackgroundColor = color;
        if (sImpl.mWindowInitialized)
        {
            nativeSetBackgroundColor(msApplicationPointer, Color.red(color), Color.green(color), Color.blue(color), Color.alpha(color));
        }
    }

    /**
     *  Takes a snapshot of the current view
     */
    public void requestSnapshot(SnapshotReadyListener listener)
    {
        mSnapshotListener = listener;
    }

    /**
     *  Takes a snapshot of the current view
     *  @param bitmap The bitmap to store the snapshot in
     */
    public void requestSnapshot(SnapshotReadyListener listener, Bitmap bitmap)
    {
        mSnapshotListener = listener;
        mSnapshotBitmap = bitmap;
    }

    public void onSizeChanged(int width, int height)
    {
        if (msApplicationPointer == 0)
        {
            String deviceName = Build.MODEL;
            int screenSize = determineScreenSize();
            msApplicationPointer = nativeCreate(mDeviceDensity, deviceName, screenSize);
            this.onNetworkStatusChange(sImpl.mNetworkStatus);
        }

        if (!sImpl.mWindowInitialized)
        {
            nativeInit(msApplicationPointer, (int) (width / mDeviceDensity), (int) (height / mDeviceDensity));
            sImpl.mWindowInitialized = true;
            this.setBackgroundColor(sImpl.mBackgroundColor);
        } else
        {
            nativeResize(msApplicationPointer, (int) (width / mDeviceDensity), (int) (height / mDeviceDensity));
        }

        if (!mSubclassInitialized)
        {
            onInit((int) (width / mDeviceDensity), (int) (height / mDeviceDensity));
            mSubclassInitialized = true;
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        super.onLayout(changed, l, t, r, b);
        if (mSurfaceView != null)
        {
            mSurfaceView.layout(l, t, r, b);
        }
    }

    @Override
    protected void finalize() throws Throwable
    {
          super.finalize();
//        if (msApplicationPointer != 0)
//        {
//            Log.w("MapEngine", "Deleting native app pointer.");
//            nativeRelease(msApplicationPointer);
//        }
    }

    public void onLocationChanged(final Location location)
    {
        if (msApplicationPointer != 0)
        {
            final double longitude = location.getLongitude();
            final double latitude = location.getLatitude();
            final double accuracy = location.getAccuracy();

            mSurfaceView.post(new Runnable() {
                @Override
                public void run()
                {
                    nativeOnLocationChanged(msApplicationPointer, longitude, latitude, accuracy);

                    if (location.hasAltitude())
                    {
                        nativeOnAltitudeChanged(msApplicationPointer, location.getAccuracy());
                    }
                }
            });

        }
    }

    public void onOrientationChanged(final double orientation)
    {
        if (msApplicationPointer != 0)
        {
            mSurfaceView.post(new Runnable()
            {
                @Override
                public void run()
                {
                    nativeOnOrientationChanged(msApplicationPointer, orientation);
                }
            });
        }
    }

    public void onNetworkStatusChange(ConnectivityBroadcastReceiver.NetworkStatus status)
    {
        if (msApplicationPointer != 0 && status != sImpl.mNetworkStatus)
        {
            sImpl.mNetworkStatus = status;

            final int value = status.getValue();

            mSurfaceView.post(new Runnable()
            {
                @Override
                public void run()
                {
                    nativeSetNetworkStatus(msApplicationPointer, value);
                }
            });
        }
    }

    public void onLocationStatusChange(final int status)
    {
        if (msApplicationPointer != 0)
        {
            mSurfaceView.post(new Runnable()
            {
                @Override
                public void run()
                {
                    nativeOnLocationStatusChanged(msApplicationPointer, status);
                }
            });
        }
    }

    /*
     *Internal calls
     */
    protected static Context getApplicationContext() {
        return msApplicationContext;
    }

    protected static void setApplicationContext(Context context)
    {
        msApplicationContext = context;
    }

    // Lifecycle methods. These must be called if using WindowAndroid directly.

    /** Restores some map state from a previous instance. This should be called by the user.
     *
     */
    public void onCreate(Bundle bundle)
    {

    }

    /**
     * Resume sensor services and rendering. If this window is not managed by a fragment, this should
     * be called by the user's application.
     */
    public void onResume()
    {
        Log.i("MapEngine", "Resuming map engine");
        if (mSurfaceView != null)
        {
            mSurfaceView.onResume();
            if (msApplicationPointer != 0)
            {
                nativeInvalidate(msApplicationPointer);
            }

            this.enable();
        }

        if (sImpl.mLocationUpdatesActive)
        {
            mLocationSource.activate(this);
        }
        if (sImpl.mOrientationUpdatesActive)
        {
            mOrientationListener.activate(this);
        }

        mConnectivityReceiver.activate(msApplicationContext, this);
        Log.i("MapEngine", "Resume complete");

        checkLocationServices();
    }

    /**
     * Pause sensor services and rendering. If this window is not managed by a fragment, this should
     * be called by the user's application.
     */
    public void onPause()
    {
        Log.i("MapEngine", "Pausing map engine");
        if (mSurfaceView != null)
        {
            mSurfaceView.onPause();
        }

        mLocationSource.deactivate();
        mOrientationListener.deactivate();
        mConnectivityReceiver.deactivate();

        Log.i("MapEngine", "Pause complete");
    }

    /**
     * Begins releasing all internal references. This should be called by the user.
     */
    public void onDestroy()
    {
        getContext().unregisterReceiver(mLocationBroadcastReceiver);

        mSubclassInitialized = false;
        mOrientationListener.deactivate();
        mOrientationListener = null;
        mConnectivityReceiver.deactivate();
        mConnectivityReceiver = null;
        mLocationSource.deactivate();
        mLocationSource = null;
        mSurfaceView.onPause();
        mSurfaceView = null;
        mSnapshotListener = null;
        mSnapshotBitmap = null;
        mRunLoopListeners = null;

        this.removeAllViews();
    }

    /**
     * Saves some state for the next instance. This should be called by the user.
     * @param bundle Bundle to save to.
     */
    public void onSaveInstanceState(Bundle bundle)
    {

    }

    /**
     * Informs the engine to begin purging memory. This should be called by the user.
     */
    public void onLowMemory()
    {

    }

    private Bitmap takeSnapshot(GL10 gl)
    {
        int width = this.getWidth();
        int height = this.getHeight();
        int screenShotSize = width * height;

        ByteBuffer buffer = ByteBuffer.allocateDirect(screenShotSize * 4);
        buffer.order(ByteOrder.nativeOrder());

        gl.glReadPixels(0, 0, width, height, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, buffer);
        int error = gl.glGetError();

        byte[] bytes = buffer.array();

        for(int i = 0; i < screenShotSize * 4; i += 4)
        {
            // RGBA
            byte r = bytes[i];
            byte g = bytes[i + 1];
            byte b = bytes[i + 2];
            byte a = bytes[i + 3];

            bytes[i] = a;
            bytes[i + 1] = r;
            bytes[i + 2] = g;
            bytes[i + 3] = b;

            // ARGB
        }

        int pixelBuffer[] = new int[screenShotSize];
        IntBuffer intBuffer = ByteBuffer.wrap(bytes).asIntBuffer();//buffer.asIntBuffer();

        intBuffer.get(pixelBuffer);
        Bitmap bitmap;
        if (mSnapshotBitmap == null || mSnapshotBitmap.getWidth() != width || mSnapshotBitmap.getHeight() != height || !mSnapshotBitmap.isMutable() || mSnapshotBitmap.getConfig() != Bitmap.Config.ARGB_8888)
        {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }
        else
        {
            bitmap = mSnapshotBitmap;
        }

        bitmap.setPixels(pixelBuffer, screenShotSize - width, -width, 0, 0, width, height);

        return bitmap;
    }

    protected void preUpdate()
    {}

    protected void postUpdate()
    {}

    public void postToMapThread(Runnable runnable)
    {
        if (mSurfaceView != null)
        {
            mSurfaceView.post(runnable);
        }
        else
        {
            throw new IllegalStateException("The window's surface view is null. Is it even running?");
        }
    }

    /**
     * Called internally when the main render thread is ready for a tick.
     */
    public boolean onStep(GL10 gl)
    {
        if (msApplicationPointer != 0)
        {
            if (mSnapshotListener != null)
            {
                final Bitmap bitmap = this.takeSnapshot(gl);
                Util.postToUIThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mSnapshotListener != null)
                        {
                            mSnapshotListener.onSnapshotReady(bitmap);
                        }
                        mSnapshotListener = null;
                        mSnapshotBitmap = null;
                    }
                });
            }

            this.preUpdate();

            nativeUpdate(msApplicationPointer);

            boolean wasIdle = mIdle;
            mIdle = !nativeRender(msApplicationPointer);
            if (!wasIdle && mIdle)
            {
                this.onIdle();
            }

            this.postUpdate();
        }

        for(CitymapsRunLoopListener listener : mRunLoopListeners)
        {
            listener.onStep();
        }

        return !mIdle;
    }
    private int determineScreenSize() {

        int layout = this.getContext().getResources().getConfiguration().screenLayout;

        if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE)
            return 4;
        if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE)
            return 3;
        if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL)
            return 2;
        if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL)
            return 1;

        return 0;
    }

    /**
     * Called internally when the rendering context is lost.
     */
    public void onContextLost()
    {
        if (msApplicationPointer != 0)
        {
            nativeOnContextLost(msApplicationPointer);
        }

        this.disable();
    }

    /**
     * Called internally when the rendering window has changed
     */
    public void onSurfaceChanged(int width, int height)
    {
        this.onSizeChanged(width, height);
        this.setBackgroundColor(sImpl.mBackgroundColor);
        this.enable();
    }

    /**
     * Called internally when the rendering window has been created
     */
    public void onSurfaceCreated()
    {
    }

    private void enable() {
        if (msApplicationPointer != 0) {
            if (!mEnabled) {
                Log.i("MapEngine", "Enable");
                this.onEnable(msApplicationPointer);
                Log.i("MapEngine", "Enabled");
            }

            mEnabled = true;
        }
    }

    private void disable() {
        if (msApplicationPointer != 0) {
            if (mEnabled) {
                Log.i("MapEngine", "Disable");
                this.onDisable();
                Log.i("MapEngine", "Disabled");
            }
            mEnabled = false;
        }
    }

    protected BroadcastReceiver mLocationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkLocationServices();
        }
    };

    private void checkLocationServices() {
        LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean enabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        int status = enabled ? LOCATION_STATUS_ENABLED : LOCATION_STATUS_DISABLED;
        onLocationStatusChange(status);
    }
}
