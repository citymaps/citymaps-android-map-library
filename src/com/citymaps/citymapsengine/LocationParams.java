package com.citymaps.citymapsengine;

/** This object is used to describe the desired behavior of the map's user location management.
 *
 * <p>It is important to tweak these parameters depending on your target GPS accuracy and power consumption.</p>
 */

public class LocationParams {

    public static final int LOCATION_ACCURACY_COURSE = 0;
    public static final int LOCATION_ACCURACY_FINE = 1;

    public static final int MIN_DISTANCE_NEAR = 5;
    public static final int MIN_DISTANCE_MEDIUM = 25;
    public static final int MIN_DISTANCE_FAR = 50;

    public static final float MIN_TIME_SHORT = 1.0f;
    public static final float MIN_TIME_MEDIUM = 5.0f;
    public static final float MIN_TIME_LONG = 10.0f;

    public static final LocationParams LOCATION_PARAMS_WEAK = new LocationParams(LOCATION_ACCURACY_COURSE, MIN_DISTANCE_FAR, MIN_TIME_LONG);
    public static final LocationParams LOCATION_PARAMS_MEDIUM = new LocationParams(LOCATION_ACCURACY_COURSE, MIN_DISTANCE_MEDIUM, MIN_TIME_MEDIUM);
    public static final LocationParams LOCATION_PARAMS_STRONG = new LocationParams(LOCATION_ACCURACY_FINE, MIN_DISTANCE_NEAR, MIN_TIME_SHORT);

    private int mAccuracy;
    private int mMinDistance;
    private float mMinTime;

    /** Construct a LocationParams object with the default arguments
     *
     * <p>Default arguments are:</p>
     *
     * <ul>
     *     <li>accuracy: {@link LocationParams#LOCATION_ACCURACY_COURSE}</li>
     *     <li>minDistance: {@link LocationParams#MIN_DISTANCE_FAR}</li>
     *     <li>minTime: {@link LocationParams#MIN_TIME_LONG}</li>
     * </ul>
     *
     */
    public LocationParams() {
        mAccuracy = LOCATION_ACCURACY_COURSE;
        mMinDistance = MIN_DISTANCE_FAR;
        mMinTime = MIN_TIME_LONG;
    }

    /** Construct a LocationParams object with the given arguments.
     *
     * @param accuracy The desired accuracy of the GPS position.
     * @param minDistance The desired minimum distance to move before a GPS event is generated.
     * @param minTime The minimum amount of time, in seconds, to wait between GPS events.
     */

    public LocationParams(int accuracy, int minDistance, float minTime) {
        mAccuracy = accuracy;
        mMinDistance = minDistance;
        mMinTime = minTime;
    }

    /** The desired accuracy, in meters of GPS information.
     *
     * @return The current accuracy, in meters.
     */

    public int getAccuracy() {
        return mAccuracy;
    }

    /** Set the desired accuracy, in meters, of GPS information
     *
     * @param accuracy The new accuracy, in meters.
     */

    public void setAccuracy(int accuracy) {
        this.mAccuracy = accuracy;
    }

    /** Get the desired minimum distance, in meters, before a GPS event is generated.
     *
     * @return The current minimum distance, in meters.
     */

    public int getMinDistance() {
        return mMinDistance;
    }

    /** Set the desired minimum distance, in meters, before a GPS event is generated.
     *
     * @param minDistance The new minimum distance, in meters.
     */

    public void setMinDistance(int minDistance) {
        this.mMinDistance = minDistance;
    }

    /** Get the minimum amount of time, in seconds, to wait between GPS events.
     *
     * @return The current minimum time.
     */

    public float getMinTime() {
        return mMinTime;
    }

    /** Set the minimum amount of time, in seconds, to wait between GPS events.
     *
     * @param minTime The new minimum time.
     */

    public void setmMinTime(float minTime) {
        this.mMinTime = minTime;
    }
}
